###########################################################
#######  											#######
#######		WEATHER_CALCULATIONS ALGORITHM			#######
#######												#######
#######		General output:							#######
#######		- stationId, timestamp, value			####### 
#######		Types and timestamp output format:		#######
#######		- allPeriod: YYYYMM						#######
#######		- hourly: UNIXtimestamp					#######
#######		- daily: YYYYMMDD						#######
#######		- halfhourly: UNIXtimestamp				#######
#######		- quarterhourly: UNIXtimestamp			#######
#######     - five_minutes: UNIXtimestamp			#######
#######		- one_minute: UNIXtimestamp				#######
#######		- allPeriod_hdd: YYYYMM					#######
#######		- daily_hdd: YYYYMMDD 					#######
#######		- monthly_hdd: YYYYMM					#######
#######		- allPeriod_cdd: YYYYMM					#######
#######		- daily_cdd: YYYYMMDD 					#######
#######		- monthly_cdd: YYYYMM					#######
#######		- monthly_hdd_cdd: YYYYMM				#######
#######		- daily_hdd_cdd: YYYYMMDD				#######
###########################################################

library(RJSONIO)

h = basicJSONHandler()

f <- file("stdin")
open(f)
params <- fromJSON(readLines(f), h)

library(Rhipe)
rhinit()

#TEST
#params<-list()
#params$input<-"/tmp/bt207t/TEST/input_from_query_weather"
#params$output<-"/tmp/bt207t/TEST/result_weather"
#params$type_of_data<-"temperatureAir"
#params$type_of_calculation<-"monthly_hdd"
#params$ts_from<-"2013/01/01 00:01:00"
#params$ts_to<-"2014/01/31 23:59:00"
#params$balance_temp<-15

# Variables definition
type_of_data <- params$type_of_data
type_of_calculation <- params$type_of_calculation
ts_from <- strftime(as.POSIXlt(params$ts_from),"%s")
ts_to <- strftime(as.POSIXlt(params$ts_to),"%s")
ndays<-ceiling((as.numeric(ts_to)-as.numeric(ts_from))/(3600*24))
if(type_of_calculation %in% c("allPeriod_hdd","monthly_hdd","daily_hdd")){
	balance_temp_heating<-params$balance_temp
} else if(type_of_calculation %in% c("allPeriod_cdd","monthly_cdd","daily_cdd")){
	balance_temp_cooling<-params$balance_temp
} else if(type_of_calculation %in% c("monthly_hdd_cdd","daily_hdd_cdd")){
	balance_temp_cooling<-params$balance_temp + 4
	balance_temp_heating<-params$balance_temp - 4
}
if( !exists("buffer_mr",where= params) ){
	buffer_mr <- 150000
} else {
	buffer_mr <- params$buffer_mr
}


# Definition of the output frequency for the resampling calculation
if(type_of_calculation %in% c("allPeriod","monthly","daily",
		"allPeriod_hdd","monthly_hdd","daily_hdd","allPeriod_cdd",
		"monthly_cdd","daily_cdd","monthly_hdd_cdd","daily_hdd_cdd")){
	nseconds=3600*24
} else if(type_of_calculation=="hourly"){
	nseconds=3600
} else if(type_of_calculation=="halfhourly"){
	nseconds=3600/2
} else if(type_of_calculation=="quarterhourly"){
	nseconds=3600/4
} else if(type_of_calculation=="five_minutes"){
	nseconds=3600/12
} else if(type_of_calculation=="one_minute"){
	nseconds=60
}

# Definition of the function to clean the outliers of the data
tsoutliers <- function(x,plot=FALSE)
{
	x <- as.ts(x)
	tt <- 1:length(x)
	resid <- residuals(loess(x ~ tt))
	resid.q <- quantile(resid,prob=c(0.25,0.75))
	iqr <- diff(resid.q)
	limits <- resid.q + 1.5*iqr*c(-1,1)
	score <- abs(pmin((resid-limits[1])/iqr,0) + pmax((resid - limits[2])/iqr,0))
	if(plot)
	{
		plot(x)
		x2 <- ts(rep(NA,length(x)))
		x2[score>0] <- x[score>0]
		tsp(x2) <- tsp(x)
		points(x2,pch=19,col="red")
		return(invisible(score))
	}
	else
		return(score)
}


# Definition of the number of reducers
n_reducers <- 8


## Mapper definition
mapper <- expression({
			
			line <- strsplit(unlist(map.values),'\t')
			
			mapply(function(line){
						
						key_mapper <- as.character(line[1])
						key_reducer <- prod(strtoi(charToRaw(key_mapper)),na.rm=T)
						if(nchar(key_reducer)>5){
							a<-nchar(key_reducer)-5
							b<-nchar(key_reducer)-2
						} else {
							a<-1
							b<-nchar(key_reducer)
						}
						whichReducer<- as.integer(substr(key_reducer,a,b)) %% n_reducers
						
						#key: c(<reducer_which_make_the_calculation>,stationId) --> values: timestamp, weather_value
						rhcollect(c(whichReducer,key_mapper),c(line[2],line[3]))
						
					}, line, SIMPLIFY=FALSE)
		})

#reducer_test <- expression(
#  	reduce= {
#		#Join all records of the specific key to a dataframe
#    	df <- do.call(rbind,reduce.values)
#    	df<-data.frame(as.integer(df[,1]),as.numeric(df[,2]))
#    	rhcollect(reduce.key[2],df)
#    	#mapply(function(x){
#    	#	rhcollect(reduce.key,c(df[x,1],df[x,2]))
#    	#},1:nrow(df))
#    }
#)
#joining_test <- rhwatch(map=mapper, reduce=reducer_test, partitioner=list(lims=1,type='string'),
#		  	input=rhfmt(params$input,type='text'),
#		  	output=params$output,
#         	read=FALSE,mapred=list(mapred.field.separator=",", mapred.textoutputformat.usekey=FALSE,
#          	rhipe_reduce_buff_size=6000,mapred.reduce.tasks=n_reducers))
#prova<-rhread(params$output)

reducer <- expression(
		pre = {
			#Load needed libraries
			suppressMessages(suppressWarnings(library(plyr)))
		},
		reduce= {
			#Join all records of the specific key to a dataframe
			df <- do.call(rbind,reduce.values)
			df<-data.frame(as.integer(df[,1]),as.numeric(df[,2]))
			
			#stationId
			stationId<-reduce.key[2]
			
			#Order the df by timestamp
			df <- df[order(df[,1]),]
			
			# Filter of outliers in case of temperatureAir data
			if(type_of_data=="temperatureAir") df[!(df[,2]>(-50)&df[,2]<60)|is.na(df[,2]),2] <- NA
			
			# Most common frequency in the input data
			freq<-table(df[2:nrow(df),1]-df[1:(nrow(df)-1),1])
			freq<-as.numeric(names(freq)[which(freq %in% max(freq))])
			
			###############
			#### Resampling algorithm - It has the scope to redefine the timestamps in order to
			#### join the consumption data with the weather data.
			###############
			
			# Consider if the relation between the theorical frequency considering the amount of data we have
			# and the most common frequency is lower than 2, and the condition that if we want the resample 
			# to the smaller frequencies, such as halfhourly or quarterhourly data, we must an original frequency
			# lower than two hours (7200s).
			
			# IF THE CONDITION IS TRUE
			if((nseconds<=3600 && freq<=7200)){ #((ndays/nrow(df))*3600*24/freq)<=2 && 
				# Vector of the theoric time from ts_from to ts_to by the frequency defined before.
				theoric_time<-seq(as.numeric(ts_from),as.numeric(ts_to),nseconds)
				# Definition of the empty "df_theo" data frame
				df_theo<-data.frame("Timestamp"=theoric_time,"Value"=rep(NA,length(theoric_time)),"Estimated_Value"=rep(NA,length(theoric_time))) 
				# Depending the type_of_data calculate the resampling result for each theorical timestamp
				if(type_of_data %in% c("temperatureAir","relativeHumidity","avgWindSpeed", "avgWindDirection", "barometricPressure")){
					df_theo[,"Value"]<-mapply(function(i){
								mean(df[which(df[,1]>=df_theo[i,"Timestamp"]&df[,1]<(df_theo[i,"Timestamp"]+nseconds)),2],na.rm=T)
							},1:length(theoric_time))
				} else if(type_of_data %in% c("rainFall", "monthlyHDD", "dailyHDD")){
					df_theo[,"Value"]<-mapply(function(i){
								sum(df[which(df[,1]>=df_theo[i,"Timestamp"]&df[,1]<(df_theo[i,"Timestamp"]+nseconds)),2],na.rm=T)
							},1:length(theoric_time))
				} else if(type_of_data %in% c("maxWindSpeed", "maxWindDirection")){
					df_theo[,"Value"]<-mapply(function(i){
								max(df[which(df[,1]>=df_theo[i,"Timestamp"]&df[,1]<(df_theo[i,"Timestamp"]+nseconds)),2],na.rm=T)
							},1:length(theoric_time))
				}
				# Clean of outliers of df_theo[,"Value"]
				df_theo[tsoutliers(df_theo["Value"],plot=FALSE)>0,"Value"]<-NA
				# Approximate the value in some timestamps if they don't have any value data.
				if(sum(is.na(df_theo[,"Value"]))>0){
					x<-approx(df_theo$Timestamp, df_theo$Value, xout=df_theo$Timestamp, method="linear", n=nrow(df_theo))$x
					y<-round(approx(df_theo$Timestamp, df_theo$Value, xout=df_theo$Timestamp, method="linear", n=nrow(df_theo))$y,2)
					df_theo[,"Estimated_Value"]<-y
				} else {
					df_theo[,"Estimated_Value"]<-df_theo[,"Value"]
				}
				# IF THE CONDITION IS FALSE
			} else {
				# Calculation of the hourly resampling in order to get the empty timegaps.
				nseconds_pre<-3600
				theoric_time<-seq(as.numeric(ts_from),as.numeric(ts_to),nseconds_pre)
				df_theo_pre<-data.frame("Timestamp"=theoric_time,"Estimated_Value"=rep(NA,length(theoric_time))) 
				if(type_of_data %in% c("temperatureAir","relativeHumidity","avgWindSpeed", "avgWindDirection", "barometricPressure")){
					df_theo_pre[,"Estimated_Value"]<-mapply(function(i){
								mean(df[which(df[,1]>=df_theo_pre[i,"Timestamp"]&df[,1]<(df_theo_pre[i,"Timestamp"]+nseconds_pre)),2],na.rm=T)
							},1:length(theoric_time))
				} else if(type_of_data %in% c("rainFall", "monthlyHDD", "dailyHDD")){
					df_theo_pre[,"Estimated_Value"]<-mapply(function(i){
								sum(df[which(df[,1]>=df_theo_pre[i,"Timestamp"]&df[,1]<(df_theo_pre[i,"Timestamp"]+nseconds_pre)),2],na.rm=T)
							},1:length(theoric_time))
				} else if(type_of_data %in% c("maxWindSpeed", "maxWindDirection")){
					df_theo_pre[,"Estimated_Value"]<-mapply(function(i){
								max(df[which(df[,1]>=df_theo_pre[i,"Timestamp"]&df[,1]<(df_theo_pre[i,"Timestamp"]+nseconds_pre)),2],na.rm=T)
							},1:length(theoric_time))
				}
				# Clean of outliers of df_theo[,"Estimated_value"]
				df_theo_pre[tsoutliers(df_theo_pre["Estimated_Value"],plot=FALSE)>0,"Estimated_Value"]<-NA
				# Resampling another time from the resampled hourly data to the desired frequency. Consider that
				# only the calculated values with less than a 50% of not available (NA) hourly values will be calculated
				theoric_time<-seq(as.numeric(ts_from),as.numeric(ts_to),nseconds)
				df_theo<-data.frame("Timestamp"=theoric_time,"Estimated_Value"=rep(NA,length(theoric_time))) 
				if(type_of_data %in% c("temperatureAir","relativeHumidity","avgWindSpeed", "avgWindDirection", "barometricPressure")){
					df_theo[,"Estimated_Value"]<-mapply(function(i){
								temporalData<-df_theo_pre[which(df_theo_pre[,"Timestamp"]>=df_theo[i,"Timestamp"]&df_theo_pre[,"Timestamp"]<(df_theo[i,"Timestamp"]+nseconds)),"Estimated_Value"]
								if(sum(is.na(temporalData))>as.integer(0.5*length(temporalData))){
									mean(temporalData,na.rm=F)
								} else mean(temporalData,na.rm=T)
							},1:length(theoric_time))
				} else if(type_of_data %in% c("rainFall", "monthlyHDD", "dailyHDD")){
					df_theo[,"Estimated_Value"]<-mapply(function(i){
								temporalData<-df_theo_pre[which(df_theo_pre[,"Timestamp"]>=df_theo[i,"Timestamp"]&df_theo_pre[,"Timestamp"]<(df_theo[i,"Timestamp"]+nseconds)),"Estimated_Value"]
								if(sum(is.na(temporalData))>as.integer(0.5*length(temporalData))){
									sum(temporalData,na.rm=F)
								} else sum(temporalData,na.rm=T)
							},1:length(theoric_time))
				} else if(type_of_data %in% c("maxWindSpeed", "maxWindDirection")){
					df_theo[,"Estimated_Value"]<-mapply(function(i){
								temporalData<-df_theo_pre[which(df_theo_pre[,"Timestamp"]>=df_theo[i,"Timestamp"]&df_theo_pre[,"Timestamp"]<(df_theo[i,"Timestamp"]+nseconds)),"Estimated_Value"]
								if(sum(is.na(temporalData))>as.integer(0.5*length(temporalData))){
									max(temporalData,na.rm=F)
								} else max(temporalData,na.rm=T)
							},1:length(theoric_time))
				}
			}
			
			# Calculation of the monthly and allPeriod frequencies. They are calculated differently because the number of seconds of a month
			# change depending on the month, so this results will be calculated from a initial daily resampling. Addicionally, we could take
			# into account the NA values from some days, in case of that, to consider these values in the month or allPeriod resampling.
			# Also is remarkable, that in monthly, allPeriod and daily resampling, the timestamp representation will be the "YYYY" for the
			# first, "YYYYMM" for the second one, and "YYYYMMDD" for the third one. 
			if(type_of_calculation=="monthly"){
				df_theo[,"Timestamp"]<-(strftime(as.POSIXlt(as.numeric(df_theo[,"Timestamp"]),origin=as.POSIXlt("1970/01/01 00:00:00"),format="%s"),"%Y%m"))
				if(type_of_data %in% c("temperatureAir","relativeHumidity","avgWindSpeed", "avgWindDirection", "barometricPressure")){
					df_theo<-ddply(df_theo,.(Timestamp),summarize,"Estimated_Value"=mean(Estimated_Value,na.rm=T))
				} else if(type_of_data %in% c("rainFall", "monthlyHDD", "dailyHDD")){
					df_theo<-ddply(df_theo,.(Timestamp),summarize,"Estimated_Value"=sum(Estimated_Value,na.rm=T))
				} else if(type_of_data %in% c("maxWindSpeed", "maxWindDirection")){
					df_theo<-ddply(df_theo,.(Timestamp),summarize,"Estimated_Value"=max(Estimated_Value,na.rm=T))
				}
			} else if(type_of_calculation=="allPeriod"){
				df_theo[,"Timestamp"]<-(strftime(as.POSIXlt(as.numeric(ts_from),origin=as.POSIXlt("1970/01/01 00:00:00"),format="%s"),"%Y"))
				if(type_of_data %in% c("temperatureAir","relativeHumidity","avgWindSpeed", "avgWindDirection", "barometricPressure")){
					df_theo<-ddply(df_theo,.(Timestamp),summarize,"Estimated_Value"=mean(Estimated_Value,na.rm=T))
				} else if(type_of_data %in% c("rainFall", "monthlyHDD", "dailyHDD")){
					df_theo<-ddply(df_theo,.(Timestamp),summarize,"Estimated_Value"=sum(Estimated_Value,na.rm=T))
				} else if(type_of_data %in% c("maxWindSpeed", "maxWindDirection")){
					df_theo<-ddply(df_theo,.(Timestamp),summarize,"Estimated_Value"=max(Estimated_Value,na.rm=T))
				}
			} else if(type_of_calculation=="daily"){
				df_theo[,"Timestamp"]<-(strftime(as.POSIXlt(as.numeric(df_theo[,"Timestamp"]),origin=as.POSIXlt("1970/01/01 00:00:00"),format="%s"),"%Y%m%d"))
			}
			
			
			
			# These calculations will be carried out only if the values are not yet HDD values 
			if (!type_of_data %in% c("monthlyHDD", "dailyHDD")){
				
				# Calculation of the heating degree day. This values will be calculated from the daily resampled values. It will take into account a
				# balance temperature (balance_temp_heating) in order to calculate the results. For the monthly_hdd and allPeriod_hdd calculations
				# we will only sum the daily values according the frequency desired (monthly or allPeriod)
				if(type_of_calculation %in% c("monthly_hdd","daily_hdd","allPeriod_hdd")){
					df_theo$Estimated_Value<-mapply(function(x){
								ifelse(!is.na(df_theo$Estimated_Value[x]),balance_temp_heating-df_theo$Estimated_Value[x],NA)
							},1:nrow(df_theo))	
				# The results are >0 when the temp is lower than the balance_temp_heating or 0 if not
					df_theo$Estimated_Value<-ifelse(df_theo$Estimated_Value<0,0,df_theo$Estimated_Value)
					if(type_of_calculation=="monthly_hdd"){
						df_theo[,"Timestamp"]<-(strftime(as.POSIXlt(as.numeric(df_theo[,"Timestamp"]),origin=as.POSIXlt("1970/01/01 00:00:00"),format="%s"),"%Y%m"))
						df_theo<-ddply(df_theo,.(Timestamp),summarize,"Estimated_Value"=sum(Estimated_Value,na.rm=T))
					} else if(type_of_calculation=="allPeriod_hdd"){
						df_theo[,"Timestamp"]<-(strftime(as.POSIXlt(as.numeric(ts_from),origin=as.POSIXlt("1970/01/01 00:00:00"),format="%s"),"%Y"))
						df_theo<-ddply(df_theo,.(Timestamp),summarize,"Estimated_Value"=sum(Estimated_Value,na.rm=T))  
					} else if(type_of_calculation=="daily_hdd"){
						df_theo[,"Timestamp"]<-(strftime(as.POSIXlt(as.numeric(df_theo[,"Timestamp"]),origin=as.POSIXlt("1970/01/01 00:00:00"),format="%s"),"%Y%m%d"))
					}
				}
				
				# Calculation of the cooling degree day. It works in the same way than the calculation of the heating degree day explained before.
				if(type_of_calculation %in% c("monthly_cdd","daily_cdd","allPeriod_cdd")){
					df_theo$Estimated_Value<-mapply(function(x){
								ifelse(!is.na(df_theo$Estimated_Value[x]),df_theo$Estimated_Value[x]-balance_temp_cooling,NA)
							},1:nrow(df_theo))
				# The results are >0 when the temp is higher than the balance_temp_cooling or 0 if not
					df_theo$Estimated_Value<-ifelse(df_theo$Estimated_Value<0,0,df_theo$Estimated_Value)
					if(type_of_calculation=="monthly_cdd"){
						df_theo[,"Timestamp"]<-(strftime(as.POSIXlt(as.numeric(df_theo[,"Timestamp"]),origin=as.POSIXlt("1970/01/01 00:00:00"),format="%s"),"%Y%m"))
						df_theo<-ddply(df_theo,.(Timestamp),summarize,"Estimated_Value"=sum(Estimated_Value,na.rm=T))
					} else if(type_of_calculation=="allPeriod_cdd"){
						df_theo[,"Timestamp"]<-(strftime(as.POSIXlt(as.numeric(ts_from),origin=as.POSIXlt("1970/01/01 00:00:00"),format="%s"),"%Y"))
						df_theo<-ddply(df_theo,.(Timestamp),summarize,"Estimated_Value"=sum(Estimated_Value,na.rm=T))
					} else if(type_of_calculation=="daily_cdd"){
						df_theo[,"Timestamp"]<-(strftime(as.POSIXlt(as.numeric(df_theo[,"Timestamp"]),origin=as.POSIXlt("1970/01/01 00:00:00"),format="%s"),"%Y%m%d"))
					}
				}
				
				# Calculation of the cooling degree day and the heating degree day in combination (POSITIVE VALUES -->HEATING, NEGATIVE VALUES-->COOLING)
				if(type_of_calculation %in% c("monthly_hdd_cdd","daily_hdd_cdd")){
					df_theo$Estimated_Value<-mapply(function(x){
								if(!is.na(df_theo$Estimated_Value[x])){
									if(df_theo$Estimated_Value[x]<balance_temp_heating){
										balance_temp_heating-df_theo$Estimated_Value[x]
									} else if (df_theo$Estimated_Value[x]>balance_temp_cooling){
										balance_temp_cooling-df_theo$Estimated_Value[x]
									} else 0
								} else NA
							},1:nrow(df_theo))
					if(type_of_calculation=="monthly_hdd_cdd"){
						df_theo[,"Timestamp"]<-(strftime(as.POSIXlt(as.numeric(df_theo[,"Timestamp"]),origin=as.POSIXlt("1970/01/01 00:00:00"),format="%s"),"%Y%m"))
						df_theo<-ddply(df_theo,.(Timestamp),summarize,"Estimated_Value"=sum(Estimated_Value,na.rm=T))
					} else if(type_of_calculation=="daily_hdd_cdd"){
						df_theo[,"Timestamp"]<-(strftime(as.POSIXlt(as.numeric(df_theo[,"Timestamp"]),origin=as.POSIXlt("1970/01/01 00:00:00"),format="%s"),"%Y%m%d"))
					}
				}
			}
			
			# Collect the results
			mapply(function(x){
						rhcollect(stationId, c(stationId, df_theo[x,"Timestamp"], df_theo[x,"Estimated_Value"]))
					},1:nrow(df_theo))
			
		}
)

mapreduce <- rhwatch(map=mapper, reduce=reducer, partitioner=list(lims=1,type='string'),
		input=rhfmt(params$input,type='text'),
		output=rhfmt(params$output,type='text'),
		read=FALSE,noeval=TRUE,mapred=list(mapred.field.separator=",", mapred.textoutputformat.usekey=FALSE,
				rhipe_reduce_buff_size=buffer_mr,rhipe_map_buff_size=buffer_mr,mapred.reduce.tasks=n_reducers))

job <- rhex(mapreduce, async=TRUE)
status <- rhstatus(job)
print('Printing rhstatus')
toJSON(status, collapse='', .escapeEscapes = FALSE)

rhclean()