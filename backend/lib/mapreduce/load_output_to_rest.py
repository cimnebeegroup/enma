
#from myMRJob import myMRJob
from mrjob.job import MRJob
from pymongo import MongoClient
from bson import ObjectId
import glob
from datetime import datetime
from json import load,loads

class HDFSToREST(MRJob):
   
    def mapper_init(self):
        fn = glob.glob('*.json')
        config = load(open(fn[0]))
        host = config['mongo_host']
        port = config['mongo_port']
        self.setupId = config['setupId'] if 'setupId' in config else None
        self.rest = MongoClient(host, port)
        self.rest[config['mongo_db']].authenticate(
                config['mongo_user'],
                config['mongo_pass']
                )
        self.db = self.rest[config['mongo_db']]
        self.mongo_collection = config['mongo_collection']
        self.fields = config['fields']
        self.key = config['key']
        self.sep = config['sep']
        self.update = config['update']
        
    def mapper(self, _, line):
        
        line = line.decode('utf-8').split(self.sep)

        #Construct all the document to be loaded to REST
        
        # Construction of the key variables of the document
        document = {}
        field_key=0
        field=0
        while field_key<len(self.key):
            key = self.key[field_key]
            # setupId key
            if key=="setupId":
                document[key] = ObjectId(self.setupId)
                field_key+=1
            # companyId key
            elif key in ["year","month","day","time","companyId","utilityId"]:
                document[key] = int(line[0].split("~")[field])
                field_key+=1
                field+=1
            # _created key. Datetime format defined in the config of the module
            elif key.split('~')[0]=="_created":
                document[key.split("~")[0]]  = datetime.strptime(line[0].split("~")[field],key.split("~")[1])
                field_key+=1
                field+=1
            # Other keys (strings)
            else:
                document[key] = str(line[0].split("~")[field])
                field_key+=1
                field+=1
        
        # Save the doc_to_update document if we have to update (no insert) the document
        # This document only considers the key variables of the document
        if self.update is True:
            doc_to_update = document.copy()
        
        # Continuation in the construction of the values variables of the document
        column=0
        for field in range(len(self.fields)):
            try:
                #None values
                if line[column+1] in ['\\N','\N','NULL','NA','character(0)','numeric(0)','integer(0)','logical(0)','NaN','inf','-inf']:
                    document[self.fields[field].split("~")[0]]  = None
                    column += 1
                #Float/Integer variable
                elif len(self.fields[field].split("~"))==1:
                    document[self.fields[field]]  = float(line[column+1])
                    column += 1
                #Other type of variable (See below)
                else:
                    raise(ValueError)
            except ValueError, e:
                #String variable
                if len(self.fields[field].split("~"))==1:
                    document[self.fields[field]]  = line[column+1]
                    column +=1
                #If this field has the sufix ~JSON, the algorithm parse this column
                elif self.fields[field].split("~")[1]=="JSON":
                    document[self.fields[field].split("~")[0]] = loads(line[column+1])
                    column +=1
                #Datetime variable.
                elif self.fields[field].split("~")[1]=="DATETIME": 
                    try:
                        document[self.fields[field].split("~")[0]] = datetime.strptime(line[column+1],self.fields[field].split("~")[2])
                    except:
                        document[self.fields[field].split("~")[0]] = None
                    column +=1
                #If this field has the prefix ~DO_NOT_LOAD, the algorithm pass this column
                elif self.fields[field].split("~")[1]=="DO_NOT_LOAD": 
                    column +=1
                #Datetime variable. Datetime format defined in the config of the module
                elif self.fields[field].split("~")[0]=="timestamp": 
                    document[self.fields[field].split("~")[0]] = datetime.strptime(line[column+1],self.fields[field].split("~")[1])
                    column +=1
                #Dictionary of strings variable
                else:
                    document[self.fields[field].split("~")[0]] = {self.fields[field].split("~")[pos_field+1]:str(line[column+pos_field+1].encode('utf-8')) for pos_field in range(len(self.fields[field].split("~")[1:]))}
                    column += len(self.fields[field].split("~")[1:])
        
        #Add the _created variable if it doesn't exist
        if '_created' not in document:
            document['_created']=datetime.utcnow()
        
        #Insert or update
        if self.update is False:
            self.db[self.mongo_collection].insert(document)
        else:
            self.db[self.mongo_collection].update(doc_to_update, {'$set':document}, upsert=True, multi=False)
            #self.db[self.mongo_collection].find_and_modify(query=doc_to_find, update=document, remove=False, upsert=True, sort=None, full_response=False)
            #print "Find and modify a row into rest with key %s. If the key doesn't exists, it will create the row" % line[0]
                    

            
            
            
            
if __name__ == '__main__':
    HDFSToREST.run()    