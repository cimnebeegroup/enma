
#from myMRJob import myMRJob
from mrjob.job import MRJob
import happybase
import glob
from json import load

class HDFSToHBase(MRJob):
    
  
    def mapper_init(self):
        fn = glob.glob('*.json')
        config = load(open(fn[0]))
        
        self.setupId = config['setupId']
        
        self.hbase = happybase.Connection(config['hbase_host'], config['hbase_port'])
        self.hbase.open()

        self.tables_list = self.hbase.tables()
        try:
            if not config['hbase_table_name'] in self.tables_list:
                cfs={}
                [cfs.update({item: dict()}) for item in config['hbase_table_cf']]
                self.hbase.create_table(config['hbase_table_name'], cfs)
                self.tables_list.append(config['hbase_table_name'])
        except:
            pass
        self.hbase_table = self.hbase.table(config['hbase_table_name'])
        
        # Other Hbase configs
        self.cf_lut = config['hbase_table']
        self.sep = config['sep']
        
    
    def mapper(self, _, line):
        """
        line = line.split(self.sep)
        try:
            row = {}
            for field in range(len(self.cf_lut)):
                row[self.cf_lut[field]] = line[field+1]
            key = line[0]+'~'+str(self.setupId)
            self.hbase_table.put(key, row)
            print 'Inserted a row into HBase with key %s' % key
                    
        except ValueError:
            print "!! Error !! - Error while inserting result of module into HBase on loop line: %s" % line
        """
        # To avoid problems with non-ascii character -> decode('utf-8') encode('utf-8') 
        # Hbase shell doesn't show utf-8 characters, but the characters are already saved as utf-8
        line = line.decode('utf-8').split(self.sep)
        try:
            row = {}
            for field in range(len(self.cf_lut)):
                row[self.cf_lut[field]] = line[field+1].encode('utf-8')
            key = line[0]+'~'+str(self.setupId)
            self.hbase_table.put(key, row)
            #print 'Inserted a row into HBase with key %s' % key
        
        except ValueError: 
            print "!! Error !! - Error while inserting result of module into HBase on loop line: %s" % line
    
    
if __name__ == '__main__':
    HDFSToHBase.run()    
