# Generic imports
import random
import string
import subprocess
import re
import calendar
import pytz
import types
import os
from pymongo import MongoClient
from json import loads as JSONloads
from json import dumps as JSONdumps
from bson import ObjectId, BSON
from datetime import datetime, timedelta
from UserDict import IterableUserDict
from dateutil.relativedelta import relativedelta
from subprocess import Popen, PIPE
from tempfile import NamedTemporaryFile

from mapreduce.load_output_to_hbase import HDFSToHBase
from mapreduce.load_output_to_rest import HDFSToREST

import logging
logger = logging.getLogger('module')


class Report(IterableUserDict):
    def __init__(self, data = {}, monogodb_fd = None, db = None, collection = None, **kw):
        IterableUserDict.__init__(self)
        if monogodb_fd is not None and db is not None and collection is not None:
            self.db = monogodb_fd[db][collection]
    
    def update(self):
        # Add the companyId variable
        if not 'companyId' in self.data:
            self.data['companyId'] = self.data['params'][0].get('companyId') if 'companyId' in self.data['params'][0] else None
        
        # Add the datetime when the module is already calculated
        if not 'finished_at' in self.data:
            self.data['finished_at'] = None
        
        # Add of update the datetime of the last update
        self.data['last_update'] = datetime.now()
        
        if '_id' in self.data:
            self.db.update({"_id": self.data['_id']}, {"$set": self.data}, upsert=True)
        else:
            self.data['_id'] = self.db.insert(self.data)
        
    def save(self):
        # Add the companyId variable
        if not 'companyId' in self.data:
            self.data['companyId'] = self.data['params'][0].get('companyId') if 'companyId' in self.data['params'][0] else None
        
        # Add or update the datetime when the module is already calculated
        self.data['finished_at'] = datetime.now()
        
        # Pop the last_update variable
        if 'last_update' in self.data:
            self.data.pop('last_update')
        
        if '_id' in self.data:
            self.db.update({"_id": self.data['_id']}, {"$set": self.data}, upsert=True)
        else:
            self.data['_id'] = self.db.insert(self.data)
            
"""
Not context-dependent functions
"""


def id_generator(size=10, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))


def random_paths(obj,old,new):
    try:
        # replace in a dict object
        for key in obj.keys():
            obj[key] = obj[key].replace(old,new)
    except Exception,e:
        #r eplace in a list object
        obj = [item.replace(old,new) for item in obj]
    return obj


def unix_time(dt):
    epoch = datetime.utcfromtimestamp(0)
    unix_time = (dt - epoch).total_seconds()
    return int(unix_time)


def get_year(ts_from, ts_to=None):
    logger.debug('Getting year for ts_from: %s ' % ts_from)
    try:
        fr = int("%d" % (ts_from.year))
    except Exception, e:
        raise Exception('Timestamp provided is not a valid Timestamp')
    logger.debug('Successfully recovered DateTime from timestamp %s. Creating YYYY representation ' % fr)
    return fr


def get_month(ts_from, ts_to=None):
    logger.debug('Getting month for ts_from: %s ' % ts_from)
    try:
        fr = int("%d%02d" % (ts_from.year,ts_from.month))
    except Exception, e:
        raise Exception('Timestamp provided is not a valid Timestamp')
    logger.debug('Successfully recovered DateTime from timestamp %s. Creating YYYYMM representation ' % fr)
    return fr


def get_day(ts_from, ts_to=None):
    logger.debug('Getting day for ts_from: %s ' % ts_from)
    try:
        fr = int("%d%02d%02d" % (ts_from.year,ts_from.month,ts_from.day))
    except Exception, e:
        raise Exception('Timestamp provided is not a valid Timestamp')
    logger.debug('Successfully recovered DateTime from timestamp %s. Creating YYYYMMDD representation ' % fr)
    return fr


def get_hour(ts_from, ts_to=None):
    logger.debug('Getting hour for ts_from: %s ' % ts_from)
    try:
        fr = int("%d%02d%02d%02d" % (ts_from.year,ts_from.month,ts_from.day,ts_from.hour))
    except Exception, e:
        raise Exception('Timestamp provided is not a valid Timestamp')
    logger.debug('Successfully recovered DateTime from timestamp %s. Creating YYYYMMDDHH representation ' % fr)
    return fr


def yearmonth_to_datetime(yearmonth):
    try:
        ym = str(ym)
        dt = datetime.strptime(ym,"%Y%m")
    except Exception, e:
        raise Exception('Yearmonth cannot be cast to datetime')
    return dt


def diff_month(d1, d2):
    return (d1.year - d2.year)*12 + d1.month - d2.month


def dtLocal_to_tsHbase(date_local, tz='Europe/Madrid'):
    """From datetime in local timezone to timestamp in Hbase timezone"""
    date_hbase=pytz.timezone("%s" % tz).localize(datetime(date_local.year,date_local.month,
    date_local.day,date_local.hour,date_local.minute,date_local.second,date_local.microsecond)).astimezone(pytz.utc)
    return date_hbase.strftime("%s")


def unique(seq, idfun=None): 
   # order preserving
   if idfun is None:
       def idfun(x): return x
   seen = {}
   result = []
   for item in seq:
       marker = idfun(item)
       # in old Python versions:
       # if seen.has_key(marker)
       # but in new ones:
       if marker in seen: continue
       seen[marker] = 1
       result.append(item)
   return result


def value_in_a_dict(seq, key):
    return dict((d[key], dict(d, index=i)) for (i, d) in enumerate(seq))


def delete_items_in_list(list,items):
    
    # Items in list format
    items=[items] if isinstance(items,types.StringTypes) else items
    
    # Search and delete the items elements in the list
    for item in items:
        if item in list:
            list.remove(item)
    
    return list


def is_float(s):
    try:
        float(s)
        if s.lower() in ['nan','inf','+nan','-nan','+inf','-inf']:
            return False
        else:
            return True
    except ValueError:
        return False


def get_various_months(ts_to,monthback):
    try:
        ts_t = ts_to
    except Exception, e:
        raise Exception('Timestamp provided is not a valid Timestamp')
    
    month = ts_t.month - 1 - monthback
    year = ts_t.year + month / 12
    month = month % 12 + 1
    day = min(ts_t.day,calendar.monthrange(year,month)[1])
    hour = ts_t.hour
    minute = ts_t.minute
    second = ts_t.second
    ts_f = datetime(year,month,day,hour,minute,second)+relativedelta(months=1)
    
    months=[]
    while(ts_f<=ts_t):
        months.append("%d%02d" % (ts_f.year,ts_f.month))
        ts_f=ts_f+relativedelta(months=1)

    return months


def criteria_querybuilder(criteria_name,table_alias):
    
    # power criteria
    if criteria_name.split("__")[0]=="power":
        # group by constant intervals of power 0-999,1000-1999,2000-2999,3000-3999,... Ex. power__1000
        if len(criteria_name.split("__"))==2:
            sentence = "cast( cast( cast( %s.power as float ) / %s as int ) as string )" % (table_alias, str(criteria_name.split("__")[1]))
        # other cases concerning the power criteria
        else:
            sentence = "%s.power" % (table_alias)
            
    # dwellingArea criteria
    elif criteria_name.split("__")[0]=="dwellingArea":
        # group by constant intervals of dwellingArea 0-49,50-99,100-149,150-199,... Ex. dwellingArea__50
        if len(criteria_name.split("__"))==2:
            sentence = "cast( cast( cast( %s.dwellingArea as float ) / %s as int ) as string )" % (table_alias, str(criteria_name.split("__")[1]))
        # other cases concerning the dwellingArea criteria
        else:
            sentence = "%s.dwellingArea" % (table_alias)
    #
    # ... Remember to add new cases using the elif instance
    #
    # other cases
    else:
        sentence = "%s.%s" % (table_alias, criteria_name)
    
    return sentence


def get_various_days(ts_to,monthback):

    try:
        ts_t = ts_to
    except Exception, e:
        raise Exception('Timestamp provided is not a valid Timestamp')
    
    month = ts_t.month - 1 - monthback
    year = ts_t.year + month / 12
    month = month % 12 + 1
    day = min(ts_t.day,calendar.monthrange(year,month)[1])
    hour = ts_t.hour
    minute = ts_t.minute
    second = ts_t.second
    ts_f = datetime(year,month,day,hour,minute,second)+relativedelta(days=1)
    
    days=[]
    while(ts_f<=ts_t):
        days.append("%d%02d%02d" % (ts_f.year,ts_f.month,ts_f.day))
        ts_f=ts_f+relativedelta(days=1)
    
    return days


def get_utc_timestamp(ts):
    timestamp = int(ts.strftime('%s'))
    return timestamp


def yearmonth_to_datetime(yearmonth):
    """Function to calculate the datetime object referred to a given yearmonth integer\
    e.g. INPUT: 201310 ---> OUTPUT: datetime(2013,10,1,0,0,0)"""
    dt = datetime(int(str(yearmonth)[0:4]),int(str(yearmonth)[4:6]),1,0,0,0)
    return dt


def lastday_lastmonth(ts):
    """Function to calculate the last day of the last month considering a given datetime object\
    e.g. INPUT: datetime(2014,6,23,10,0,0) ---> OUTPUT: datetime(2014,5,31,23,59,59)"""
    ts2=datetime(ts.year,ts.month,1,0,0,0)-timedelta(seconds=1)
    return ts2


def firstday_thismonth(ts,n_months_before=1):
    """Function to calculate the first day of the month (or n_months_before) considering a given datetime object\
    e.g. INPUT: datetime(2014,6,23,10,0,0) ---> OUTPUT: datetime(2014,6,1,0,0,0)"""
    ts2=ts-relativedelta(months=n_months_before)
    ts2=datetime(ts2.year,ts2.month,1,0,0,0)+relativedelta(months=1)
    return ts2


def lastday_thismonth(ts,n_months_after=1):
    """Function to calculate the last day of the month (or n_months_after) considering a given datetime object\
    e.g. INPUT: datetime(2014,6,23,10,0,0) ---> OUTPUT: datetime(2014,6,30,23,59,59)"""
    ts2=ts+relativedelta(months=n_months_after)
    ts2=datetime(ts2.year,ts2.month,1,0,0,0)-relativedelta(seconds=1)
    return ts2


def day_before(ts):
    """Function to calculate the last complete day a given datetime object\
    e.g. INPUT: datetime(2014,6,23,1,0,0) ---> OUTPUT: datetime(2014,6,22,23,59,59)"""
    ts2=datetime(ts.year,ts.month,ts.day,0,0,0)-timedelta(seconds=1)
    return ts2


def day_before_oldResults(ts):
    """Function to calculate the last complete day a given datetime object\
    e.g. INPUT: datetime(2014,6,23,1,0,0) ---> OUTPUT: datetime(2014,6,22,23,59,59)"""
    ts2=ts-timedelta(days=1)
    ts2=int("%d%02d%02d" % (ts2.year,ts2.month,ts2.day))
    return ts2


def monthback_date(ts_p,monthback):
    #Needs datetime.datetime, calendar, time libraries
    ts_p = ts_p + timedelta(minutes=1)
    month = ts_p.month - 1 - monthback
    year = ts_p.year + month / 12
    month = month % 12 + 1
    day = min(ts_p.day,calendar.monthrange(year,month)[1])
    hour = ts_p.hour
    minute = ts_p.minute
    second = 0
    ts_previous = datetime(year,month,day,hour,minute,second)
    return ts_previous


def initial_month(ts_p):
    #Needs datetime.datetime, calendar, time libraries
    
    month = ts_p.month
    year = ts_p.year
    day = 1
    hour = 0
    minute = 0
    second = 0
    ts_previous = datetime(year,month,day,hour,minute,second)+timedelta(minutes=1)
    
    return ts_previous


def firstday_previousyear(ts):
    """Function to calculate the first day of the previous year from a datetime object"""
    return datetime((ts.year-1),1,1,0,0,0)


def firstday_actualyear(ts):
    """Function to calculate the first day of the actual year from a datetime object"""
    return datetime(ts.year,1,1,0,0,0)


def trans_tz_dt(dt,timezone):
    """Enter a defined datetime object and output a datetime object with the same values than the original but with the desired timezone selected"""
    dt2 = datetime(dt.year,dt.month,dt.day,dt.hour,dt.minute,dt.second,dt.microsecond,pytz.UTC)
    dt2 = dt2.astimezone(pytz.timezone(timezone))
    dt2 = datetime(dt.year,dt.month,dt.day,dt.hour,dt.minute,dt.second,dt.microsecond,dt2.tzinfo)
    
    return dt2


def select_json(fields,close_list=True):
    ## This function creates a string to apply in Hive queries which generates a parsable JSON list of documents with the variables that you need
    ##
    ## fields --> List of tuples (name_variable,type_variable,hive_formula)
    ## close_list --> If true, the string of dictionaries will be closed by [ ]. If false, the dictionaries list will not be closed.
    ##                This can be useful when you don't want to close a list in order to add more documents in following queries.
    ##
    #example input // fields = [("contractId","string","contractId"),("sum","float","value"),("month","int","month")], close_list=True
    #example output of this variable of the hive query [{"contractId":"XXX","sum":133,"month":2},{"contractId":"XXX","sum":231,"month":3},
    #                                                   {"contractId":"XXX","sum":231,"month":4},...]
    
    
    quote="'"
    i=0
    for items in fields:
        # Field that opens the dictionary
        if i==0:
            collect_set = '%s{"%s":%s,' % (quote,items[0],quote)
            if items[1] in ["string","str","character"]:
                collect_set = collect_set + 'if(%s is null, "null",concat(%s"%s,cast(%s as string),%s"%s))' %(items[2],quote,quote,items[2],quote,quote)
            if items[1] in ["float","numeric","int","integer"]:
                collect_set = collect_set + 'if(%s is null, "null", %s)' % (items[2],items[2])
            
        # Other fields
        else:
            collect_set = collect_set + ',%s,"%s":%s,' % (quote,items[0],quote)
            if items[1] in ["string","str","character"]:
                collect_set = collect_set + 'if(%s is null, "null",concat(%s"%s,cast(%s as string),%s"%s))' %(items[2],quote,quote,items[2],quote,quote)
            if items[1] in ["float","numeric","int","integer"]:
                collect_set = collect_set + 'if(%s is null, "null", %s)' % (items[2],items[2])
            
        # Close the dictionary    
        if i==(len(fields)-1):
            collect_set = collect_set + ',%s}%s' % (quote,quote)
        
        i+=1
        
    # Definition of the json_query using the collect_set defined before
    json_query = "concat_ws(',' ,collect_set( concat( %s ) ) )" % collect_set
        
    # Close the json_query as a list of dictionaries
    if close_list:
        json_query = 'concat("[", %s ,"]")' % (json_query)    
    return json_query


def select_table_weather(tables_type_weather_calculation,type_weather_calculation):
        tf = [type_weather_calculation in item for item in tables_type_weather_calculation]
        try:
            result = [tables_type_weather_calculation[i] for i in xrange(len(tables_type_weather_calculation)) if tf[i]==True][0]
        except Exception:
            raise Exception('Type of weather calculation not considered. Please add it in the types_of_weather_calculations list of the module')
        return result


def parse_feedback(line, buffer, progress):
    """Return a task progress dictionary from R output. 
    As all task progress information is in different lines, we will create and return a buffer to refeed the function when needed 
    
    Keyword arguments:
    line -- string line from R command
    buffer -- string list returned by function itself
    progress -- dictionary with parsed status progress
    """
    #Set up some string markers to an easy identify of which information we are parsing
    rhstatus_marker = 'Printing rhstatus'
    
    if rhstatus_marker in line:
        logger.debug('rhstatus marker detected. Next line will be a JSON with rhstatus')
        buffer = 'rhstatus'
    elif buffer is not None:
        if buffer == 'rhstatus':
            logger.debug('Parsing JSON with rhstatus from line: %s' % line)
            progress['rhstatus'] = JSONloads(line[5:-2].replace('\\n','').replace('\\',''))
            if 'progress' in progress['rhstatus']:
                progress['rhstatus'].pop('progress')
            if 'counters' in progress['rhstatus']:
                progress['rhstatus'].pop('counters')
            if 'config' in progress['rhstatus']:
                progress['rhstatus'].pop('config')
            if 'jobname' in progress['rhstatus']:
                progress['rhstatus']['started_at'] = datetime.strptime(progress['rhstatus'].pop('jobname'),'%Y-%m-%d %H:%M:%S')
            if 'duration' in progress['rhstatus']:
                progress['rhstatus']['finished_at'] = progress['rhstatus']['started_at'] + relativedelta(seconds=progress['rhstatus'].pop('duration'))
            if 'rerrors' in progress['rhstatus']:
                progress['rhstatus']['errors_found'] = progress['rhstatus'].pop('rerrors')
                if progress['rhstatus']['errors_found'] is False:
                    progress['rhstatus'].pop('errors')
                    progress['rhstatus'].pop('warnings')
            if 'tracking' in progress['rhstatus']:
                progress['rhstatus']['jobname'] = (progress['rhstatus'].pop('tracking')).split('jobid=')[1]
            buffer = None
        # WHEN WE WILL USE RHADOOP WE WILL HAVE TO ADD SUPPORT FOR THE JOB STATS IN THIS LINE 
        else:
            buffer.append(line)
    
    return buffer, progress


def run_rscript(cmd, r_params):
    """Returns a report of Rscript trace
    
    Keyword arguments:
    cmd -- command to call R script (i.e. ['Rscript', '/some/path/to/file.R'])
    r_params -- dict of params to write on subprocess stdin and pass to R  
    """
    report = {}
    
    report['started_at'] =  datetime.now()
    report['status'] = 'launched'
    report['params'] = r_params.copy()
    if 'paths' in report['params']:
        report['params'].pop('paths')
    if 'input' in report['params']:
        report['params'].pop('input')
    if 'output' in report['params']:
        report['params'].pop('output')
    path_R = re.sub(r"(?:/R/)(.*$)","",cmd[1])+'/R/'
    report['script'] = cmd[1].replace(path_R,"")
    
    logger.info('Executing Rscript %s' % cmd[1])
    logger.debug('Parameters provided to Rscript: %s' % r_params)
    
    try:
        sub = Popen(cmd, stdout=PIPE, stderr=PIPE, stdin=PIPE)
        sub.stdin.write(JSONdumps(r_params))
        sub.stdin.close()
    except Exception,e:
        report['status'] = 'failed'
        logger.error('Rscript execution raised an error: %s' % e)
        raise Exception("RScript execution failed: %s " % e)
    else:
        report['finished_at'] = datetime.now()
        report['status'] = 'finished'
        logger.debug('Rscript %s executed successfully' % cmd[1])
    
    # reading output
    buffer = None
    progress = {}
    report['job_status'] = {}
    
    logger.info('Parsing feedback from RScript')
    for line in iter(sub.stdout.readline, b''):
        buffer, progress = parse_feedback(line, buffer, progress)
        if 'rhstatus' in progress:
            # Add rhipe job status into report and clear progress to start new parse
            logger.debug('Rhipe job finished. Adding status to task report')
            progress_job = progress['rhstatus'].copy()
            progress_jobname = progress_job.pop('jobname')
            report['job_status'][progress_jobname] = progress_job
            if progress['rhstatus'].get('errors_found') is True:
                report['status'] = 'failed'
            process={}

    errors = []
    for error in sub.stderr:
        errors.append(error)
    
    if len(errors) > 0:
        logger.info('There were R errors')
        report['stderr'] = []
        for er in errors[-4:]:
            # remove configuration warnings and rhclean trace which don't give valuable information
            if not 'WARN conf.Configuration' in er and not 'Deleted' in er:
                report['stderr'].append(er)
        # if cleanup didn't left any item inside stderr, remove it from report
        if not report['stderr']:
            del report['stderr']

    if report['status']=='failed':
        raise Exception("RScript %s execution failed" % cmd[1])

    return report


def build_cf_list(table):
    """Returns a list with elements like: 'cf:column'
    It will be useful to build schema-agnostic bulk process
    
    Keyword arguments:
    table -- python dictionary (from module config) with HBase table description (name, cfs and its fields)  
    """
    list = []
    for cf in table['cf']:
        for field in cf['fields']:
            list.append(cf['name']+':'+field)
    return list  


"""
Context dependent functions
"""


def update_current_task_progress(context):
    """Returns an updated context with an updated task state 
    
    Keyword arguments:
    context -- dictionary with task context
    """
    context['job_meta']['current'] += 1
    context['report']['progress'] = ( float(context['job_meta']['current']) / float(context['job_meta']['total']) ) * 100
    context['report'].update()
    
    context['current_task'].update_state(state='PROGRESS', meta=context['job_meta'])
    return context


def cleanup_temp_data(context, paths, recurse=False):
    """Deletes all files in specified paths
    
    Keyword arguments:
    context -- task context containing hdfs snakebite client
    paths -- a list of paths to be deleted 
    recurse -- boolean to perform a recursive delete (defaults to False)
    """
    
    logger.info('Cleaning temporary data from HDFS')
    # Connect to hdfs with snakebite library
    if type(paths) is not list:
        paths = [paths]
    
    report = {}       
    report['started_at'] = datetime.now()
    report['status'] = 'started'
    report['paths'] = paths
    
    logger.debug('Removing folder/files from HDFS: %s Recursive mode: %s' % (paths, recurse))
    
    for delete in context['clients']['hdfs'].delete(paths, recurse=recurse):
        try:
            # Use the cursor just to delete specified path
            delete
        except Exception,e:
            raise Exception("Can't delete specified paths '%s': %s " % (paths, e))
    
    report['finished_at'] = datetime.now()
    report['status'] = 'finished'
    
    logger.info('Temporary data removed from HDFS')
    
    return report


def ls_path_in_hdfs(context, paths):
    """Returns a list of the directories or files inside one (or various) various path(s)
    
    Keyword arguments:
    path -- string (or list) of path(s) where the funcion have to search
    """
    if type(paths) is not list:
        paths = [paths]

    logger.debug('Searching folder/files in hdfs://%s' % (paths))
    
    result = [item['path'] for item in list(context['clients']['hdfs'].ls(paths))]
    if len(result)==0:
        result = None
    
    return result 


def load_output_to_hbase(context, input, table, sep=","):
    """Creates a MR job that inserts input records into an HBase table.
    
    Keyword arguments:
    context -- dictionary task context
    input -- HDFS text file or folder containing records to be loaded into HBase (CSV)
    table -- HBase table description, with hbase table name, column families and its fields
    """
    
    report = {}
    report['started_at'] = datetime.now()
    report['status'] = 'setup'
    report['table'] = table
    #report['consider_setupId'] = consider_setupId
    
    logger.info('Saving output file (from MapReduce Job) into HBase with mrjob ')
    logger.debug('Input file: %s' % input)
    logger.debug('HBase table description: %s' % table)
    
    job_conf = {
                'hbase_host': context['config']['app']['hbase']['host'],
                'hbase_port': context['config']['app']['hbase']['port'],
                'hbase_table': build_cf_list(table),
                'hbase_table_name': table['name'],
                'hbase_table_cf': [item['name'] for item in table['cf']] if 'cf' in table else ['results'],
                'sep': sep
                }
    if 'setupId' in context['report']['params'][0]:
        job_conf.update({'setupId': str(context['report']['params'][0]['setupId'])})
        
    f = NamedTemporaryFile(delete=False, suffix='.json')
    f.write(JSONdumps(job_conf))
    f.close()
    
    logger.debug('Created temporary config file to upload into hadoop and read from job')
    mr_job = HDFSToHBase(args=['-r', 'hadoop', input, '--file', f.name])
    with mr_job.make_runner() as runner:
        try:
            runner.run()
        except Exception,e:
            raise Exception('Error running MRJob load_output_to_hbase: %s' % e)

    logger.info('Saved output file into HBase table (%s) successfully' % table['name'])                
    f.unlink(f.name)
    logger.debug('Temporary config file uploaded has been deleted from FileSystem')
    
    report['finished_at'] = datetime.now()
    report['status'] = 'finished'
    
    return report


def load_output_to_rest_data(context, input, fields, exists_setupId=True, collection=None, mongo_params=None,ts_from_remove=None, ts_to_remove=None, company_to_remove=None,
                             type_to_remove=None, key=['month','companyId','contractId','setupId'],update=False,sep=","):
    """Creates a MR job that inserts input records into a MongoDB collection
    
    Keyword arguments:
    context -- dictionary task context
    input -- HDFS text file or folder containing records to be loaded into HBase (CSV)
    fields -- List of collection field names
    """
    
    report = {}
    report['started_at'] = datetime.now()
    report['status'] = 'setup'
    report['fields'] = fields
    
    logger.info('Saving output file (from MapReduce Job) into REST with mrjob ')
    logger.debug('Input file: %s' % input)
    logger.debug('REST resource fields: %s' % fields)
    
    if collection is None:
        collection = context['config']['module']['public']['mongodb']['collection']
        
    job_conf = {
                'mongo_host': mongo_params['host'] if mongo_params else context['config']['app']['mongodb']['host'],
                'mongo_port': mongo_params['port'] if mongo_params else context['config']['app']['mongodb']['port'],
                'mongo_user': mongo_params['username'] if mongo_params else context['config']['app']['mongodb']['username'],
                'mongo_pass': mongo_params['password'] if mongo_params else context['config']['app']['mongodb']['password'],
                'mongo_db': mongo_params['db'] if mongo_params else context['config']['app']['mongodb']['db'],
                'mongo_collection': collection,
                'mongo_params': mongo_params,
                'fields': fields,
                'key': key,
                'sep': sep,
                'update': update
                }
    if 'setupId' in context['report']['params'][0]:
        job_conf.update({'setupId': str(context['report']['params'][0]['setupId'])})
    
    f = NamedTemporaryFile(delete=False, suffix='.json')
    f.write(JSONdumps(job_conf))
    f.close()
    
    logger.debug('Created temporary config file to upload into hadoop and read from job')

    #Remove old results (not update)-----> ts_from_remove=None, ts_to_remove=None, company_to_remove=None (in argument of the function)
    if ts_from_remove and ts_to_remove and company_to_remove:
        rest = MongoClient(job_conf['mongo_host'], job_conf['mongo_port'])
        rest[job_conf['mongo_db']].authenticate(job_conf['mongo_user'],job_conf['mongo_pass'])
        connection = rest[job_conf['mongo_db']]
        if isinstance(ts_to_remove,types.ListType):
            for i in range(len(ts_to_remove)):
                #If there is not the setupId in the results of Mongo, you have to add the argument exists_setupId=False in the definition of the function
                if exists_setupId is True:
                    if type_to_remove:
                        docs_to_find = {
                                   'setupId': ObjectId(job_conf['setupId']),
                                   'companyId': int(company_to_remove),
                                   'type': str(type_to_remove),
                                   key[0]: {"$gte":ts_from_remove[i],"$lte":ts_to_remove[i]}
                                   }
                    else:
                        docs_to_find = {
                                   'setupId': ObjectId(job_conf['setupId']),
                                   'companyId': int(company_to_remove),
                                   key[0]: {"$gte":ts_from_remove[i],"$lte":ts_to_remove[i]}
                                   }
                else:
                    if type_to_remove:
                        docs_to_find = {
                                       'companyId': int(company_to_remove),
                                       'type': str(type_to_remove),
                                       key[0]: {"$gte":ts_from_remove[i],"$lte":ts_to_remove[i]}
                                       }
                    else:
                        docs_to_find = {
                                       'companyId': int(company_to_remove),
                                       key[0]: {"$gte":ts_from_remove[i],"$lte":ts_to_remove[i]}
                                       }
                connection[job_conf['mongo_collection']].remove(docs_to_find)
        else:
            #If there is not the setupId in the results of Mongo, you have to add the argument exists_setupId=False in the definition of the function
            if exists_setupId is True:
                if type_to_remove:
                    docs_to_find = {
                                   'setupId': ObjectId(job_conf['setupId']),
                                   'companyId': int(company_to_remove),
                                   'type': str(type_to_remove),
                                   key[0]: {"$gte":ts_from_remove,"$lte":ts_to_remove}
                                   }
                else:
                    docs_to_find = {
                                   'setupId': ObjectId(job_conf['setupId']),
                                   'companyId': int(company_to_remove),
                                   key[0]: {"$gte":ts_from_remove,"$lte":ts_to_remove}
                                   }
            else:
                if type_to_remove:
                    docs_to_find = {
                                   'companyId': int(company_to_remove),
                                   'type': str(type_to_remove),
                                   key[0]: {"$gte":ts_from_remove,"$lte":ts_to_remove}
                                   }
                else:
                    docs_to_find = {
                                   'companyId': int(company_to_remove),
                                   key[0]: {"$gte":ts_from_remove,"$lte":ts_to_remove}
                                   }
            connection[job_conf['mongo_collection']].remove(docs_to_find)
        
    report['status'] = 'launched'
    mr_job = HDFSToREST(args=['-r', 'hadoop', '--file', f.name, input])

    with mr_job.make_runner() as runner:
        try:
            runner.run()
        except Exception,e:
            raise Exception('Error running MRJob load_output_to_rest_data: %s' % e)

    logger.info('Saved output file into REST collection (%s) successfully' % collection)
    
    f.unlink(f.name)
    logger.debug('Temporary config file uploaded has been deleted from FileSystem')
    
    report['finished_at'] = datetime.now()
    report['status'] = 'finished'
    
    return report

        
def get_companies_for_module(context, module):
    """Returns a list of companies ids that has a specific module to calculate
    
    Keyword arguments:
    module -- String with module name to search
    
    """
    db = context['clients']['mongo']
    db = db[context['config']['app']['mongodb']['db']]
    result = []
    
    logger.info('Querying mongo to get companies who implement module %s' % module)
    
    for x in db.companies.find({'modules':{'$in':[module]}},{'id':1}):
        result.append(x['id'])

    return result


def cleanup_deleted_contracts_data_from_rest(context, companyId):
    """
    
    Removes documents of deleted contracts in MongoDB
    
    Keyword arguments:
    context -- that contains information for mongo connection, collections and its TTL 
    contracts -- array of contractId's to delete his data from the desired collection
    
    """
    
    logger.info('Removing deleted contracts data from REST')
    
    report = {}
    report['started_at'] = datetime.now()
    
    contracts = subprocess.Popen(["hadoop", "fs", "-cat", "/tmp/contracts_comparison/%s/deleted/000000_0" % companyId], stdout=subprocess.PIPE).stdout.readlines()
    for i in range(len(contracts)):
        contracts[i] = contracts[i].replace("\n","")
    
    # Remove MongoDB query
    try:
        db = context['clients']['mongo'][context['config']['app']['mongodb']['db']]
        result = db[context['config']['module']['public']['mongodb']['collection']].remove({'contractId': {'$in': contracts},'companyId':companyId})
    except Exception, e:
        raise Exception('Error removing deleted contracts data from REST %s ' % e)
    
    logger.info('Deleted contracts data removed from REST successfully')

    report['finished_at'] = datetime.now()
    
    return report
    

def cleanup_old_data_from_rest(context, time_representation, type="month", collection = None, app=False, energy_type=None):
    """Removes old documents from MongoDB
    
    Keyword arguments:
    context -- that contains information for mongo connection, collections and its TTL 
    time_representation:
        month -- Integer result of get_month() function
        day -- Integer result of get_day() function
        _created -- Datetime
    type: [month,day,_created]
    app: if the collection is in the app server
    energy_type: if we have to consider the energy_type in the delete process
    """
    
    logger.info('Removing old data from REST')
    
    report = {}
    report['started_at'] = datetime.now()
    
    # If the collection is not assigned in the arguments of the function, consider the collection especified in the mongodb dictionary of the module config.json 
    if collection is None:
        collection = context['config']['module']['public']['mongodb']['collection']
    
    if type=="month":
        # Calculate the first time representation valid for the REST collection we are treating
        month_to_remove = datetime.strptime(str(time_representation),'%Y%m') - relativedelta(months=context['config']['module']['public']['mongodb']['ttl'])
        month_to_remove = int(month_to_remove.strftime('%Y%m'))
        logger.debug('Removing old data from REST where month < %s' % month_to_remove)
        # Remove MongoDB query
        try:
            db = context['clients']['mongo'][context['config']['app']['mongodb']['db']] if app is False else context['clients']['mongo_app'][context['config']['app']['mongodb2']['db']]
            if energy_type is None:
                result = db[collection].remove({'month':{'$lte': month_to_remove}})
            else:
                result = db[collection].remove({'month':{'$lte': month_to_remove},'type':energy_type})
        except Exception, e:
            raise Exception('Error removing old data from REST %s ' % e)
    
    elif type=="day":
        # Calculate the first time representation valid for the REST collection we are treating
        days_to_remove = datetime.strptime(str(time_representation),'%Y%m%d') - relativedelta(months=context['config']['module']['public']['mongodb']['ttl'])
        days_to_remove = int(days_to_remove.strftime('%Y%m%d'))
        logger.debug('Removing old data from REST where days < %s' % days_to_remove)
        # Remove MongoDB query
        try:
            db = context['clients']['mongo'][context['config']['app']['mongodb']['db']] if app is False else context['clients']['mongo_app'][context['config']['app']['mongodb2']['db']]
            if energy_type is None:
                result = db[collection].remove({'day':{'$lte': days_to_remove}})
            else:
                result = db[collection].remove({'day':{'$lte': days_to_remove},'type':energy_type})
        except Exception, e:
            raise Exception('Error removing old data from REST %s ' % e)
    
    elif type=="_created":
        # Calculate the first time representation valid for the REST collection we are treating
        ts_to_remove = time_representation - relativedelta(months=context['config']['module']['public']['mongodb']['ttl'])
        logger.debug('Removing old data from REST where _created < %s' % ts_to_remove)
        # Remove MongoDB query
        try:
            db = context['clients']['mongo'][context['config']['app']['mongodb']['db']] if app is False else context['clients']['mongo_app'][context['config']['app']['mongodb2']['db']]
            if energy_type is None:
                result = db[collection].remove({'_created':{'$lte': ts_to_remove}})
            else:
                result = db[collection].remove({'_created':{'$lte': ts_to_remove},'type':energy_type})
        except Exception, e:
            raise Exception('Error removing old data from REST %s ' % e)
        
    logger.info('Old data removed from REST successfully')

    report['finished_at'] = datetime.now()
    report['result'] = result
    return report


def create_customersAux_temp_table(context, companyId, id_task = None):
    logger.debug('Creating HIVE temporary table to query contracts: %s' % companyId)
    table_name = 'customersAux_'+str(companyId)
    if id_task is None or id_task != "":
        table_name_uuid = 'customersAux_'+str(companyId)
    else:
        table_name_uuid = 'customersAux_'+str(companyId)+'_'+str(id_task)
    sentence = "CREATE EXTERNAL TABLE IF NOT EXISTS\
                %s (key struct<company:string,contractId:string>,\
                payerId string, ownerId string, signerId string, power int, dateStart timestamp, dateEnd timestamp, customerId string,\
                meteringPointId string, activityCode string, version int, tariffId string, stationId string,\
                criteria_1 string, criteria_2 string, criteria_3 string, ot701 string, last_access bigint,\
                dwellingAirConditioning int, dwellingElectricCooking int, dwellingOtherKitchen int, dwellingOtherAppliances int,\
                dwellingAllHeatingSources string,buildingId string, city string, cityCode string, country string, countryCode string,\
                street string, postalCode string, province string, provinceCode string, parcelNumber string,\
                totalPersonsNumber int, minorsPersonsNumber int, workingAgePersonsNumber int, retiredAgePersonsNumber int,\
                malePersonsNumber int, femalePersonsNumber int, buildingConstructionYear int, dwellingArea float,\
                buildingType string,dwellingPositionInBuilding string, dwellingOrientation string, buildingWindowsType string,\
                buildingWindowsFrame string, buildingHeatingSource string, buildingHeatingSourceDhw string, buildingSolarSystem string)\
                ROW FORMAT DELIMITED\
                COLLECTION ITEMS TERMINATED BY '~'\
                STORED BY 'org.apache.hadoop.hive.hbase.HBaseStorageHandler'\
                WITH SERDEPROPERTIES\
                ('hbase.columns.mapping' = ':key, info:payerId, info:ownerId, info:signerId, info:power, info:dateStart, info:dateEnd,\
                info:customerId, info:meteringPointId, info:activityCode, info:version, info:tariffId, info:weatherStationId,\
                info:criteria_1, info:criteria_2, info:criteria_3, info:OT701, info:last_access, info:dwellingAirConditioning,\
                info:dwellingElectricCooking, info:dwellingOtherKitchen,info:dwellingOtherAppliances, info:dwellingAllHeatingSources,\
                address:buildingId, address:city, address:cityCode, address:country, address:countryCode,address:street, address:postalCode,\
                address:province, address:provinceCode, address:parcelNumber,profile:totalPersonsNumber, profile:minorsPersonsNumber,\
                profile:workingAgePersonsNumber, profile:retiredAgePersonsNumber,profile:malePersonsNumber, profile:femalePersonsNumber,\
                profile:buildingConstructionYear, profile:dwellingArea, profile:buildingType, profile:dwellingPositionInBuilding,\
                profile:dwellingOrientation, profile:buildingWindowsType,profile:buildingWindowsFrame, profile:buildingHeatingSource,\
                profile:buildingHeatingSourceDhw, profile:buildingSolarSystem')\
                TBLPROPERTIES ('hbase.table.name' = '%s')" % (table_name_uuid,table_name)
    try:            
        context['clients']['hive'].execute(sentence)
        logger.debug('Created HIVE temporary table %s' % table_name_uuid)
    except Exception, e:
        logger.error('Failed to create HIVE temporary table %s: %s' % (table_name_uuid, e))
        raise Exception('Failed to create HIVE temporary table %s: %s' % (table_name_uuid, e))
    

    if 'temp_hive_tables' in context:
        context['temp_hive_tables']['input'].append(table_name_uuid)
    else:
        context['temp_hive_tables'] = {'measures':[],'input':[]}
        context['temp_hive_tables']['input'] = [table_name_uuid]
        
    return table_name_uuid


def create_auxiliar_table_from_local_textfile(context, path, name, fields, sep=",", id_task = None):
    logger.debug('Creating HIVE temporary table from local textfile in path: %s' % path)

    if id_task is None:
        table_name_uuid = name
    else:
        table_name_uuid = name+'_'+str(id_task)
    
    # Definition of fields in a hive format
    fields_str = ",".join(['%s %s' % (item[0],item[1]) for item in fields])
    
    # Creation of the hive table
    sentence = "CREATE TABLE %s ( %s ) ROW FORMAT DELIMITED FIELDS TERMINATED BY '%s' STORED AS TEXTFILE" % (table_name_uuid,fields_str,sep)
    try:
        context['clients']['hive'].execute(sentence)
        logger.debug('Created HIVE temporary table %s' % table_name_uuid)
    except Exception, e:
        logger.error('Failed to create HIVE temporary table %s: %s' % (table_name_uuid, e))
        raise Exception('Failed to create HIVE temporary table %s: %s' % (table_name_uuid, e))
    
    # Load the data from local textfile
    sentence = "LOAD DATA LOCAL INPATH '%s' INTO TABLE %s" % (path,table_name_uuid)
    try:            
        context['clients']['hive'].execute(sentence)
        logger.debug('Data from local textfile loaded in %s' % table_name_uuid)
    except Exception, e:
        logger.error('Data from local textfile not loaded in table %s: %s' % (table_name_uuid, e))
        raise Exception('Data from local textfile not loaded in table %s: %s' % (table_name_uuid, e))
    

    if 'temp_hive_tables' in context:
        context['temp_hive_tables']['input'].append(table_name_uuid)
    else:
        context['temp_hive_tables'] = {'measures':[],'input':[]}
        context['temp_hive_tables']['input'] = [table_name_uuid]
        
    return table_name_uuid


def create_measures_temp_table(context, reading, companyId, id_task, cumulative = False, tertiary=False, tariffs=False, daily_cumulative = False, number_period_tertiary = 6, for_delete_process = False):
    logger.debug('Creating HIVE temporary table to query measures: %s' % reading)
    
    if tariffs==False and cumulative==True and tertiary==False and daily_cumulative==False:
        table_name = reading+'_'+str(companyId)
        if id_task!="":
            table_name_uuid = reading+'_'+str(companyId)+'_'+id_task
        else:
            table_name_uuid = reading+'_'+str(companyId)
        sentence = "CREATE EXTERNAL TABLE IF NOT EXISTS \
             %s( key struct<b:tinyint,ts:bigint,contractid:string>, value float, accumulated float, calculated int ) \
             ROW FORMAT DELIMITED \
             COLLECTION ITEMS TERMINATED BY '~' \
             STORED BY \
             'org.apache.hadoop.hive.hbase.HBaseStorageHandler' \
             WITH SERDEPROPERTIES \
             ('hbase.columns.mapping' = ':key, m:v, m:va, m:calc') \
             TBLPROPERTIES \
             ('hbase.table.name' = '%s')" % (table_name_uuid, table_name)

    if tariffs==False and cumulative==False and tertiary==True and daily_cumulative==False :
        table_name = reading+'_'+str(companyId)
        if id_task!="":
            table_name_uuid = reading+'_'+str(companyId)+'_'+id_task
        else:
            table_name_uuid = reading+'_'+str(companyId)
        mapping_hive = ['p'+str(i)+' float, p'+str(i)+'a float' for i in xrange(1,number_period_tertiary+1)]
        mapping_hbase = ['m:p'+str(i)+', m:p'+str(i)+'a' for i in xrange(1,number_period_tertiary+1)]
        sentence = "CREATE EXTERNAL TABLE IF NOT EXISTS \
             %s( key struct<b:tinyint,ts:bigint,contractid:string>, %s, value float, calculated int ) \
             ROW FORMAT DELIMITED \
             COLLECTION ITEMS TERMINATED BY '~' \
             STORED BY \
             'org.apache.hadoop.hive.hbase.HBaseStorageHandler' \
              WITH SERDEPROPERTIES \
              ('hbase.columns.mapping' = ':key, %s, m:v, m:calc') \
              TBLPROPERTIES \
              ('hbase.table.name' = '%s')" % (table_name_uuid, ", ".join(mapping_hive), ", ".join(mapping_hbase), table_name)
    
    if tariffs==False and daily_cumulative==True and tertiary==False:
        table_name = reading+'_'+str(companyId)
        if id_task!="":
            table_name_uuid = reading+'_'+str(companyId)+'_'+id_task
        else:
            table_name_uuid = reading+'_'+str(companyId)
        sentence = "CREATE EXTERNAL TABLE IF NOT EXISTS \
             %s( key struct<b:tinyint,ts:bigint,contractid:string>, value_daily float, value float, days int ) \
             ROW FORMAT DELIMITED \
             COLLECTION ITEMS TERMINATED BY '~' \
             STORED BY \
             'org.apache.hadoop.hive.hbase.HBaseStorageHandler' \
             WITH SERDEPROPERTIES \
             ('hbase.columns.mapping' = ':key, m:vd, m:v, m:nd') \
             TBLPROPERTIES \
             ('hbase.table.name' = '%s')" % (table_name_uuid, table_name)
    
    if tariffs==True and tertiary==False:
        table_name = reading+'_'+str(companyId)
        if id_task!="":
            table_name_uuid = reading+'_'+str(companyId)+'_'+id_task
        else:
            table_name_uuid = reading+'_'+str(companyId)
        sentence = "CREATE EXTERNAL TABLE IF NOT EXISTS \
             %s( key struct<b:tinyint,ts:bigint,contractId:string>, value float, accumulated float,\
             calculated int, energyCost float, flatRate float, timeSlotName string, periodKey string) \
             ROW FORMAT DELIMITED COLLECTION ITEMS TERMINATED BY '~' STORED BY \
             'org.apache.hadoop.hive.hbase.HBaseStorageHandler' WITH SERDEPROPERTIES \
             ('hbase.columns.mapping' = ':key, m:v, m:va, m:calc, m:energyCost, m:flatRate, m:timeSlotName, m:periodKey')\
              TBLPROPERTIES ('hbase.table.name' = '%s')" % (table_name_uuid, table_name)

    if tariffs==False and cumulative==False and tertiary==False and daily_cumulative==False:
        table_name = reading+'_'+str(companyId)
        if id_task!="":
            table_name_uuid = reading+'_'+str(companyId)+'_'+id_task
        else:
            table_name_uuid = reading+'_'+str(companyId)
        sentence = "CREATE EXTERNAL TABLE IF NOT EXISTS \
             %s( key struct<b:tinyint,ts:bigint,contractid:string>, value float, calculated int ) \
             ROW FORMAT DELIMITED \
             COLLECTION ITEMS TERMINATED BY '~' \
             STORED BY \
             'org.apache.hadoop.hive.hbase.HBaseStorageHandler' \
              WITH SERDEPROPERTIES \
              ('hbase.columns.mapping' = ':key, m:v, m:calc') \
              TBLPROPERTIES \
              ('hbase.table.name' = '%s')" % (table_name_uuid, table_name)
    
    if for_delete_process==True:
        table_name = reading+'_'+str(companyId)
        if id_task!="":
            table_name_uuid = reading+'_'+str(companyId)+'_'+id_task
        else:
            table_name_uuid = reading+'_'+str(companyId)
        mapping_hive = ['p'+str(i)+' float, p'+str(i)+'a float' for i in xrange(1,number_period_tertiary+1)]
        mapping_hbase = ['m:p'+str(i)+', m:p'+str(i)+'a' for i in xrange(1,number_period_tertiary+1)]
        sentence = "CREATE EXTERNAL TABLE IF NOT EXISTS \
             %s( key struct<b:tinyint,ts:bigint,contractid:string>, %s, value float, accumulated float, calculated int ) \
             ROW FORMAT DELIMITED \
             COLLECTION ITEMS TERMINATED BY '~' \
             STORED BY \
             'org.apache.hadoop.hive.hbase.HBaseStorageHandler' \
              WITH SERDEPROPERTIES \
              ('hbase.columns.mapping' = ':key, %s, m:v, m:va, m:calc') \
              TBLPROPERTIES \
              ('hbase.table.name' = '%s')" % (table_name_uuid, ", ".join(mapping_hive), ", ".join(mapping_hbase), table_name)
    
    try:
        context['clients']['hive'].execute(sentence)
        logger.debug('Created HIVE temporary table %s' % table_name_uuid)
    except Exception, e:
        logger.error('Failed to create HIVE temporary table %s: %s' % (table_name_uuid, e))
        raise Exception('Failed to create HIVE temporary table %s: %s' % (table_name_uuid, e))
    

    if 'temp_hive_tables' in context:
        context['temp_hive_tables']['measures'].append(table_name_uuid)
    else:
        context['temp_hive_tables'] = {'measures':[],'input':[]}
        context['temp_hive_tables']['measures'] = [table_name_uuid]
        
    return table_name_uuid


def create_weather_measures_temp_table(context, reading, id_task):
    logger.debug('Creating HIVE temporary table to query measures: %s' % reading)
    table_name = reading
    if id_task!="":
        table_name_uuid = reading+'_'+id_task
    else:
        table_name_uuid = reading
    sentence = "CREATE EXTERNAL TABLE IF NOT EXISTS \
         %s( key struct<b:tinyint,ts:bigint,stationId:string>, value float ) \
         ROW FORMAT DELIMITED \
         COLLECTION ITEMS TERMINATED BY '~' \
         STORED BY \
         'org.apache.hadoop.hive.hbase.HBaseStorageHandler' \
          WITH SERDEPROPERTIES \
          ('hbase.columns.mapping' = ':key, m:v') \
          TBLPROPERTIES \
          ('hbase.table.name' = '%s')" % (table_name_uuid, table_name)
                    
    try:            
        context['clients']['hive'].execute(sentence)
        logger.debug('Created HIVE temporary table %s' % table_name_uuid)
    except Exception, e:
        logger.error('Failed to create HIVE weather temporary table %s: %s' % (table_name_uuid, e))
        raise Exception('Failed to create HIVE weather temporary table %s: %s' % (table_name_uuid, e))
    
    if 'temp_hive_tables' in context:
        context['temp_hive_tables']['measures'].append(table_name_uuid)
    else:
        context['temp_hive_tables'] = {'measures':[],'input':[]}
        context['temp_hive_tables']['measures'] = [table_name_uuid]

    return table_name_uuid


def create_module_oldResults_temp_table(context, module, id_task, fields, key_fields=[('month','bigint'),('companyId','string'),('contractId','string'),('setupId','string')]):
    logger.debug('Creating HIVE temporary table to query old results of module %s' % module)
    table_name = module
    if id_task!="":
        table_name_uuid = 'oldResults_'+module+'_'+id_task
    else:
        table_name_uuid = 'oldResults_'+module
    
    definition_key=[]
    definition_columns=[':key']
    definition_fields=[]
    if fields is not None:
        for field in fields:
            definition_columns.append('results:'+field[0])
            definition_fields.append(field[0]+' '+field[1])
    for key in key_fields:
        definition_key.append(key[0]+':'+key[1])
    definition_columns = ",".join(definition_columns)
    definition_fields = ",".join(definition_fields)
    definition_key = ",".join(definition_key)
    
    sentence = "CREATE EXTERNAL TABLE IF NOT EXISTS \
             %s( key struct<%s>, %s ) \
             ROW FORMAT DELIMITED \
             COLLECTION ITEMS TERMINATED BY '~' \
             STORED BY \
             'org.apache.hadoop.hive.hbase.HBaseStorageHandler' \
              WITH SERDEPROPERTIES \
              ('hbase.columns.mapping' = '%s') \
              TBLPROPERTIES \
              ('hbase.table.name' = '%s')" % (table_name_uuid, definition_key, definition_fields, definition_columns, table_name)
                              
    try:            
        context['clients']['hive'].execute(sentence)
        logger.debug('Created HIVE temporary table %s' % table_name_uuid)
    except Exception, e:
        logger.error('Failed to create HIVE temporary table to query old results - %s: %s' % (table_name_uuid, e))
        raise Exception('Failed to create HIVE temporary table to query old results - %s: %s' % (table_name_uuid, e))
    
    if 'temp_hive_tables' in context:
        context['temp_hive_tables']['input'].append(table_name_uuid)
    else:
        context['temp_hive_tables'] = {'measures':[],'input':[]}
        context['temp_hive_tables']['input'] = [table_name_uuid]

    return table_name_uuid


def create_summary_results_temp_table(context, module, columns, key=[("month","bigint"),("companyId","bigint"),("criteria","string"),("groupCriteria","string"),("setupId","string")]):
    logger.debug('Creating HIVE temporary table to query summary results of module %s' % module)
    table_name = 'summaryGroupingCriteria_'+module
    
    definition_key = ",".join([str(item[0])+':'+str(item[1]) for item in key])
    definition_columns=[':key']
    definition_fields=[]
    for i in xrange(0,len(columns)):
        definition_columns.append('results:'+columns[i])
        definition_fields.append(columns[i]+' float')
    definition_columns=", ".join(definition_columns)
    definition_fields=", ".join(definition_fields)
    
    sentence = "CREATE EXTERNAL TABLE IF NOT EXISTS \
         %s( key struct<%s>, %s ) \
         ROW FORMAT DELIMITED \
         COLLECTION ITEMS TERMINATED BY '~' \
         STORED BY \
         'org.apache.hadoop.hive.hbase.HBaseStorageHandler' \
          WITH SERDEPROPERTIES \
          ('hbase.columns.mapping' = '%s') \
          TBLPROPERTIES \
          ('hbase.table.name' = '%s')" % (table_name, definition_key, definition_fields, definition_columns, table_name)
    
    
    try:            
        context['clients']['hive'].execute(sentence)
        logger.debug('Created HIVE temporary table %s' % table_name)
    except Exception, e:
        logger.error('Failed to create HIVE temporary table to query summary results - %s: %s' % (table_name, e))
        raise Exception('Failed to create HIVE temporary table to query summary results - %s: %s' % (table_name, e))
    
    if 'temp_hive_tables' in context:
        context['temp_hive_tables']['input'].append(table_name)
    else:
        context['temp_hive_tables'] = {'measures':[],'input':[]}
        context['temp_hive_tables']['input'] = [table_name]

    return table_name


def delete_measures_temp_table(context, table_name):    
    sentence = "DROP TABLE %s" % table_name
    try:            
        context['clients']['hive'].execute(sentence)
        logger.debug('Deleted HIVE temporary table %s' % table_name)
    except Exception, e:
        logger.error('Failed to delete HIVE temporary table %s: %s' % (table_name, e))
        raise Exception('Failed to delete HIVE temporary table %s: %s' % (table_name, e))
    
    return 'done'


def create_hive_module_input_table(context, name, location, fields, id_task, sep="\t"):
    '''
    CREATE EXTERNAL TABLE OT201Input (customerid string, actual float, previous float, difference float)
    ROW FORMAT DELIMITED
    FIELDS TERMINATED BY '\t'
    STORED AS TEXTFILE LOCATION '/tmp/ot201/input_from_query';
    fields = [('customerid','string'), ('actual', 'float')]
    '''
    definition = []
    
    #Name with the uuid of the task
    name = name+'_'+id_task
    
    for field in fields:
        definition.append(field[0]+' '+field[1])
    
    definition = ','.join(definition)    
    sentence = "CREATE EXTERNAL TABLE IF NOT EXISTS %s (%s) \
                ROW FORMAT DELIMITED \
                FIELDS TERMINATED BY '%s' \
                STORED AS TEXTFILE LOCATION '%s'" % (name, definition, sep, location)
                
    try:            
        context['clients']['hive'].execute(sentence)
        logger.debug('Created HIVE external table %s to output result on HDFS %s' % (name, location))
    except Exception, e:
        logger.error('Failed to create HIVE input table %s: %s' % (name, e))
        raise Exception('Failed to create HIVE input table %s: %s' % (name, e))
    
    if 'temp_hive_tables' in context:
        context['temp_hive_tables']['input'].append(name)
    else:
        context['temp_hive_tables'] = {'measures':[],'input':[]}
        context['temp_hive_tables']['input'] = [name]
    
    return name


def load_table_from_a_text_file_to_hive(context, name, location, separator, fields, id_task):
    '''
    CREATE EXTERNAL TABLE ... ( Key string,Ed float,Edwd float,...)
    ROW FORMAT DELIMITED
    FIELDS TERMINATED BY ","
    LOCATION '/tmp/ot401/TEST2/result';
    '''
    definition = []
    
    #Name with the uuid of the task
    if id_task=="":
        name = name
    else:
        name = name+'_'+id_task
    
    for field in fields:
        definition.append(field[0]+' '+field[1])
    
    definition = ','.join(definition)
    sentence = "CREATE EXTERNAL TABLE IF NOT EXISTS %s (%s) \
                ROW FORMAT DELIMITED \
                FIELDS TERMINATED BY '%s' \
                LOCATION '%s'" % (name,definition,separator,location)
    logger.debug(sentence)            
    try:            
        context['clients']['hive'].execute(sentence)
        logger.debug('Created HIVE external table %s to output result on HDFS %s' % (name, location))
    except Exception, e:
        logger.error('Error in the loading process of the text file in HDFS to the HIVE table %s: %s' % (name, e))
        raise Exception('Error in the loading process of the text file in HDFS to the HIVE table %s: %s' % (name, e))
        
    if 'temp_hive_tables' in context:
        context['temp_hive_tables']['input'].append(name)
    else:
        context['temp_hive_tables'] = {'measures':[],'input':[]}
        context['temp_hive_tables']['input'] = [name]
        
    return name


def delete_hive_module_input_table(context, name):
    sentence = "DROP TABLE %s" % name
    try:            
        context['clients']['hive'].execute(sentence)
        logger.debug('Deleted HIVE external table %s' % name)
    except Exception, e:
        logger.error('Failed to delete HIVE input table %s: %s' % (name, e))
        raise Exception('Failed to delete HIVE input table %s: %s' % (name, e))


def create_buffer_mt(cursor):
    """Accumulates cursor documents until we fill the buffer or there are no more documents on cursor"""
    buffer = []
    while cursor:
        try:
            buffer.append(cursor.next())
        except StopIteration, e:
            break
    
    return buffer


def create_hive_partitioned_table(context, type, companyId, table_fields, partitioner_fields, location, table_needs_to_be_recreated, sep='\t'):
    
    # Name of the table
    name = "%s_%s_HDFS" % (type,str(companyId))
    
    # Table and partitioner fields format correction for the HIVE sentence
    t_fields = []
    for field in table_fields:
        t_fields.append(field[0]+' '+field[1])
    p_fields = []
    for field in partitioner_fields:
        p_fields.append(field[0]+' '+field[1])
        
    # HIVE sentence definition
    sentence = "CREATE TABLE IF NOT EXISTS %s (%s)\
                PARTITIONED BY (%s)\
                ROW FORMAT DELIMITED\
                FIELDS TERMINATED BY '%s'\
                STORED AS TEXTFILE LOCATION '%s'" % (name, ','.join(t_fields), ','.join(p_fields), sep, location)
    
    # If table_needs_to_be_recreated==True, delete the old partitioned table. This only happens when the maximum number of periods of the tertiary measures is bigger than the latest number of periods
    if table_needs_to_be_recreated is True:
        drop_table = 'DROP TABLE %s' % name
        context['clients']['hive'].execute(drop_table)
        logger.debug('Deleted old HIVE partitioned table: %s' % name)
        
    try:            
        context['clients']['hive'].execute(sentence)
        logger.debug('Created HIVE partitioned table: %s' % name)
    except Exception, e:
        logger.error('Failed to create temp HIVE table for new partition: %s' % name)
        logger.error('%s' % e)
        return False
            
    return name


def get_period(root_path, companyId, type):
    try:
        with open( '%s/%s-%s.p' % (root_path,str(companyId),type) ) as f:
            p = int(f.readline().strip('\n'))
    except:
        # consider any error as there is no previous backup yet
        p = None
    return p