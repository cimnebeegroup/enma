from __future__ import absolute_import
import sys

# General imports
from dateutil.relativedelta import relativedelta
from time import mktime
from bson import ObjectId
from datetime import datetime, timedelta
import json
import copy
import types
import itertools
import string
import inspect
import sys
import subprocess
import re
import calendar
import time
import pytz
import types

# Mapreduce imports
import lib
from os import path
from lib.etl.mapreduce.MR_delete_keys_from_hbase import MRJob_delete_keys

# HBase, Mongo, serialization and files interation
from pymongo import MongoClient
from json import loads as JSONloads
from json import dumps as JSONdumps
from UserDict import IterableUserDict
from subprocess import Popen, PIPE
from tempfile import NamedTemporaryFile
import happybase

# CIMNE functions import
from lib.utils import *
from lib.querybuilder import QueryBuilder
from ETL import ETLTask, ETLTaskFailed

# Logger import
import logging
logger = logging.getLogger('deleteHbaseResults')

def time_representation(timestamp,format):
    try:
        timestamp = int(timestamp)
    except:
        return {'hive':None, 'mongo':None}
    if format=="month":
        time_rep_hive = (datetime.fromtimestamp(timestamp)).strftime("%Y%m")
        time_rep_mongo = int((datetime.fromtimestamp(timestamp)).strftime("%Y%m"))
    elif format=="day":
        time_rep_hive = (datetime.fromtimestamp(timestamp)).strftime("%Y%m%d")
        time_rep_mongo = int((datetime.fromtimestamp(timestamp)).strftime("%Y%m%d"))
    elif format=="hour":
        time_rep_hive = (datetime.fromtimestamp(timestamp)).strftime("%Y%m%d%h")
        time_rep_mongo = int((datetime.fromtimestamp(timestamp)).strftime("%Y%m%d%h"))
    elif format=="_created":
        time_rep_hive = (datetime.fromtimestamp(timestamp)).strftime("%Y%m%d")
        time_rep_mongo = datetime.fromtimestamp(timestamp)
    return {'hive': time_rep_hive, 'mongo': time_rep_mongo}

def delete_results_hbase(context,companyId,task_UUID):
    ######################################################################################################################################################################################
    ######################################################################################################################################################################################
    """ TASKS DESCRIPTION """
    ######################################################################################################################################################################################
    """
    DELETE_RESULTS MODULE
    1. READ THE DOCUMENTS IN delete_results COLLECTION THAT HAVE status:false
    2. ORDER EVERY COMBINATION OF services, setupId, ts_from AND ts_to BY contractId AND customerId
    3. SELECT THE DATA TO BE DELETED. (DELETE THAT DATA FROM MONGO AND BUFFER ALL THE KEYS THAT HAVE TO BE DELETED FROM HBASE)
    4. JOINING OF THE HBASE KEYS TO DELETE
    5. MR PROCESS TO DELETE THE KEYS FROM HBASE
    6. DELETE HIVE TEMPORARY HIVE TABLES
    7. DELETE THE TEMPORARY HDFS DIRECTORY
    8. UPDATE THE REPORT ON THE delete_results COLLECTION
    """
    ######################################################################################################################################################################################
    ######################################################################################################################################################################################
    
    ######################################################################################################################################################################################    
    """READ THE DELETE ORDERS"""
    ######################################################################################################################################################################################
    
    report = {}
    report['started_at'] = datetime.now()
    
    logger.info('Reading the delete orders...')
    
    db = context['config']['app']['mongodb']['db']
    collection = context['config']['etl']['delete_results']['public']['mongodb']['collection']
    cursor = context['clients']['mongo'][db][collection].find({'report':{'$exists':False},'companyId':companyId})
    buffer_docs = []
    possible_services = [item for item in context['config']['etl']['delete_results']['info_modules'].iterkeys()]
    for docs in cursor:
        if docs['services'] is None:
            if docs['companyId'] in [1234509876,1234519876]:
                docs['services'] = [item for item in possible_services if item[0:2]=="mt"]
            else:
                docs['services'] = [item for item in possible_services if item[0:2]!="mt"]
        buffer_docs.append(docs)
    

    if len(buffer_docs)>0:
        
        logger.info('There are %s delete orders to process.................................' %str(len(buffer_docs)))
        
        ######################################################################################################################################################################################
        """ORDER EVERY COMBINATION"""
        ######################################################################################################################################################################################
    
        """
        example:
        
            BUFFER OF ORDERS TO DELETE
            buffer_docs = [
                             {
                                  u'_id': ObjectId('54e76326f2f23eb23ba0d742'),
                                  u'companyId': 1234509876L,
                                  u'contractId': None,
                                  u'customerId': None,
                                  u'report': {
                                      u'finished_at': None,
                                      u'num_objects_deleted': None,
                                      u'started_at': None,
                                      u'status': False
                                  },
                                  u'services': [u'mt401'],
                                  u'setupId': None,
                                  u'ts_from': datetime.datetime(2015, 2, 1, 0, 0),
                                  u'ts_to': datetime.datetime(2015, 2, 27, 0, 0)  
                             }
                          ]
            
            ORDER BY SERVICES
            ser = {
                    'mt401': [
                        {
                             'companyId': 1234509876L,
                             'contractId': None,
                             'customerId': None,
                             'setupId': None,
                             'ts_from': datetime.datetime(2015, 2, 1, 0, 0),
                             'ts_to': datetime.datetime(2015, 2, 27, 0, 0)
                        }
                     ]
                  }
            
            ORDER BY SERVICES AND PERIOD COMBINATION
            sercom = {
                        'mt401': {
                            '1422745200-1424991600': [
                                {
                                    'companyId': 1234509876L,
                                    'contractId': None,
                                    'customerId': None,
                                    'setupId': None
                                }
                            ]
                        }
                     }
            
            ORDER BY SERVICES, PERIOD COMBINATION AND COMPANYID_SETUPID COMBINATION
            sercomcs = {
                'mt401': {
                    '1422745200-1424991600': {
                        '1234509876-None': {
                            'contracts': [None],
                            'customers': [None]
                        }
                    }
                }
            }
        """
        
        logger.info('Building the general condition to select the results to delete')
        
        #Split by services
        ser={}
        for service in possible_services:
            values = []
            for item in buffer_docs:
                if service in item['services']:
                    # Assign contractId as a list of contracts or a None value
                    if isinstance(item['contractId'],types.NoneType):
                        value_contractId = None
                    elif isinstance(item['contractId'],types.StringTypes) or isinstance(item['contractId'],types.IntType):
                        value_contractId = [str(item['contractId'])]
                    elif isinstance(item['contractId'],types.ListType):
                        value_contractId = [str(item_contract) for item_contract in item['contractId']]
                    # Assign customerId as a list of customers or a None value
                    if isinstance(item['customerId'],types.NoneType):
                        value_customerId = None
                    elif isinstance(item['customerId'],types.StringTypes) or isinstance(item['customerId'],types.IntType):
                        value_customerId = [str(item['customerId'])]
                    elif isinstance(item['customerId'],types.ListType):
                        value_customerId = [str(item_customer) for item_customer in item['customerId']]
                    
                    values.append({
                              'companyId': item['companyId'],
                              'contractId': value_contractId,
                              'customerId': value_customerId,
                              'ts_to': item['ts_to'] if 'ts_to' in item else None,
                              'ts_from': item['ts_from'] if 'ts_from' in item else None,
                              'setupId': item['setupId'] if 'setupId' in item else None
                             })
            if values != []:
                ser[service] = values
        
        
        sercom={}
        
        #Split by services and period combination
        for service,list_per_services in ser.iteritems():
            sercom[service]={}
            # Combination of ts_from and ts_to for this service
            period_comb=[]
            for item in list_per_services:
                if item['ts_from'] is not None and item['ts_to'] is not None:
                    period_comb.append(str(calendar.timegm(item['ts_from'].timetuple())) + '-' + str(calendar.timegm(item['ts_to'].timetuple())))
                elif item['ts_from'] is None and item['ts_to'] is not None:
                    period_comb.append('None-' + str(calendar.timegm(item['ts_to'].timetuple())))
                elif item['ts_from'] is not None and item['ts_to'] is None:
                    period_comb.append(str(calendar.timegm(item['ts_from'].timetuple())) + '-None')
                else:
                    period_comb.append('None-None')
            for period in period_comb:
                values = []
                for item in list_per_services:
                    if item['ts_from'] is not None and item['ts_to'] is not None:
                        period_item = str(calendar.timegm(item['ts_from'].timetuple())) + '-' + str(calendar.timegm(item['ts_to'].timetuple()))
                    elif item['ts_from'] is None and item['ts_to'] is not None:
                        period_item = 'None-' + str(calendar.timegm(item['ts_to'].timetuple()))
                    elif item['ts_from'] is not None and item['ts_to'] is None:
                        period_item = str(calendar.timegm(item['ts_from'].timetuple())) + '-None'
                    else:
                        period_item = 'None-None'
                    if period_item==period:
                        values.append({
                                   'contractId': item['contractId'],
                                   'customerId': item['customerId'],
                                   'companyId': item['companyId'],
                                   'setupId': item['setupId']
                                   })
                sercom[service][period]=values
        
        
        
        #Split by services, period combination and companyId-setupId combination
        sercomcs = {}
        for service,dict_per_services in sercom.iteritems():
            sercomcs[service]={}
            for period,list_per_period in dict_per_services.iteritems():
                sercomcs[service][period]={}
                company_setup_comb = [str(item['companyId'])+'-'+str(item['setupId']) for item in list_per_period]
                for company_setup in company_setup_comb:
                    values = []
                    for item in list_per_period:
                        if str(item['companyId'])+'-'+str(item['setupId'])==company_setup:
                            values.append({
                                           'contractId': item['contractId'],
                                           'customerId': item['customerId'],
                                          })
                    # Contracts definition (contractId)
                    contracts_aux = [item['contractId'] for item in values]
                    if not None in contracts_aux:
                        contracts=[]
                        for i in range(len(contracts_aux)):
                            if isinstance(contracts_aux[i],types.StringTypes):
                                contracts.append(contracts_aux[i])
                            else:
                                contracts.extend(contracts_aux[i])
                        contracts = unique(contracts)
                    else:
                        contracts = None
                    # Customers definition (customerId)
                    customers_aux = [item['customerId'] for item in values]
                    if not None in customers_aux:
                        customers = []
                        for i in range(len(customers_aux)):
                            if isinstance(customers_aux[i],types.StringTypes):
                                customers.append(customers_aux[i])
                            else:
                                customers.extend(customers_aux[i])
                        customers = unique(customers)
                    else:
                        customers = None
                    #Consider the customerId of the contractId's selected, and the contractId for the customerId's selected. 
                    customers_from_contracts = [item['customer']['customerId'] for item in
                                                    context['clients']['mongo'][db]['contracts'].find({
                                                            'companyId':int(company_setup.split("-")[0]),
                                                            'contractId':{'$in':contracts}
                                                    })] if contracts is not None else None
                    contracts_from_customers = [item['contractId'] for item in
                                                    context['clients']['mongo'][db]['contracts'].find({
                                                            'companyId':int(company_setup.split("-")[0]),
                                                            'customer.customerId':{'$in':customers}
                                                    })] if customers is not None else None
                    if contracts_from_customers:
                        contracts = unique(contracts.extend(contracts_from_customers)) if contracts is not None else unique(contracts_from_customers)
                    if customers_from_contracts:
                        customers = unique(customers.extend(customers_from_contracts)) if customers is not None else unique(customers_from_contracts)
                    #Final contracts and customers
                    sercomcs[service][period][company_setup]={
                                                            'contractId': contracts,
                                                            'customerId': customers
                                                            }
                        
        
        ###################################################################################################################################################################################### 
        """SELECT THE DATA TO BE DELETED. (DELETE THAT DATA FROM MONGO AND BUFFER ALL THE KEYS THAT HAVE TO BE DELETED FROM HBASE)"""
        ######################################################################################################################################################################################
            
        """
        Example of the document that we will iterate, generating the key - table output needed to delete the results in Hbase and Mongo.
        sercomcs = {
            'mt401': {
                '1422745200-1424991600': {
                    '1234509876-None': {
                        'contracts': None,
                        'customers': None
                    }
                }
            }
        }
        """
        report['elements_deleted']={}
        multiple_table_from=[]
        
        for service,dict_to_delete in sercomcs.iteritems():
            
            #Initialize the elements deleted dictionary for this service
            report['elements_deleted'][service]={}
            
            # Key items definition for this service
            key_type_items = context['config']['etl']['delete_results']['info_modules'][service]['key']
            key_items = [item[0] for item in key_type_items]
            
            value_items = context['config']['etl']['delete_results']['info_modules'][service]['values']
            
        
            ###########################################################################################
            # Definition of the selection in Mongo and Hbase
            ###########################################################################################
            
            time_format = key_items[0]
            user_format = 'contractId' if 'contractId' in key_items else 'customerId'
            
            condition_hive = []
            condition_final_mongo = {'$or':[]}
            
            # Period definition
            for period,periods_results in dict_to_delete.iteritems():
                
                condition_period = [] ####INITIALIZE
                condition_mongo = {'$and':[]}
                condition_period_mongo = {}
                
                time_from = time_representation(period.split("-")[0],time_format) if period.split("-")[0]!='None' else None
                time_to = time_representation(period.split("-")[1],time_format) if period.split("-")[1]!='None' else None
                if time_from is not None:
                    condition_period.append('r.key.'+time_format.replace("_","")+'>='+time_from['hive'])
                    condition_period_mongo[time_format]={'$gte':time_from['mongo']}
                if time_to is not None:
                    condition_period.append('r.key.'+time_format.replace("_","")+'<='+time_to['hive'])
                    condition_period_mongo[time_format].update({'$lte':time_to['mongo']})
                
                # companyId and setupId definition
                condition_company =[]
                condition_company_mongo={'$or':[]}
                for companysetup, companysetup_results in periods_results.iteritems():
                    condition_companysetup = [] ####INITIALIZE
                    condition_companysetup_mongo = {}
                    
                    companyId = companysetup.split("-")[0] if companysetup.split("-")[0]!='None' else None
                    setupId = companysetup.split("-")[1] if companysetup.split("-")[1]!='None' else None
                    if companyId is not None and service[0:2]!="mt": #special condition for the mobile tool
                        condition_companysetup.append('r.key.companyId = "' + companyId + '"')
                        condition_companysetup_mongo['companyId'] = int(companyId)
                    if setupId is not None:
                        condition_companysetup.append('r.key.setupId = "' + setupId + '"')
                        condition_companysetup_mongo['setupId'] = setupId
                
                    #contracts or customers
                    users_to_search = companysetup_results[user_format]
                    if users_to_search is not None:
                        condition_companysetup.append('r.key.' + user_format + ' in (' + ",".join(['"'+item+'"' for item in users_to_search]) + ')')
                        if service[0:2]=="mt": #special condition for the mobile tool
                            condition_companysetup_mongo['user_id']={}
                            condition_companysetup_mongo['user_id']['$in']=users_to_search
                        else:
                            condition_companysetup_mongo[user_format]={}
                            condition_companysetup_mongo[user_format]['$in']=users_to_search
                    if len(condition_companysetup)>0:
                        # Hive condition
                        condition_company.append('(' + ' and '.join(condition_companysetup) + ')')
                        # Mongo condition
                        condition_company_mongo['$or'].append(condition_companysetup_mongo)
                
                #If there is period condition...            
                if(len(condition_period)>0):
                    # And companysetup condition...
                    if len(condition_company)>0:
                        condition_period.append('(' + ' or '.join(condition_company) + ')')
                        condition_period_mongo.update(condition_company_mongo)
                    # Save HIVE condition
                    condition_hive.append('(' + ' and '.join(condition_period) + ')')
                    # Save MONGO condition
                    condition_mongo['$and'].append(condition_period_mongo)
                #If there isn't period condition...
                else:
                    # And companysetup condition...
                    if len(condition_company)>0:
                        condition_period.append('(' + ' or '.join(condition_company) + ')')
                        # Save HIVE condition
                        condition_hive.append(condition_period)
                        # Save MONGO condition
                        condition_mongo['$and'].append(condition_period_mongo)
                # Update the final MONGO condition
                if(len(condition_mongo['$and'])>0):
                    condition_final_mongo['$or'].append(condition_mongo)
                
            
            # Final HIVE condition
            if(len(condition_hive)>0):
                condition_final_hive = '('+' or '.join(condition_hive)+')'
            else:
                condition_final_hive = None
            
            #Final MONGO condition
            if len(condition_final_mongo['$or'])==0:
                condition_final_mongo = None
    
            report['trace'] = {}
            
            ###########################################################################################
            # Delete the results in Mongo
            ###########################################################################################
            
            #logger.info( ('Query to delete from mongo in the service %s results: '% service) +str(condition_mongo) )
            
            if service[0:2]=="mt":
                db = context['config']['app']['mongodb2']['db']
                if service=="mt401":
                    collection_of_the_service = "alert_tips"
                else:    
                    collection_of_the_service = service
                mongo_connection = context['clients']['mongoApp']
            else:
                db = context['config']['app']['mongodb']['db']
                collection_of_the_service = service[0:2].upper()+service[2:]+'Results'
                mongo_connection = context['clients']['mongo']
                
            try:
                pass
                #if condition_final_mongo:
                #    report['trace']['mongo_delete_results_%s'%service] = mongo_connection[db][collection_of_the_service].remove(condition_mongo)
                #else:
                #    report['trace']['mongo_delete_results_%s'%service] = mongo_connection[db][collection_of_the_service].remove()
                #report['elements_deleted'][service]['mongo'] = report['trace']['mongo_delete_results_%s'%service]['n']
            except Exception, e:
                logger.info('The mongo collection of service %s is not available' % service)
            
            ###########################################################################################
            # Query to found the keys that we have to delete
            ###########################################################################################
            
            # Test if there is an existing Hbase table for this service
            exist_hbase_table = True if service in context['clients']['hbase'].tables() else False
            
            if exist_hbase_table:
                
                logger.info("Running the HIVE query to detect the keys to delete in the table %s" %(service) )

                # Create and run the Hive query to Hbase
                qb = QueryBuilder(context['clients']['hive'])
                fields = [('key','string'),('hbase_table','string')]
                    
                table_from = create_module_oldResults_temp_table(context, service, 'aux_table_'+task_UUID, fields=[(item.replace("_",""),'string') for item in value_items], 
                                                                 key_fields = [(item[0].replace("_",""),item[1]) for item in key_type_items])
                table_input = create_hive_module_input_table(context, '%s_keys_to_delete' %service, context['config']['etl']['delete_results']['paths']['result']+'/'+service, fields,task_UUID)
                qb = qb.add_from(table_from, 'r').add_insert(table=table_input)
                qb = qb.add_select('concat_ws("~",%s),"%s"' % (",".join(['cast(r.key.'+item.replace("_","")+' as string)' for item in key_items]),service))
                if condition_hive is not None:
                    qb = qb.add_where('%s' % condition_final_hive)
                groups_condition = ['r.key.'+item.replace("_","") for item in key_items]
                groups_condition.extend(['r.'+item.replace("_","") for item in value_items])
                qb = qb.add_groups(groups_condition)
                try:
                    pass
                    report['trace']['hbase_delete_results_%s_keys'%service] = qb.execute_query()
                    multiple_table_from.append(table_input)
                except Exception, e:
                    raise ModuleTaskFailed(context, msg='%s' % e)
                
                # Count the keys to delete
                table_input_count = create_hive_module_input_table(context, '%s_keys_to_delete_counter' %service, context['config']['etl']['delete_results']['paths']['count'] ,[('count','int')],task_UUID)
                qb = QueryBuilder(context['clients']['hive'])
                qb = qb.add_from(table_input, 'r').add_insert(table=table_input_count)
                qb = qb.add_select('COUNT(r.key)')
                qb = qb.add_groups(['r.hbase_table'])
                try:
                    pass
                    qb.execute_query()
                except Exception, e:
                    raise ModuleTaskFailed(context, msg='%s' % e)
                
                aux = subprocess.Popen(["hadoop", "fs", "-cat", context['config']['etl']['delete_results']['paths']['count']+'/000000_0'], stdout=subprocess.PIPE).stdout.readlines()
                
                if len(aux)>0:
                    report['elements_deleted'][service]['hbase'] = int(aux[0].replace("\n",""))
                else:
                    report['elements_deleted'][service]['hbase'] = 0
                            
    
    
        ######################################################################################################################################################################################
        """QUERY TO JOIN ALL THE RESULTS"""
        ######################################################################################################################################################################################
        
        logger.info("Running the HIVE query to join all the keys to delete from all the tables")
        
        #Test if there are results to delete
        keys_selected = 0
        for service in sercomcs.iterkeys():
            pass
            keys_selected += report['elements_deleted'][service]['hbase']
        
        if keys_selected>0:    
            qb = QueryBuilder(context['clients']['hive'])
            fields = [('key','string'),('hbase_table','string')]
            union_all_query = '(' + ' UNION ALL '.join([ str('SELECT * FROM %s' % table_from) for table_from in multiple_table_from ]) + ')'
            table_input_final = create_hive_module_input_table(context, '%s_keys_to_delete_ALL' %service, context['config']['etl']['delete_results']['paths']['result_final'], fields,task_UUID)
            qb = qb.add_from(union_all_query, 'r').add_insert(table=table_input_final)
            qb = qb.add_select('r.key,r.hbase_table')
            try:
                pass
                report['trace']['hbase_delete_results_all_keys'] = qb.execute_query()
            except Exception, e:
                raise ModuleTaskFailed(context, msg='%s' % e)
                
        
        
        ######################################################################################################################################################################################
        """MR PROCESS TO DELETE THE KEYS FROM HBASE"""
        ######################################################################################################################################################################################
        
        report['MR_job'] = {}
        
        if keys_selected>0:

            logger.info("Running a MRJOB to delete from HBase all the selected keys")

            report['MR_job']['started_at'] = datetime.now()
            
            input = context['config']['etl']['delete_results']['paths']['result_final']
                    
            report['MR_jobs'] = {
                      'started_at': datetime.now(),
                      'state': 'launched',
                      'input': input
                      }
            
            # Create temporary file to upload with json extension to identify it in HDFS
            job_extra_config = context['config']
            f = NamedTemporaryFile(delete=False, suffix='.json')
            f.write(JSONdumps(job_extra_config))
            f.close()
            report['config_temp_file'] = f.name
            logger.debug('Created temporary config file to upload into hadoop and read from job: %s' % f.name)
            
            # create hadoop job instance adding file location to be uploaded
            mr_job = MRJob_delete_keys(args=['-r', 'hadoop', 'hdfs://'+input, '--file', f.name, '--python-archive', path.dirname(lib.__file__)])
            with mr_job.make_runner() as runner:
                try:
                    runner.run()
                except Exception,e:
                    f.unlink(f.name)
                    raise Exception('Error running MRJob process using hadoop: %s' % e)
            
            f.unlink(f.name)
            logger.debug('The MRjob was successful! Temporary config file uploaded has been deleted from FileSystem')
            
            report['MR_job']['finished_at'] = datetime.now()
            report['MR_job']['status'] = 'finished'
        else:
            report['MR_job'] = 'No keys to delete - HIVE query does not found any measures'
            
        
        
        ######################################################################################################################################################################################
        """DELETE HIVE TEMPORARY HIVE TABLES"""
        ######################################################################################################################################################################################
        
        logger.info("Delete the HIVE temporary tables")
        
        try:
            pass
            for table in context['temp_hive_tables']['measures']:
                delete_measures_temp_table(context,table)
            for table in context['temp_hive_tables']['input']:
                delete_hive_module_input_table(context,table)
        except Exception, e:
            raise ModuleTaskFailed(context,msg='Error deleting the HIVE temporary tables: %s' % e)
                
                
        
        ######################################################################################################################################################################################
        """DELETE THE TEMPORARY HDFS DIRECTORY"""
        ######################################################################################################################################################################################
        
        logger.info("Delete the HDFS temporary files")
        
        try:
            pass
            report['trace']['cleanup_temp_data'] = cleanup_temp_data(context, context['config']['etl']['delete_results']['paths']['result'], recurse=True)
        except Exception, e:
            raise ModuleTaskFailed(context,msg='Error deleting the Temporary Data from HDFS: %s' % e)
                
        
        
        ######################################################################################################################################################################################    
        """UPDATE THE REPORT OF THE ORDERS IN THE delete_results COLLECTION"""
        ######################################################################################################################################################################################
        
        logger.info("Updating the report for the delete orders executed")
        
        db = context['config']['app']['mongodb']['db']
        collection = context['config']['etl']['delete_results']['public']['mongodb']['collection']
        for doc in buffer_docs:
            pass
            context['clients']['mongo'][db][collection].update({
                                                                '_id':doc['_id']
                                                               },
                                                               {
                                                                '$set': {
                                                                         'report':{
                                                                                   'status': True,
                                                                                   'started_at': report['started_at'],
                                                                                   'finished_at': datetime.now()
                                                                                  }
                                                                        }
                                                               })
        
    
    else:
        report = 'No results delete orders for company %s' % companyId
        logger.info("There are not any results delete orders")
        
    
    logger.info('Module_delete_results execution finished...')

    return report