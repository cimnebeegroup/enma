from tempfile import NamedTemporaryFile
from cPickle import dumps
from lib.utils import unix_time
from datetime import datetime
import calendar
import pytz
from pymongo import MongoClient, ASCENDING, DESCENDING


def create_buffer(cursor, n):
    """Accumulates cursor documents until we fill the buffer or there are no more documents on cursor"""
    buffer = []
    for i in range(n):
        try:
            buffer.append(cursor.next())
        except StopIteration, e:
            break
    
    return buffer


def should_duplicate_and_remove(context, db, collection, query, min_num_of_docs = 4, percent = 30):
    """
    If # docs on collection > 4M
        If # docs_to_remove (find().count()) > 25-30% 
    """
    stats = context['clients']['mongo'][db].command("collstats",collection)
    if float(stats['count']) > (min_num_of_docs * 1000000):
        if float(context['clients']['mongo'][db][collection].find(query).count()) > (float(stats['count']) * percent/100):
            return True
        else:
            return False
    else: 
        return False
    

def duplicate_and_remove(context, db, collection, query, buffer_size, index, index2):
    """
    if query!={}:
        rename to old_collection
        create collection + index
        find + [ buffer + insert ]*
        drop old_collection
    else:
        drop collection
        create collection + index
    """
    if query != dict():
        #Negative query
        query_neg=query.copy()
        for item in query_neg:
            if item=='companyId':
                if type(query_neg['companyId'])==dict:
                    companies_valid = [ int(company['companyId']) for company in context['clients']['mongo'][db]['companies'].find({'companyId':{'$not':query_neg['companyId']}},{'_id':0,'companyId':1}) ]
                else:
                    companies_valid = [ int(company['companyId']) for company in context['clients']['mongo'][db]['companies'].find({'companyId':{'$ne':query_neg['companyId']}},{'_id':0,'companyId':1}) ]
                query_neg['companyId'] = {"$in":companies_valid}
            else:
                query_neg[item] = {"$not":query_neg[item]} if type(query_neg[item])==dict else {"$ne":query_neg[item]}
        #Step1. Rename of the old collection
        context['clients']['mongo'][db][collection].rename(collection+'_old_collection')
        #Step2. Create the new collection and index it
        context['clients']['mongo'][db].create_collection(collection)
        context['clients']['mongo'][db][collection].create_index(index)
        if index2 is not None:
            context['clients']['mongo'][db][collection].create_index(index2)
        #Step3. Insert the documents that don't match the query from the old collection to the new one
        buffer = []
        for doc in context['clients']['mongo'][db][collection+'_old_collection'].find(query_neg):
            # append to buffer and insert when buffer gets buffer_size
            buffer.append(doc)
            if len(buffer) == buffer_size:
                context['clients']['mongo'][db][collection].insert(buffer)
                buffer = []
        if buffer:
            # last buffer bulk
            context['clients']['mongo'][db][collection].insert(buffer)
        #Step4. Drop the old collection
        context['clients']['mongo'][db][collection+'_old_collection'].drop()
    else:
        #Step1. Drop the actual collection
        context['clients']['mongo'][db][collection].drop()
        #Step2. Create the new collection and index it
        context['clients']['mongo'][db].create_collection(collection)
        context['clients']['mongo'][db][collection].create_index(index)
        if index2 is not None:
            context['clients']['mongo'][db][collection].create_index(index2)


def remove_measures(context,db, collection, query, buffer_size=500000 ,index=[("companyId", ASCENDING),("timestamp", DESCENDING)], index2=None):
    if should_duplicate_and_remove(context, db, collection, query):
        duplicate_and_remove(context, db, collection, query, buffer_size, index, index2)
    else:
        context['clients']['mongo'][db][collection].remove(query, timeout=False)
        

def create_buffer_v2(context,query, n):
    """Accumulates cursor documents until we fill the buffer or there are no more documents on cursor"""
    buffer = []
    # setting variables for readability
    measures_collection = context['config']['etl']['mongodb']['collection']
    db = context['config']['app']['mongodb']['db']
    for i in range(n):
        try:
            #buffer.append(cursor.next())
            cursor = context['clients']['mongo'][db][measures_collection].find_and_modify(query = query, remove = True)
            if cursor is None:
                return buffer
            buffer.append(cursor)
        except StopIteration, e:
            break
    return buffer


def tsHbase_to_dtLocal(timestamp,timezone):
    """From timestamp in Hbase timezone to datetime in local utility timezone"""
    date_local=pytz.utc.localize(datetime.fromtimestamp(timestamp)).astimezone(pytz.timezone('%s'%timezone))
    return date_local


def dtLocal_to_tsHbase(date_local):
    """From datetime in local utility timezone to timestamp in Hbase timezone"""
    date_utc=date_local.astimezone(pytz.utc)
    date_hbase=pytz.timezone('Europe/Madrid').localize(datetime(date_utc.year,date_utc.month,
    date_utc.day,date_utc.hour,date_utc.minute,date_utc.second,date_utc.microsecond)).astimezone(pytz.utc)
    date_hbase=calendar.timegm(datetime.timetuple(date_hbase))
    return date_hbase


def create_csv_from_buffer(buffer, row_definition):
    """Creates a temporary CSV file from a list of documents (buffer).
    We need to care with columns order so row_definition must be a list
    """
    # Create temporary file on local machine
    file = NamedTemporaryFile(delete=False)
    # write every record into file with pickle
    for doc in buffer:
        file.write(doc_to_hbase_string(doc, row_definition)+'\n')
    file.close()
    return file


def create_pickle_file_from_buffer(buffer):
    """Creates a temporary CSV file from a list of documents (buffer).
    We need to care with columns order so row_definition must be a list
    """
    # Create temporary file on local machine
    file = NamedTemporaryFile(delete=False)
    for doc in buffer:
        file.write(dumps(doc).encode('string_escape')+'\r\n')
    file.close()
    return file


def doc_to_hbase_string(doc, row_definition):
    """Create a string with a python dictionary mapping row_definition names with doc keys.
    row_definition can have more information like its type so we can make our custom casts
    "row_definition": [
        { "name": "companyId", "type": "integer" },
        { "name": "deviceId", "type": "string" },
        { "name": "timestamp","type": "datetime" },
        { "name": "value", "type": "float" },
        { "name": "reading", "type": "ObjectId" }
    ]
    doc = {
        'companyId': 0,
        'deviceId': '',
        'timestamp': datetime,
        'value': 0.0,
        'reading': ObjectId('')
    }
    """
    import pickle
    row = []
    for d in row_definition:
        row.append(str(doc[d['name']]))

    return ",".join(row)


def convert_apptype_to_amontype(doc):
    if doc['type']=="heating":
        doc['type'] = "heatConsumption"
    if doc['type']=="electricity":
        doc['type'] = "electricityConsumption"
    return doc


def convert_depending_type(doc):
    types = ['heatConsumption', 'heating', 'gasConsumption']
    if doc['type'] in types:
        doc['value'] *= 11.63
    return doc


def convert_units_to_kilo(doc):
    conversions = ['W', 'Wh', 'VArh', 'VA', 'VAr', 'Whth']
    # Watts, WattsHour, VoltAmpHour, VoltAmps, VoltAmpsReactive,WattHoursofHeath     
    if doc['reading']['unit'].lower() in [item.lower() for item in conversions]:
        doc['value'] /= 1000
    return doc


def convert_units_to_kilo_tertiary(doc,value):
    conversions = ['W', 'Wh', 'VArh', 'VA', 'VAr', 'Whth']
    # Watts, WattsHour, VoltAmpHour, VoltAmps, VoltAmpsReactive,WattHoursofHeath     
    if doc['reading']['unit'].lower() in [item.lower() for item in conversions]:
         value /= 1000
    return value


def translate_datetime(doc):
    #doc['timestamp'] = int(timegm(doc['timestamp'].timetuple()))
    doc['timestamp'] = int(doc['timestamp'].strftime('%s'))
    #doc['timestamp'] = unix_time(doc['timestamp'])
    return doc


def add_ts_bucket(doc):
    doc['bucket'] = (doc['timestamp']/100) % 100
    return doc


def reload_buffer_to_rest(context,buffer):
    """
    Buffer format:
    [ 
                {
                    "companyId" : NumberLong(6554129435),
                    "timestamp" : ISODate("2014-07-07T08:30:00.000Z"),
                    "value" : 0.048,
                    "deviceId" : "0df5711f-63fa-4868-8747-304f96867b0d",
                    "_created" : ISODate("2014-09-30T12:13:20.000Z"),
                    "reading" : ObjectId("52e8f44fdfeb572af78fecbe"),
                    "type" : "electricityConsumption"
                },
                {
                    "companyId" : NumberLong(6554129435),
                    "timestamp" : ISODate("2014-07-07T08:45:00.000Z"),
                    "value" : 0.087,
                    "deviceId" : "0df5711f-63fa-4868-8747-304f96867b0d",
                    "_created" : ISODate("2014-09-30T12:13:21.000Z"),
                    "reading" : ObjectId("52e8f44fdfeb572af78fecbe"),
                    "type" : "electricityConsumption"
                },...
    ]
    """
    # setting variables for readability
    measures_collection = context['config']['etl']['mongodb']['collection']
    db = context['config']['app']['mongodb']['db']
    context['clients']['mongo'][db][measures_collection].insert(buffer)