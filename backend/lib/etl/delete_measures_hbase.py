from __future__ import absolute_import
import sys

# General imports
from dateutil.relativedelta import relativedelta
from time import mktime
from bson import ObjectId
from datetime import datetime, timedelta
import json
import copy
import types
import itertools
import string
import inspect
import sys
import subprocess
import re
import calendar
import time
import pytz
import types

# Mapreduce imports
import lib
from os import path
from lib.etl.mapreduce.MR_delete_keys_from_hbase import MRJob_delete_keys

# HBase, Mongo, serialization and files interation
from pymongo import MongoClient
from json import loads as JSONloads
from json import dumps as JSONdumps
from UserDict import IterableUserDict
from subprocess import Popen, PIPE
from tempfile import NamedTemporaryFile
import happybase

# CIMNE functions import
from lib.utils import *
from lib.querybuilder import QueryBuilder
from ETL import ETLTask, ETLTaskFailed

# Logger import
import logging
logger = logging.getLogger('deleteHbaseMeasures')


def delete_measures_hbase(context,companyId,task_UUID):
    ######################################################################################################################################################################################
    ######################################################################################################################################################################################
    """ TASKS DESCRIPTION """
    ######################################################################################################################################################################################
    """
    DELETE_MEASURES MODULE
    1. READ THE DOCUMENTS IN delete_measures COLLECTION THAT HAVE status:false
    2. ORDER EVERY COMBINATION OF type_company AND ts_from-ts_to
    3. SELECT THE DATA TO BE DELETED. (BUFFER ALL THE KEYS THAT HAVE TO BE DELETED FROM HBASE)
    4. JOINING OF THE HBASE KEYS TO DELETE
    5. MR PROCESS TO DELETE THE KEYS FROM HBASE
    6. DELETE HIVE TEMPORARY TABLES
    7. DELETE THE TEMPORARY HDFS DIRECTORY
    8. UPDATE THE REPORT ON THE delete_measures COLLECTION
    """
    ######################################################################################################################################################################################
    ######################################################################################################################################################################################
    
    ######################################################################################################################################################################################    
    """READ THE DELETE ORDERS"""
    ######################################################################################################################################################################################
    
    report = {}
    report['started_at'] = datetime.now()
    
    logger.info('Reading the delete orders...')
    
    db = context['config']['app']['mongodb']['db']
    collection = context['config']['etl']['delete_measures']['public']['mongodb']['collection']
    cursor = context['clients']['mongo'][db][collection].find({'report':{'$exists':False},'companyId':companyId})
    buffer_docs = []
    possible_types = [item for item in context['config']['etl']['delete_measures']['types']]
    for docs in cursor:
        if docs.get('type') is None:
            if docs['companyId'] in [1234509876,1234519876]:
                docs['type'] = ["electricityConsumption","heatConsumption"]
            else:
                docs['type'] = [item for item in possible_types]
        elif isinstance(docs.get('type'),types.StringTypes):
            docs['type'] = [docs['type']]
        ###
        # New version. Add compatibility for touTypes -->
        #       In case of delete measures orders for this type of measures, we have to delete the measures from the HBase touType table and also from the HBase generic type table
        ###
        for difftype in docs['type']:
            if difftype[0:3]=='tou':
                lowerFirst = lambda s: s[:1].lower() + s[1:]
                docs['type'].append(lowerFirst( difftype[3:] ))
        buffer_docs.append(docs)
    
    if len(buffer_docs)>0:
        
        logger.info('There are %s delete orders to process.................................' %str(len(buffer_docs)))
        
        ######################################################################################################################################################################################
        """ORDER EVERY COMBINATION"""
        ######################################################################################################################################################################################
    
        """
        example:
        
            BUFFER OF ORDERS TO DELETE
            buffer_docs = [
                             {
                                  u'_id': ObjectId('54e76326f2f23eb23ba0d742'),
                                  u'companyId': 1234509876,
                                  u'contractId': None,
                                  u'report': {
                                      u'finished_at': None,
                                      u'num_objects_deleted': None,
                                      u'started_at': None,
                                      u'status': False
                                  },
                                  u'type': 'electricityConsumption',
                                  u'ts_from': datetime.datetime(2015, 2, 1, 0, 0),
                                  u'ts_to': datetime.datetime(2015, 2, 27, 0, 0)
                             }
                          ]
            
            ORDER THE ORDERS BY TYPE_COMPANY
            typcom = {
                    'electricityConsumption_1234509876': [
                        {
                             'contractId': None,
                             'ts_from': datetime.datetime(2015, 2, 1, 0, 0),
                             'ts_to': datetime.datetime(2015, 2, 27, 0, 0)
                        }
                     ]
                  }
            
            ORDER CONTRACTS TO DELETE BY TYPE_COMPANY AND PERIOD COMBINATION
            typcomper = {
                        'electricityConsumption_1234509876': {
                            '1422745200-1424991600': None
                        }
                    }
            
        """
        
        logger.info('Building the general condition to select the measures to delete')
        possible_typcom = {}
        for item in buffer_docs:
            for type_item in item['type']:
                possible_typcom[str(type_item+'_'+str(int(item['companyId'])))] = 1
        possible_typcom = possible_typcom.keys()
        
        #Split by type_company
        typcom={}
        for type_company in possible_typcom:
            values = []
            for item in buffer_docs:
                for item_type in item['type']:
                    if type_company == item_type+'_'+str(int(item['companyId'])):
                        # Assign contractId as a list of contracts or a None value
                        if isinstance(item['contractId'],types.NoneType):
                            value_contractId = None
                        elif isinstance(item['contractId'],types.StringTypes) or isinstance(item['contractId'],types.IntType):
                            value_contractId = [str(item['contractId'])]
                        elif isinstance(item['contractId'],types.ListType):
                            value_contractId = [str(item_contract) for item_contract in item['contractId']]
                        value = {
                                  'contractId': value_contractId,
                                  'ts_to': item['ts_to'] if 'ts_to' in item else None,
                                  'ts_from': item['ts_from'] if 'ts_from' in item else None
                                }
                        values.append(value)
            if values != []:
                typcom[type_company] = values
                
        typcomper={}
        #Split by type_company and period combination
        for type_company,list_per_typcom in typcom.iteritems():
            typcomper[type_company]={}
            # Combination of ts_from and ts_to for this type_company
            period_comb=[]
            for item in list_per_typcom:
                if item['ts_from'] is not None and item['ts_to'] is not None:
                    period_comb.append(str(calendar.timegm(item['ts_from'].timetuple())) + '-' + str(calendar.timegm(item['ts_to'].timetuple())))
                elif item['ts_from'] is None and item['ts_to'] is not None:
                    period_comb.append('None-' + str(calendar.timegm(item['ts_to'].timetuple())))
                elif item['ts_from'] is not None and item['ts_to'] is None:
                    period_comb.append(str(calendar.timegm(item['ts_from'].timetuple())) + '-None')
                else:
                    period_comb.append('None-None')
            for period in period_comb:
                values = []
                for item in list_per_typcom:
                    if item['ts_from'] is not None and item['ts_to'] is not None:
                        period_item = str(calendar.timegm(item['ts_from'].timetuple())) + '-' + str(calendar.timegm(item['ts_to'].timetuple()))
                    elif item['ts_from'] is None and item['ts_to'] is not None:
                        period_item = 'None-' + str(calendar.timegm(item['ts_to'].timetuple()))
                    elif item['ts_from'] is not None and item['ts_to'] is None:
                        period_item = str(calendar.timegm(item['ts_from'].timetuple())) + '-None'
                    else:
                        period_item = 'None-None'
                    if period_item==period:
                        values.append(item['contractId'])
                if None in values:
                    values = None
                else:
                    values = unique(list(itertools.chain.from_iterable(values)))
                typcomper[type_company][period] = values
                    
        
        ###################################################################################################################################################################################### 
        """SELECT THE DATA TO BE DELETED. (BUFFER ALL THE KEYS THAT HAVE TO BE DELETED FROM HBASE)"""
        ######################################################################################################################################################################################
            
        """
        Example of the document that we will iterate, generating the key - table output needed to delete the results in Hbase.
        typcomper = {
                        'electricityConsumption_1234509876': {
                            '1422745200-1424991600': [None]
                        }
                    }
        """
        report['elements_deleted']={}
        multiple_table_from=[]
        
        for type_company,dict_to_delete in typcomper.iteritems():
            
            #Initialize the elements deleted dictionary for this type_company
            report['elements_deleted'][type_company]={}
            
            ###########################################################################################
            # Definition of the condition for the HIVE query
            ###########################################################################################
            
            condition_hive = []
            
            # Period definition
            for period,contracts_to_delete in dict_to_delete.iteritems():
                
                condition_period = [] ####INITIALIZE
                
                time_from = period.split("-")[0] if period.split("-")[0]!='None' else None
                time_to = period.split("-")[1] if period.split("-")[1]!='None' else None
                if time_from is not None:
                    condition_period.append('r.key.ts>='+time_from)
                if time_to is not None:
                    condition_period.append('r.key.ts<='+time_to)
                
                if contracts_to_delete:
                    condition_period.append('r.key.contractId in (%s)' %(",".join(['"%s"'%item_to_del for item_to_del in contracts_to_delete])))
                
                if condition_period!=[]:
                    condition_hive.append( '(%s)' % " and ".join(condition_period) )
            
            #Final HIVE condition
            if condition_hive!=[]:
                condition = "(%s)" %(" or ".join(condition_hive) )
            else:
                condition = None
            
            report['trace'] = {}
            
            ###########################################################################################
            # Query to found the keys that we have to delete
            ###########################################################################################
            
            # Test if there is an existing Hbase table for this type_company
            exist_hbase_table = True if type_company in context['clients']['hbase'].tables() else False
            
            if exist_hbase_table:
                
                logger.info("Running the HIVE query to detect the keys to delete in the table %s" %(type_company) )
                
                # Create and run the Hive query to Hbase
                qb = QueryBuilder(context['clients']['hive'])
                fields = [('key','string'),('hbase_table','string')]
                
                #table_from = create_measures_temp_table(context, type_company.split("_")[0], type_company.split("_")[1], task_UUID, cumulative=True)
                table_from = create_measures_temp_table(context, type_company.split("_")[0], type_company.split("_")[1], task_UUID, for_delete_process=True)
                table_input = create_hive_module_input_table(context, '%s_keys_to_delete' % type_company, context['config']['etl']['delete_measures']['paths']['result']+'/'+type_company, fields,task_UUID)
                qb = qb.add_from(table_from, 'r').add_insert(table=table_input)
                qb = qb.add_select('concat_ws("~",cast(r.key.b as string),cast(r.key.ts as string),cast(r.key.contractId as string)),"%s"' % type_company)
                if condition is not None:
                    qb = qb.add_where('%s' % condition)
                #qb = qb.add_groups(['r.value','r.accumulated'])
                try:
                    pass
                    report['trace']['hbase_delete_measures_%s_keys'%type_company] = qb.execute_query()
                    multiple_table_from.append(table_input)
                except Exception, e:
                    raise ETLTaskFailed(context, msg='%s' % e)
                
                # Count the keys to delete
                table_input_count = create_hive_module_input_table(context, '%s_keys_to_delete_counter' %type_company, context['config']['etl']['delete_measures']['paths']['count'] ,[('count','int')],task_UUID)
                qb = QueryBuilder(context['clients']['hive'])
                qb = qb.add_from(table_input, 'r').add_insert(table=table_input_count)
                qb = qb.add_select('COUNT(r.key)')
                qb = qb.add_groups(['r.hbase_table'])
                try:
                    pass
                    qb.execute_query()
                except Exception, e:
                    raise ETLTaskFailed(context, msg='%s' % e)
                
                aux = subprocess.Popen(["hadoop", "fs", "-cat", context['config']['etl']['delete_measures']['paths']['count']+'/000000_0'], stdout=subprocess.PIPE).stdout.readlines()
                
                if len(aux)>0:
                    report['elements_deleted'][type_company]['hbase'] = int(aux[0].replace("\n",""))
                else:
                    report['elements_deleted'][type_company]['hbase'] = 0
                
        
    
        ######################################################################################################################################################################################
        """QUERY TO JOIN ALL THE RESULTS"""
        ######################################################################################################################################################################################
        
        logger.info("Running the HIVE query to join all the keys to delete from all the tables")
        
        #Test if there are results to delete
        keys_selected = 0
        for type_company in typcomper.iterkeys():
            pass
            keys_selected += report['elements_deleted'][type_company]['hbase']
        
        if keys_selected>0:
            qb = QueryBuilder(context['clients']['hive'])
            fields = [('key','string'),('hbase_table','string')]
            union_all_query = '(' + ' UNION ALL '.join([ str('SELECT * FROM %s' % table_from) for table_from in multiple_table_from ]) + ')'
            table_input_final = create_hive_module_input_table(context, '%s_keys_to_delete_ALL' %type_company, context['config']['etl']['delete_measures']['paths']['result_final'], fields,task_UUID)
            qb = qb.add_from(union_all_query, 'r').add_insert(table=table_input_final)
            qb = qb.add_select('r.key,r.hbase_table')
            try:
                pass
                report['trace']['hbase_delete_measures_all_keys'] = qb.execute_query()
            except Exception, e:
                raise ETLTaskFailed(context, msg='%s' % e)
        
        
        
        
        ######################################################################################################################################################################################
        """MR PROCESS TO DELETE THE KEYS FROM HBASE"""
        ######################################################################################################################################################################################
        
        report['MR_job'] = {}
        
        if keys_selected>0:
            
            logger.info("Running a MRJOB to delete from HBase all the selected keys")
            
            report['MR_job']['started_at'] = datetime.now()
            
            input = context['config']['etl']['delete_measures']['paths']['result_final']
                    
            report['MR_jobs'] = {
                      'started_at': datetime.now(),
                      'state': 'launched',
                      'input': input
                      }
            
            # Create temporary file to upload with json extension to identify it in HDFS
            job_extra_config = context['config']
            f = NamedTemporaryFile(delete=False, suffix='.json')
            f.write(JSONdumps(job_extra_config))
            f.close()
            report['config_temp_file'] = f.name
            logger.debug('Created temporary config file to upload into hadoop and read from job: %s' % f.name)
            
            # create hadoop job instance adding file location to be uploaded
            mr_job = MRJob_delete_keys(args=['-r', 'hadoop', 'hdfs://'+input, '--file', f.name, '--python-archive', path.dirname(lib.__file__)])
            with mr_job.make_runner() as runner:
                try:
                    runner.run()
                except Exception,e:
                    f.unlink(f.name)
                    raise Exception('Error running MRJob process using hadoop: %s' % e)
            
            f.unlink(f.name)
            logger.debug('The MRjob was successful! Temporary config file uploaded has been deleted from FileSystem')
            
            report['MR_job']['finished_at'] = datetime.now()
            report['MR_job']['status'] = 'finished'
        else:
            report['MR_job'] = 'No keys to delete - HIVE query does not found any measures to delete'
        
        
        
        
        ######################################################################################################################################################################################
        """DELETE HIVE TEMPORARY HIVE TABLES"""
        ######################################################################################################################################################################################
        
        logger.info("Deleting all the HIVE temporary tables")
        
        try:
            pass
            for table in context['temp_hive_tables']['measures']:
                delete_measures_temp_table(context,table)
            for table in context['temp_hive_tables']['input']:
                delete_hive_module_input_table(context,table)
        except Exception, e:
            raise ETLTaskFailed(context,msg='Error deleting the HIVE temporary tables: %s' % e)
        
        
        
        ######################################################################################################################################################################################
        """DELETE THE TEMPORARY HDFS DIRECTORY"""
        ######################################################################################################################################################################################
        
        logger.info("Deleting the HDFS temporary files")
        
        try:
            pass
            report['trace']['cleanup_temp_data'] = cleanup_temp_data(context, context['config']['etl']['delete_measures']['paths']['result'], recurse=True)
        except Exception, e:
            raise ETLTaskFailed(context,msg='Error deleting the Temporary Data from HDFS: %s' % e)
        
        
        
        ######################################################################################################################################################################################    
        """UPDATE THE REPORT OF THE ORDERS IN THE delete_measures COLLECTION"""
        ######################################################################################################################################################################################
        
        logger.info("Updating the report for the delete orders executed")
        
        db = context['config']['app']['mongodb']['db']
        collection = context['config']['etl']['delete_measures']['public']['mongodb']['collection']
        for doc in buffer_docs:
            pass
            context['clients']['mongo'][db][collection].update({
                                                                '_id':doc['_id']
                                                               },
                                                               {
                                                                '$set': {
                                                                         'report':{
                                                                                   'status': True,
                                                                                   'started_at': report['started_at'],
                                                                                   'finished_at': datetime.now()
                                                                                  }
                                                                        }
                                                               })
        
    
    else:
        report = 'No measures delete orders for company %s' % companyId
        logger.info("There are not any measures delete orders")
            
    return report