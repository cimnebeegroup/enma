
from mrjob.job import MRJob
from mrjob.protocol import PickleValueProtocol

# mongo clients libs
import happybase
from pymongo import MongoClient
from bson.objectid import ObjectId

# Generic imports
import glob
from json import load
from datetime import datetime
from calendar import timegm
from copy import deepcopy
from dateutil.relativedelta import relativedelta
from lib.etl import transformations


class MRJob_delete_keys(MRJob):
    
    #INPUT_PROTOCOL = PickleValueProtocol
    
    def mapper_init(self):
        # recover json configuration uploaded with script
        fn = glob.glob('*.json')
        self.config = load(open(fn[0]))
        
    def reducer_init(self):
        # recover json configuration uploaded with script
        fn = glob.glob('*.json')
        self.config = load(open(fn[0]))
        
        # columns to delete
        self.columns_to_delete = self.config['columns_to_delete'] if 'columns_to_delete' in self.config else None
        
        # hbase connections
        self.hbase = happybase.Connection(self.config['app']['hbase']['host'], self.config['app']['hbase']['port'])
        self.hbase.open()
        
    def mapper(self, _, doc):   #we don't have value -> input protocol pickleValue which means no key is read
        
        doc = doc.split('\t')
        # doc generation
        d = {
             'key': doc[0],
             'table': doc[1]
            }
        
        yield d['table'],d['key']
        
    def reducer(self, key, values):
        
        # Delete the row-keys in Hbase using a batch process
        if self.columns_to_delete:
            table = self.hbase.table(key)
            with table.batch(batch_size=1000) as b:
                for value in values:
                    b.delete(str(value), columns=self.columns_to_delete)
        else:
            table = self.hbase.table(key)
            with table.batch(batch_size=1000) as b:
                for value in values:
                    b.delete(str(value))
                    
if __name__ == '__main__':
    MRJob_delete_keys.run()  