from datetime import datetime

import logging
logger = logging.getLogger('module')


class QueryBuilder():
    
    def __init__(self, hive_client):
        self.client = hive_client
        
        self.table = None
        self.join = []
        self.insert = None
        self.select = None 
        self.group = None   
        self.order = None
        self.where = None
        self.dynamic = None

    def add_dynamic(self):
        self.dynamic = 'set hive.exec.dynamic.partition.mode=nonstrict;'
        
        return self
    
    def add_from(self, table, alias=None):
        if not alias:
            self.table = 'FROM %s ' % table
        else:
            self.table = 'FROM %s %s ' % (table, alias)
        
        return self  
            
    def add_join(self, table, alias, condition=None):
        if condition:
            self.join.append('JOIN %s %s ON (%s) ' % (table, alias, condition))
        else:
            self.join.append('JOIN %s %s ' % (table, alias))
        
        return self
    
    def add_full_outer_join(self,table,alias,condition=None):
        if condition:
            self.join.append('FULL OUTER JOIN %s %s ON (%s) ' % (table, alias, condition))
        else:
            self.join.append('FULL OUTER JOIN %s %s ' % (table, alias))
                    
        return self
    
    def add_left_outer_join(self,table,alias,condition=None):
        if condition:
            self.join.append('LEFT OUTER JOIN %s %s ON (%s) ' % (table, alias, condition))
        else:
            self.join.append('LEFT OUTER JOIN %s %s ' % (table, alias))
        return self

    def add_right_outer_join(self,table,alias,condition=None):
        if condition:
            self.join.append('RIGHT OUTER JOIN %s %s ON (%s) ' % (table, alias, condition))
        else:
            self.join.append('RIGHT OUTER JOIN %s %s ' % (table, alias))
        return self
        
    def add_insert(self, table=None, overwrite=True, directory=None, partition=None): 
        if not directory and table:
            if partition:
                self.insert = 'INSERT %s TABLE %s PARTITION (%s)' % ('' if not overwrite else 'OVERWRITE', table, partition)
            else:
                self.insert = 'INSERT %s TABLE %s' % ('' if not overwrite else 'OVERWRITE', table)
        elif directory:
            self.insert = 'INSERT %s DIRECTORY \'%s\'' % ('' if not overwrite else 'OVERWRITE', directory)
        else:
            raise Exception('Error adding INSERT sentence into HIVE query. Neither table or directory specified')
        
        return self
        
    def add_select(self, select):
        self.select = 'SELECT %s ' % select
        
        return self
        
    def add_groups(self, groups):
        self.group = 'GROUP BY %s ' %  ','.join(groups)
        
        return self
        
    def add_order(self, name, order=None):
        self.order = 'ORDER BY %s %s ' % (name, 'ASC' if not order else order)
        
        return self
    
    def add_sort(self, content):
        self.sort = 'SORT BY %s ' % (content)
        
        return self
        
    def add_where(self, condition):
        self.where = 'WHERE %s ' % condition
        
        return self 
    
    def add_and_where(self, condition):
        self.where += 'AND %s ' % condition
        
        return self
    
    def create_query(self):
        if not self.table or not self.insert or not self.select:
            raise Exception('Creating query with some missing parts') 
        # Add from
        query_parts = [self.table]
        
        # Add join sentence if needed
        if self.join:
            query_parts.append(' '.join(self.join))
        
        # Add insert and select sentences (mandatory)
        query_parts.append(self.insert)
        query_parts.append(self.select)
        
        if self.where:
            query_parts.append(self.where)
            
        if self.group:
            query_parts.append(self.group)
        
        if self.order:
            query_parts.append(self.order)
        
        if self.dynamic:
            self.client.execute(self.dynamic)
            
        return '\n'.join(query_parts)
    
    def execute_query(self):
        sentence = self.create_query() if len(self.create_query()) < 5000 else self.create_query()[1:5000]+'...The query continues, but it won\'t be shown...'
        logger.debug('Executing HIVE query: %s' % sentence)
        report = {}
        report['started_at'] = datetime.now()
        report['status'] = 'launched'
        report['query'] = self.create_query()
        try:
            self.client.execute(self.create_query())
        except Exception,e:
            report['status'] = 'failed'
            logger.error('HIVE query execution raised an error: %s' % e)
            raise Exception("HIVE query failed: %s " % e)
        else:
            report['finished_at'] = datetime.now()
            report['status'] = 'finished'
            logger.debug('HIVE query executed successfully')
        
        return report