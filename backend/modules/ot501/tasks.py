#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import absolute_import
import sys

from datetime import datetime
import json
import subprocess

#module imports
from modules import ModuleTaskFailed, ModuleTask
from lib.utils import *
from lib.logs import setup_log
from lib.querybuilder import QueryBuilder

# celery imports
from celery_backend import app
from celery import current_task
from celery.utils.log import get_task_logger
from celery.signals import after_setup_task_logger, after_setup_logger

# import function to execute the corresponding ETL's
from ETL.tasks import *

"""Module global variables"""
MODULE="ot501"

after_setup_logger.connect(setup_log)
after_setup_task_logger.connect(setup_log)
logger = get_task_logger('module.%s' % MODULE)

class ModuleOT501Task(ModuleTask):
    abstract = True
    def __init__(self):
        logger.debug('ModuleTask init for module %s' % MODULE)
        ModuleTask.__init__(self)
    
app.Task = ModuleOT501Task


@app.task(name='ot501')
def module_ot501(params):
    
    """ """
    module_ot501.MODULE = MODULE
    
    """Create context dict to pass to functions"""
    logger.info('Starting Module ot501...')
    
    logger.debug('Builing task context with config, clients and report')
    context = {}
    context['current_task'] = current_task
    context['config'] = module_ot501.config
    
    """task_UUID for paths in HDFS and queries"""
    task_UUID = id_generator()
    context['config']['module']['paths'] = random_paths(context['config']['module']['paths'],'UUID', task_UUID)
    
    try:
        context['clients'] = {
                              'hive': module_ot501.hive,
                              'hdfs': module_ot501.hdfs,
                              'mongo': module_ot501.mongo,
                              'hbase': module_ot501.hbase
                              }
    except Exception, e:
        raise ModuleTaskFailed(context, 'Error connecting to needed connection clients: %s' % e)
    
    
    context['report'] = Report(monogodb_fd=context['clients']['mongo'], db=context['config']['app']['report']['db'], collection=context['config']['app']['report']['collection'])
    context['report']['task_id'] = current_task.request.id
    context['report']['task_UUID_paths_queries'] = task_UUID
    context['report']['started_at'] = datetime.now()
    context['report']['params'] = current_task.request.args
    context['report']['module'] = MODULE
    context['report']['progress'] = 1
    context['report'].update()
    
    
    """Get Month as param and check inconsistencies"""
    try:
        ts_to = params['ts_to']
        num_months = params['num_months']
        ts_from = monthback_date(ts_to,num_months)
        type = params['type']
        daily_summary_calculation = params['daily_summary_calculation']
        monthly_summary_calculation = params['monthly_summary_calculation']
        weather_type = params['weather_type']
        companyId = params['companyId']
        setupId = params['setupId']
        timezone = params['timezone']
        timezone_meteo = params['timezone_meteo']
        relation_contract_weatherStation = params['relation_contract_weatherStation']
    except KeyError, e:
        error_msg = 'Not enough parameters provided to module %s: %s' % (MODULE, e)
        logger.error(error_msg)
        raise ModuleTaskFailed(context, msg=error_msg)
    
    try:
        month = get_month(ts_from)
    except Exception, e:
        raise ModuleTaskFailed(context, msg='%s' % e)
    
    
    """ STARTING THE MODULE TASKS """
        
    logger.debug('Starting module ot501 calculations for company %s' % companyId)
    
    context['report']['trace'] = {}
    
    total_num_tasks = 15 if monthly_summary_calculation is True else 11
    total_num_tasks = (total_num_tasks+4) if daily_summary_calculation is True else total_num_tasks
    total_num_tasks = (total_num_tasks+1) if relation_contract_weatherStation else total_num_tasks
    context['job_meta'] = { 'current': 0, 'total': total_num_tasks }
    
    
    
    
    """
    QUERY AND PROCESSING OF METEOROLOGICAL DATA
    """
    
    """ Creation of the customers_weather hdfs table """
    if relation_contract_weatherStation:
        # CUSTOMERS QUERY
        qb = QueryBuilder(context['clients']['hive'])
        fields = [('contractId', 'string'),('customerId','string'),('stationId','string'),('countryCode','string'),('postalCode','string'),('latitude','float'),('longitude','float'),('altitude','float'),('companyId','string')]
        countryCode_company = (context['clients']['mongo'][context['config']['app']['mongodb']['db']]['companies'].find_one(
                              {'companyId':companyId})['countryCode']).upper()
        table_input = create_hive_module_input_table(context, 'customers_wS_%s' % companyId, context['config']['module']['paths']['all']+'/customers_weather/customers', fields, task_UUID)
        qb = qb.add_from('customers', 'c').add_insert(table=table_input).add_join('postalCode_geoLoc', 'p', 'p.countryCode="%s" AND c.postalCode = p.postalCode' % countryCode_company)
        qb = qb.add_select('c.key.contractId,c.customerId,if(c.stationId IS NULL,"NULL",c.stationId), c.countryCode, c.postalCode, p.latitude, p.longitude, p.altitude, c.key.company')
        qb = qb.add_where('c.key.company = %s' % companyId)
        try:
            pass
            context['report']['trace']['hive_Customers_for_customers_weather'] = qb.execute_query()
        except Exception, e:
            raise ModuleTaskFailed(context,msg='%s' % e)
        
        # STATIONS QUERY
        qb = QueryBuilder(context['clients']['hive'])
        fields2 = [('stationId','string'),('latitude','string'),('longitude','string'),('altitude','float')]
        table_input2 = create_hive_module_input_table(context, 'stations_wS_%s' % companyId, context['config']['module']['paths']['all']+'/customers_weather/stations', fields2, task_UUID)
        qb = qb.add_from('stations', 's').add_insert(table=table_input2)
        qb = qb.add_select('s.key.stationId, s.latitude, s.longitude, s.altitude')
        try:
            pass
            context['report']['trace']['hive_Stations_for_customers_weather'] = qb.execute_query()
        except Exception, e:
            raise ModuleTaskFailed(context,msg='%s' % e)
        
        # MAPREDUCE ALGORITHM TO CALCULATE THE CLOSEST STATIONS TO EACH CONTRACTID
        cmd = ['Rscript', context['config']['module']['R_scripts']['customers_weather.R']]
        r_params = {
                    'input':  context['config']['module']['paths']['all']+'/customers_weather/customers',
                    'output':   context['config']['module']['paths']['all']+'/customers_weather/customers_weather',
                    'stations':  context['config']['module']['paths']['all']+'/customers_weather/stations'
                    }
        try:
            pass
            context['report']['trace']['customers_weather'] = run_rscript(cmd, r_params)
        except Exception, e:
            raise ModuleTaskFailed(context,msg='%s' % e)
        
        #LOAD IN HIVE THE customers_weather TABLE
        delete_hive_module_input_table(context, 'customers_weather_%s_%s' % (companyId,task_UUID))
        fields = [('contractId', 'string'),('customerId','string'),('stationId','string'),('companyId','string'),('distance','float'),('postalCode','string')]
        table_contracts_and_weather = load_table_from_a_text_file_to_hive(context, 'customers_weather_%s' % companyId, context['config']['module']['paths']['all']+'/customers_weather/customers_weather', ',', fields, task_UUID)
        
        #DELETE OLD HIVE TABLES
        delete_hive_module_input_table(context, table_input)
        delete_hive_module_input_table(context, table_input2)
    
    context = update_current_task_progress(context)
    
    
    """Select distinct stationId's for the queried contractId's"""
    
    fields=[('stationId','string')]
    table_input_needed = create_hive_module_input_table(context, 'OT501NeededStations', context['config']['module']['paths']['stationIds'], fields, task_UUID)
    qb = QueryBuilder(context['clients']['hive']).add_insert(table=table_input_needed)
    qb = qb.add_from(table_contracts_and_weather)
    qb = qb.add_select('DISTINCT(stationId)')
    
    try:
         pass
         context['report']['trace']['hive_query_stationIds'] = qb.execute_query()
    except Exception, e:
        raise ModuleTaskFailed(context, msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    
    """Query needed weather data"""
    aux = subprocess.Popen(["hadoop", "fs", "-cat", context['config']['module']['paths']['stationIds']+"/000000_0"], stdout=subprocess.PIPE).stdout.readlines()
    for i in range(len(aux)):
        aux[i] = '"'+aux[i].replace("\n","")+'"'
    needed_stationIds = ", ".join(aux)
    
    qb = QueryBuilder(context['clients']['hive'])
    table_from2 = create_weather_measures_temp_table(context, weather_type, task_UUID)
    fields = [('stationId','string'),('ts','int'),('value','float')]
    table_input2 = create_hive_module_input_table(context, 'OT501Weather', context['config']['module']['paths']['input_from_query']+'_weather', fields, task_UUID)
    qb = qb.add_from(table_from2, 'w').add_insert(table=table_input2)
    qb = qb.add_select('w.key.stationId, unix_timestamp(from_utc_timestamp(from_unixtime(w.key.ts),"%s")), w.value' % timezone_meteo)
    qb = qb.add_where('unix_timestamp(from_utc_timestamp(from_unixtime(w.key.ts),"%s")) >= UNIX_TIMESTAMP("%s","yyyy-MM-dd HH:mm:ss")' % (timezone_meteo,ts_from))
    qb = qb.add_and_where('unix_timestamp(from_utc_timestamp(from_unixtime(w.key.ts),"%s")) <= UNIX_TIMESTAMP("%s","yyyy-MM-dd HH:mm:ss")' % (timezone_meteo,ts_to))
    qb = qb.add_and_where('w.key.stationId in (%s)' % needed_stationIds)
    
    try:
         pass
         context['report']['trace']['hive_query_weather'] = qb.execute_query()
    except Exception, e:
        raise ModuleTaskFailed(context, msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    
    """Corrections of the weather data"""
    
    context['report']['trace']['join_job'] = {}
    cmd = ['Rscript', context['config']['module']['R_scripts']['weather_calculations.R']]
    r_params = {
                'type_of_calculation': 'five_minutes',
                'type_of_data': weather_type,
                'ts_to': str(ts_to),
                'ts_from': str(ts_from),
                'input': context['config']['module']['paths']['input_from_query']+'_weather',
                'output': context['config']['module']['paths']['result_dir']+'_weather',
                'buffer_mr': len(needed_stationIds)*48*(ts_to-ts_from).days
                }
    try:
        pass
        context['report']['trace']['weather_calculations'] = run_rscript(cmd, r_params)
    except Exception, e:
        raise ModuleTaskFailed(context,msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    #Load the treated weather results in Hive
    fields_weather = [('stationId','string'),('ts','int'),('value','float')]
    try:
        table_weather = load_table_from_a_text_file_to_hive(context,'OT501WeatherTreated',context['config']['module']['paths']['result_dir']+'_weather',",",fields_weather,task_UUID)
    except Exception, e:
        raise ModuleTaskFailed(context,msg='%s' % e)
    
    
    
    """
    QUERY AMON MEASURES
    """
    
    qb = QueryBuilder(context['clients']['hive'])
    
    table_from = create_measures_temp_table(context, type, companyId, task_UUID)
    fields = [('contractId','string'),('ts','int'),('value','float'),('stationId','string')]
    table_input = create_hive_module_input_table(context, 'OT501Input', context['config']['module']['paths']['input_from_query'], fields, task_UUID)
    
    qb = qb.add_from(table_from, 'e').add_insert(table=table_input).add_join(table_contracts_and_weather, 'c', 'e.key.contractId = c.contractId')
    qb = qb.add_select('e.key.contractId, unix_timestamp(from_utc_timestamp(from_unixtime(e.key.ts),"%s")), e.value, c.stationId' % timezone)
    qb = qb.add_where('unix_timestamp(from_utc_timestamp(from_unixtime(e.key.ts),"%s")) >= UNIX_TIMESTAMP("%s","yyyy-MM-dd HH:mm:ss")' % (timezone,ts_from))
    qb = qb.add_and_where('unix_timestamp(from_utc_timestamp(from_unixtime(e.key.ts),"%s")) <= UNIX_TIMESTAMP("%s","yyyy-MM-dd HH:mm:ss")' % (timezone,ts_to))
    
    try:
        pass
        context['report']['trace']['hive_query_measures'] = qb.execute_query()
    except Exception, e:
        raise ModuleTaskFailed(context, msg='%s' % e)
    
    context = update_current_task_progress(context)



    """
    JOINING OF THE AMON MEASURES AND THE WEATHER DATA
    """
    
    qb = QueryBuilder(context['clients']['hive'])
    fields = [('contractId','string'),('month','string'),('day','string'),('hour','string'),('minute','string'),('value','float'),('v_weather','float')]
    table_input2 = create_hive_module_input_table(context, 'OT501Joining', context['config']['module']['paths']['input_measures_and_weather'], fields, task_UUID)
    qb = qb.add_from(table_input, 'e').add_insert(table=table_input2).add_join(table_weather, 'w', 'e.stationId = w.stationId AND e.ts = w.ts')
    qb = qb.add_select('e.contractId,\
                        CONCAT(YEAR(from_unixtime(e.ts)),IF(LENGTH(MONTH(from_unixtime(e.ts)))==1,CONCAT(0,\
                            MONTH(from_unixtime(e.ts))),MONTH(from_unixtime(e.ts)))),\
                        CONCAT(YEAR(from_unixtime(e.ts)),IF(LENGTH(MONTH(from_unixtime(e.ts)))==1,CONCAT(0,\
                            MONTH(from_unixtime(e.ts))),MONTH(from_unixtime(e.ts))),\
                            IF(LENGTH(DAY(from_unixtime(e.ts)))==1,CONCAT(0,DAY(from_unixtime(e.ts))),\
                            DAY(from_unixtime(e.ts)))),\
                        IF(LENGTH( HOUR( CAST( from_unixtime(e.ts) as string ) ) )==1 ,\
                            CONCAT(0,HOUR( CAST( from_unixtime(e.ts) as string ) ) ),\
                            HOUR( CAST( from_unixtime(e.ts) as string ) ) ),\
                        IF(LENGTH( MINUTE( CAST( from_unixtime(e.ts) as string ) ) )==1 ,\
                            CONCAT(0,MINUTE( CAST( from_unixtime(e.ts) as string ) ) ),\
                            MINUTE( CAST( from_unixtime(e.ts) as string ) ) ),\
                        e.value, w.value')
    try:
         pass
         context['report']['trace']['hive_query_measures_and_weather'] = qb.execute_query()
    except Exception, e:
        raise ModuleTaskFailed(context, msg='%s' % e)
    
    context = update_current_task_progress(context)



    """
    GENERATE THE RESULTS BY SUBDAY
    """
    
    qb = QueryBuilder(context['clients']['hive'])
    fields = [('key','string'),('hourlyStats','string'),('totalConsumption','float'),('meanTemperature','float')]
    fields_for_json = [('temperature','float','e.v_weather'),('consumption','float','e.value'),('hour','string','concat(e.hour,":",e.minute)')]
    table_input3 = create_hive_module_input_table(context, 'OT501ResultsSubDaily', context['config']['module']['paths']['result_dir']+'_subdaily', fields, task_UUID)
    qb = qb.add_from(table_input2, 'e').add_insert(table=table_input3)
    qb = qb.add_select('concat_ws("~",e.day,"%s",e.contractId),\
                        %s,\
                        sum(e.value),\
                        avg(e.v_weather)' % (companyId,select_json(fields_for_json)) )
    qb = qb.add_groups(['e.day','e.contractId'])
    try:
         pass
         context['report']['trace']['hive_query_results_subdaily'] = qb.execute_query()
    except Exception, e:
        raise ModuleTaskFailed(context, msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    #context['report']['trace']['join_job_daily'] = {}
    #cmd = ['Rscript', context['config']['module']['R_scripts']['join_and_compare_daily.R']]
    #r_params = {
    #            'companyId': companyId,
    #            'input': context['config']['module']['paths']['input_measures_and_weather'],
    #            'output': context['config']['module']['paths']['result_dir']+'_daily'
    #            }
    #try:
    #    pass
    #    context['report']['trace']['join_job_daily'] = run_rscript(cmd, r_params)
    #except Exception, e:
    #    raise ModuleTaskFailed(context,msg='%s' % e)
    #context = update_current_task_progress(context)
    
    
    """
    GENERATE THE RESULTS BY DAY
    """
    if daily_summary_calculation is True:
        qb = QueryBuilder(context['clients']['hive'])
        fields = [('key','string'),('totalConsumption','float'),('meanTemperature','float')]
        table_input3 = create_hive_module_input_table(context, 'OT501ResultsDaily', context['config']['module']['paths']['result_dir']+'_daily', fields, task_UUID)
        qb = qb.add_from(table_input2, 'e').add_insert(table=table_input3)
        qb = qb.add_select('concat_ws("~",e.day,"%s",e.contractId),\
                            sum(e.value),\
                            avg(e.v_weather)' % (companyId) )
        qb = qb.add_groups(['e.day','e.contractId'])
        try:
             pass
             context['report']['trace']['hive_query_results_daily'] = qb.execute_query()
        except Exception, e:
            raise ModuleTaskFailed(context, msg='%s' % e)
        
        context = update_current_task_progress(context)
    
    
    """
    GENERATE THE RESULTS BY MONTH
    """
    if monthly_summary_calculation is True:
        qb = QueryBuilder(context['clients']['hive'])
        fields = [('key','string'),('totalConsumption','float'),('meanTemperature','float')]
        table_input3 = create_hive_module_input_table(context, 'OT501ResultsMonthly', context['config']['module']['paths']['result_dir']+'_monthly', fields, task_UUID)
        qb = qb.add_from(table_input2, 'e').add_insert(table=table_input3)
        qb = qb.add_select('concat_ws("~",e.month,"%s",e.contractId),\
                            sum(e.value),\
                            avg(e.v_weather)' % (companyId) )
        qb = qb.add_groups(['e.month','e.contractId'])
        try:
             pass
             context['report']['trace']['hive_query_results_monthly'] = qb.execute_query()
        except Exception, e:
            raise ModuleTaskFailed(context, msg='%s' % e)
        
        context = update_current_task_progress(context)
        
        
        #context['report']['trace']['join_job_monthly'] = {}
        #cmd = ['Rscript', context['config']['module']['R_scripts']['join_and_compare_monthly.R']]
        #r_params = {
        #            'companyId': companyId,
        #            'input': context['config']['module']['paths']['result_dir']+'_daily',
        #            'output': context['config']['module']['paths']['result_dir']+'_monthly'
        #            }
        #try:
        #    pass
        #    context['report']['trace']['join_job_monthly'] = run_rscript(cmd, r_params)
        #except Exception, e:
        #    raise ModuleTaskFailed(context,msg='%s' % e)
        #context = update_current_task_progress(context)
    
    
    
    """
    Results to REST
    """
    # Write the daily results
    try:
        pass
        context['report']['trace']['load_output_to_rest_job_subdaily'] = load_output_to_rest_data(context, input=context['config']['module']['paths']['result_dir']+'_subdaily',sep="\t",
                                                                                               collection=context['config']['module']['public']['mongodb']['subdaily']['collection'],
                                                                                               fields=context['config']['module']['public']['mongodb']['subdaily']['fields'],
                                                                                               ts_from_remove=get_day(ts_from), ts_to_remove=get_day(ts_to), company_to_remove=companyId,
                                                                                               key = ['day','companyId','contractId','setupId'])
    except Exception, e:
        raise ModuleTaskFailed(context,msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    #Write the monthly results
    if daily_summary_calculation is True:
        try:
            pass
            context['report']['trace']['load_output_to_rest_job_daily'] = load_output_to_rest_data(context, input=context['config']['module']['paths']['result_dir']+'_daily',sep="\t",
                                                                                                     collection=context['config']['module']['public']['mongodb']['daily']['collection'],
                                                                                                     fields=context['config']['module']['public']['mongodb']['daily']['fields'],
                                                                                                     ts_from_remove=get_day(ts_from), ts_to_remove=get_day(ts_to), company_to_remove=companyId,
                                                                                                     key = ['day','companyId','contractId','setupId'])
        except Exception, e:
            raise ModuleTaskFailed(context,msg='%s' % e)
        
        context = update_current_task_progress(context)
    
    #Write the monthly results
    if monthly_summary_calculation is True:
        try:
            pass
            context['report']['trace']['load_output_to_rest_job_monthly'] = load_output_to_rest_data(context, input=context['config']['module']['paths']['result_dir']+'_monthly',sep="\t",
                                                                                                     collection=context['config']['module']['public']['mongodb']['monthly']['collection'],
                                                                                                     fields=context['config']['module']['public']['mongodb']['monthly']['fields'],
                                                                                                     ts_from_remove=get_month(ts_from), ts_to_remove=get_month(ts_to), company_to_remove=companyId)
        except Exception, e:
            raise ModuleTaskFailed(context,msg='%s' % e)
        
        context = update_current_task_progress(context)
    
    """
    Results to HBase
    """
    # Write the daily results
    try:
        pass
        context['report']['trace']['load_output_to_hbase_job_subdaily'] = load_output_to_hbase(context,sep="\t", input=context['config']['module']['paths']['result_dir']+'_subdaily', 
                                                                                            table=context['config']['module']['hbase_table']['subdaily'])
    except Exception, e:
        raise ModuleTaskFailed(context,msg='%s' % e)
    
    context = update_current_task_progress(context)

    #Write the monthly results
    if daily_summary_calculation is True:
        try:
            pass
            context['report']['trace']['load_output_to_hbase_job_daily'] = load_output_to_hbase(context,sep="\t", input=context['config']['module']['paths']['result_dir']+'_daily', 
                                                                                                table=context['config']['module']['hbase_table']['daily'])
        except Exception, e:
            raise ModuleTaskFailed(context,msg='%s' % e)
    
    context = update_current_task_progress(context)
    #Write the monthly results
    if monthly_summary_calculation is True:
        try:
            pass
            context['report']['trace']['load_output_to_hbase_job_monthly'] = load_output_to_hbase(context,sep="\t", input=context['config']['module']['paths']['result_dir']+'_monthly',
                                                                                                  table=context['config']['module']['hbase_table']['monthly'])
        except Exception, e:
            raise ModuleTaskFailed(context,msg='%s' % e)
        
        context = update_current_task_progress(context)
    
    """
    Clean old results from REST
    """
    try:
        pass
        context['report']['trace']['clean_old_data_from_rest_subdaily'] = cleanup_old_data_from_rest(context, time_representation = get_day(ts_to),
        type = "day", collection = context['config']['module']['public']['mongodb']['subdaily']['collection'])
    except Exception, e:
        raise ModuleTaskFailed(context,msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    if daily_summary_calculation is True:
        try:
            pass
            context['report']['trace']['clean_old_data_from_rest_daily'] = cleanup_old_data_from_rest(context, time_representation = get_day(ts_to),
            type = "day", collection = context['config']['module']['public']['mongodb']['daily']['collection'])
        except Exception, e:
            raise ModuleTaskFailed(context,msg='%s' % e)
        
        context = update_current_task_progress(context)
    
    if monthly_summary_calculation is True:
        try:
            pass
            context['report']['trace']['clean_old_data_from_rest_monthly'] = cleanup_old_data_from_rest(context, time_representation = get_month(ts_to),
            type = "month", collection = context['config']['module']['public']['mongodb']['monthly']['collection'])
        except Exception, e:
            raise ModuleTaskFailed(context,msg='%s' % e)
        
        context = update_current_task_progress(context)

    """
    Delete HIVE temporary tables
    """
    try:
        pass
        for table in context['temp_hive_tables']['measures']:
            delete_measures_temp_table(context,table)
        for table in context['temp_hive_tables']['input']:
            delete_hive_module_input_table(context,table)
    except Exception, e:
        raise ModuleTaskFailed(context,msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    """
    Delete the HDFS temporary files
    """
    try:
        pass
        context['report']['trace']['cleanup_temp_data'] = cleanup_temp_data(context, context['config']['module']['paths']['all'], recurse=True)
    except Exception, e:
        raise ModuleTaskFailed(context,msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    
    context['current_task'].update_state(state='SUCCESS', meta=True)
        
    context['report'].save()
    
    logger.info('Module ot501 execution finished...')
    return True
            
if __name__ == '__main__':
    """
    from ... import module_ot501
    module_ot501.async(params)*
    """
"""
    from datetime import datetime
    params = {
            'companyId': 1111111111,
            'ts_to': datetime(2015,5,31,23,59),
            'type': 'electricityConsumption',
            'weather_type': 'temperatureAir',  #temperatureAir, relativeHumidity, rainFall, avgWindSpeed, avgWindDirection, maxWindSpeed, maxWindDirection, barometricPressure, solarRadiation
            'num_months': 24,
            'monthly_summary_calculation': True,
            'daily_summary_calculation': True,
            'relation_contract_weatherStation': True,
            'setupId': ObjectId(),
            'timezone':'Europe/Vienna',
            'timezone_meteo': 'Europe/Vienna'
            }

"""