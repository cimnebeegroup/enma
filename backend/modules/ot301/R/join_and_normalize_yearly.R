
library(RJSONIO)

h = basicJSONHandler()

f <- file("stdin")
open(f)
params <- fromJSON(readLines(f), h)

library(Rhipe)
rhinit()

# variables
companyId <- params$companyId
n_reducers <- 4

# Mapper definition
mapper_yearly <- expression({
			line <- strsplit(unlist(map.values),';')
			mapply(function(line){
						contractId<- unlist(strsplit(line[1],"~"))[3]
						month<-unlist(strsplit(line[1],"~"))[1]
						year<- substr(month,1,4)
						
						key<-paste(year,companyId,contractId,sep="~")
						key_reducer <- prod(strtoi(charToRaw(key)),na.rm=T)
						if(nchar(key_reducer)>5){
							a<-nchar(key_reducer)-5
							b<-nchar(key_reducer)-2
						} else {
							a<-1
							b<-nchar(key_reducer)
						}
						whichReducer<- as.integer(substr(key_reducer,a,b)) %% n_reducers
						
						summary<- fromJSON(line[3],asText=T)
						summary<- do.call("rbind",summary)
						
						rhcollect(c(whichReducer,key),data.frame("timeSlotName"=unlist(summary[,"timeSlot"]),
										"sum"=as.numeric(unlist(summary[,"sum"])),
										"price"=as.numeric(unlist(summary[,"price"])),
										"month"=rep(month,nrow(summary)),
										stringsAsFactors=F)
						)
						
					}, line, SIMPLIFY=FALSE)
		})

# Reducer definition
reducer_yearly <- expression(
		pre = {
			suppressMessages(library(RJSONIO))
		},
		reduce = {
			df<-do.call("rbind",reduce.values)
			df<-data.frame("month"=df[,4],
					"sum"=as.numeric(df[,2]),
					"price"=as.numeric(df[,3]),
					"timeSlotName"=df[,1])
			df<-df[order(as.integer(df[,"month"])),]
			
			month_timeSlot<-paste(df$month,df$timeSlotName,sep="~")
			
			calculation_by_month <- lapply(as.character(unique(month_timeSlot)),
					function(item){
						items<-unlist(strsplit(item,"~"))
						list(
								"month"=as.integer(items[1]),
								"timeSlot"=items[2],
								"consumption"=df[df$month==items[1] & df$timeSlotName==items[2],"sum"],
								"price"=df[df$month==items[1] & df$timeSlotName==items[2],"price"]
						)
					}
			)
			summary<- lapply(as.character(unique(df[,'timeSlotName'])),function(item){
						aux=df[df[,'timeSlotName']==item,]
						if (length(aux[,'sum'])>1){
							list(
									'timeSlot'= item,
									'min'= min(aux[,'sum']),
									'max'= max(aux[,'sum']),
									'mean'= mean(aux[,'sum']),
									'sum'= sum(aux[,'sum']),
									'price'= sum(aux[,'price'])
							)
						} else {
							list(
									'timeSlot'= item,
									'min'= aux[,'sum'],
									'max'= aux[,'sum'],
									'mean'= aux[,'sum'],
									'sum'= aux[,'sum'],
									'price'= aux[,'price']
							)
						}
					})
			
			rhcollect(NULL,c(reduce.key[2],gsub('\n','',toJSON(calculation_by_month, collapse='')),gsub('\n','',toJSON(summary, collapse=''))))
		}
)

monthly <- rhwatch(map=mapper_yearly, reduce=reducer_yearly, partitioner=list(lims=1,type='string'),
		input=rhfmt(params$input,type='text'),
		output=rhfmt(params$output,type='text'),
		read=FALSE,noeval=TRUE,mapred=list(mapred.field.separator=";", mapred.textoutputformat.usekey=FALSE,
				mapred.reduce.tasks=n_reducers),
		setup=expression(map={suppressMessages(library(RJSONIO))}))
job <- rhex(monthly, async=TRUE)
status <- rhstatus(job)
print('Printing rhstatus')
toJSON(status, collapse='', .escapeEscapes = FALSE)


rhclean()