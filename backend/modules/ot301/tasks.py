#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import absolute_import
import sys

from datetime import datetime
import json

#module imports
from modules import ModuleTaskFailed, ModuleTask
from lib.utils import *
from lib.logs import setup_log
from lib.querybuilder import QueryBuilder

# celery imports
from celery_backend import app
from celery import current_task
from celery.utils.log import get_task_logger
from celery.signals import after_setup_task_logger, after_setup_logger

# import function to execute the corresponding ETL's
from ETL.tasks import *

MODULE="ot301"

after_setup_logger.connect(setup_log)
after_setup_task_logger.connect(setup_log)
logger = get_task_logger('module.%s' % MODULE)


class ModuleOT301Task(ModuleTask):
    abstract = True
    def __init__(self):
        logger.debug('ModuleTask init for module %s' % MODULE)
        ModuleTask.__init__(self)
    
    
app.Task = ModuleOT301Task


@app.task(name='ot301')
def module_ot301(params):
    
    """ """
    module_ot301.MODULE = MODULE
    
    """Create context dict to pass to functions"""
    logger.info('Starting Module OT301...')
    
    logger.debug('Builing task context with config, clients and report')
    context = {}
    context['current_task'] = current_task
    context['config'] = module_ot301.config
    
    """task_UUID for paths in HDFS and queries"""
    task_UUID = id_generator()
    context['config']['module']['paths'] = random_paths(context['config']['module']['paths'],'UUID', task_UUID)
    
    try:
        context['clients'] = {
                              'hive': module_ot301.hive,
                              'hdfs': module_ot301.hdfs,
                              'mongo': module_ot301.mongo,
                              'hbase': module_ot301.hbase
                              }
    except Exception, e:
        raise ModuleTaskFailed(context, 'Error connecting to needed connection clients: %s' % e)
    
    
    context['report'] = Report(monogodb_fd=context['clients']['mongo'], db=context['config']['app']['report']['db'], collection=context['config']['app']['report']['collection'])
    context['report']['task_id'] = current_task.request.id
    context['report']['task_UUID_paths_queries'] = task_UUID
    context['report']['started_at'] = datetime.now()
    context['report']['params'] = current_task.request.args
    context['report']['module'] = MODULE
    context['report']['progress'] = 1
    context['report'].update()
         
    
    """Get Month as param and check inconsistencies"""
    try:
        ts_to = params['ts_to']
        num_months = params['num_months']
        ts_from = monthback_date(ts_to,num_months)
        monthly_summary_calculation = params['monthly_summary_calculation']
        yearly_summary_calculation = params['yearly_summary_calculation']
        companyId = params['companyId']
        flatRate_unit = params['flatRate_unit']
        type = params['type']
        timezone = params['timezone']
        setupId = params['setupId']
    except KeyError, e:
        error_msg = 'Not enough parameters provided to module %s: %s' % (MODULE, e)
        logger.error(error_msg)
        raise ModuleTaskFailed(context, msg=error_msg)
    
    
    try:
        month = get_month(ts_to)
    except Exception, e:
        raise ModuleTaskFailed(context, msg='%s' % e)
    
    
    """ STARTING THE MODULE TASKS """
        
    logger.debug('Starting module OT301 calculations for company %s' % companyId)
    
    
    context['report']['trace'] = {}

    """
    Create meta to display task progress
    5 steps module:
    * 1 hive query
    * 1 Rhipe job (joining and normalizing the data)
    * output load into hbase (mrjob)
    * output load into rest (mrjob)
    * cleanup data from rest
    * cleanup the hive temp tables
    * cleanup data temporal data
    """
    
    total_num_tasks = 7 if monthly_summary_calculation is False else 11
    total_num_tasks = total_num_tasks if yearly_summary_calculation is False else total_num_tasks+3
    context['job_meta'] = { 'current': 0, 'total': total_num_tasks }
    
    """Query for the consumption over the periods"""
    qb = QueryBuilder(context['clients']['hive'])
    table_from = create_measures_temp_table(context, type, companyId, task_UUID, tariffs=True)
    fields = [('contractId','string'),('value','float'),('day','string'),('hour','int'),('minute','int'),('energyCost','float'),('flatRate','float'),('timeSlotName','string')]
    table_input = create_hive_module_input_table(context, 'OT301Input',context['config']['module']['paths']['input'], fields, task_UUID)
    qb = qb.add_from(table_from, 'e').add_insert(table=table_input)
    qb = qb.add_select('e.key.contractId, e.value,\
    CONCAT(YEAR(from_utc_timestamp(from_unixtime(e.key.ts),"%s")),IF(LENGTH(MONTH(from_utc_timestamp(from_unixtime(e.key.ts),"%s")))==1,CONCAT(0,\
        MONTH(from_utc_timestamp(from_unixtime(e.key.ts),"%s"))),MONTH(from_utc_timestamp(from_unixtime(e.key.ts),"%s"))),\
        IF(LENGTH(DAY(from_utc_timestamp(from_unixtime(e.key.ts),"%s")))==1,CONCAT(0,DAY(from_utc_timestamp(from_unixtime(e.key.ts),"%s"))),\
        DAY(from_utc_timestamp(from_unixtime(e.key.ts),"%s")))),\
    HOUR( CAST (from_utc_timestamp(from_unixtime(e.key.ts),"%s") as string) ), MINUTE( CAST (from_utc_timestamp(from_unixtime(e.key.ts),"%s") as string) ),\
    e.energyCost,e.flatRate,e.timeSlotName' %(timezone,timezone,timezone,timezone,timezone,timezone,timezone,timezone,timezone))
    qb = qb.add_where('(unix_timestamp(from_utc_timestamp(from_unixtime(e.key.ts),"%s")) >= UNIX_TIMESTAMP("%s","yyyy-MM-dd HH:mm:ss") AND \
                        unix_timestamp(from_utc_timestamp(from_unixtime(e.key.ts),"%s")) <= UNIX_TIMESTAMP("%s","yyyy-MM-dd HH:mm:ss"))' % (timezone,ts_from,timezone,ts_to))
    qb = qb.add_and_where('e.energyCost is not null')
    
    try:
        pass
        context['report']['trace']['hive_query'] = qb.execute_query()
    except Exception, e:
        raise ModuleTaskFailed(context, msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    
    """
    Calculate the detailed consumption (daily results)
    """
    cmd = ['Rscript', context['config']['module']['R_scripts']['join_and_normalize_daily.R']]
    r_params = {
                'companyId': companyId,
                'input': context['config']['module']['paths']['input'],
                'output': context['config']['module']['paths']['result_dir']+'_daily',
                'flatRate_unit': flatRate_unit,
                'value_column':2,
                'yearmonthday_column':3,
                'hour_column': 4,
                'minute_column': 5,
                'energyCost_column': 6,
                'flatRate_column': 7,
                'timeSlotName_column': 8,
                }
    try:
        pass
        context['report']['trace']['join_and_normalize'] = run_rscript(cmd, r_params)
    except Exception, e:
        raise ModuleTaskFailed(context,msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    """
    Calculate the detailed consumption (monthly results)
    """
    if monthly_summary_calculation is True:
        cmd = ['Rscript', context['config']['module']['R_scripts']['join_and_normalize_monthly.R']]
        r_params = {
                    'companyId': companyId,
                    'input': context['config']['module']['paths']['result_dir']+'_daily',
                    'output': context['config']['module']['paths']['result_dir']+'_monthly',
                    }
        try:
            pass
            context['report']['trace']['join_and_normalize_monthly'] = run_rscript(cmd, r_params)
        except Exception, e:
            raise ModuleTaskFailed(context,msg='%s' % e)
        
        context = update_current_task_progress(context)
    
    """
    Calculate the detailed consumption (yearly results)
    """
    if yearly_summary_calculation is True:
        cmd = ['Rscript', context['config']['module']['R_scripts']['join_and_normalize_yearly.R']]
        r_params = {
                    'companyId': companyId,
                    'input': context['config']['module']['paths']['result_dir']+'_monthly',
                    'output': context['config']['module']['paths']['result_dir']+'_yearly',
                    }
        try:
            pass
            context['report']['trace']['join_and_normalize_yearly'] = run_rscript(cmd, r_params)
        except Exception, e:
            raise ModuleTaskFailed(context,msg='%s' % e)
        
        context = update_current_task_progress(context)
        
    """
    Results to REST
    """
    # Write the daily results
    try:
        pass
        context['report']['trace']['load_output_to_rest_job_daily'] = load_output_to_rest_data(context, input=context['config']['module']['paths']['result_dir']+'_daily',sep=";",
                                                                                               collection=context['config']['module']['public']['mongodb']['daily']['collection'],
                                                                                               fields=context['config']['module']['public']['mongodb']['daily']['fields'],
                                                                                               ts_from_remove=get_day(ts_from), ts_to_remove=get_day(ts_to), company_to_remove=companyId,
                                                                                               key = ['day','companyId','contractId','setupId'])
    except Exception, e:
        raise ModuleTaskFailed(context,msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    #Write the monthly results
    if monthly_summary_calculation is True:
        try:
            pass
            context['report']['trace']['load_output_to_rest_job_monthly'] = load_output_to_rest_data(context, input=context['config']['module']['paths']['result_dir']+'_monthly',sep=";",
                                                                                                     collection=context['config']['module']['public']['mongodb']['monthly']['collection'],
                                                                                                     fields=context['config']['module']['public']['mongodb']['monthly']['fields'],
                                                                                                     ts_from_remove=get_month(ts_from), ts_to_remove=get_month(ts_to), company_to_remove=companyId)
        except Exception, e:
            raise ModuleTaskFailed(context,msg='%s' % e)
        
        context = update_current_task_progress(context)
    
    if yearly_summary_calculation is True:
        try:
            pass
            context['report']['trace']['load_output_to_rest_job_yearly'] = load_output_to_rest_data(context, input=context['config']['module']['paths']['result_dir']+'_yearly',sep=";",
                                                                                                     collection=context['config']['module']['public']['mongodb']['yearly']['collection'],
                                                                                                     fields=context['config']['module']['public']['mongodb']['yearly']['fields'],
                                                                                                     ts_from_remove=get_year(ts_from), ts_to_remove=get_year(ts_to), company_to_remove=companyId,
                                                                                                     key = ['year','companyId','contractId','setupId'])
        except Exception, e:
            raise ModuleTaskFailed(context,msg='%s' % e)
        
        context = update_current_task_progress(context)
    
    """
    Results to HBase
    """
    # Write the daily results
    try:
        pass
        context['report']['trace']['load_output_to_hbase_job_daily'] = load_output_to_hbase(context,sep=";", input=context['config']['module']['paths']['result_dir']+'_daily', 
                                                                                            table=context['config']['module']['hbase_table']['daily'])
    except Exception, e:
        raise ModuleTaskFailed(context,msg='%s' % e)
    
    context = update_current_task_progress(context)

    #Write the monthly results
    if monthly_summary_calculation is True:
        try:
            pass
            context['report']['trace']['load_output_to_hbase_job_monthly'] = load_output_to_hbase(context,sep=";", input=context['config']['module']['paths']['result_dir']+'_monthly',
                                                                                                  table=context['config']['module']['hbase_table']['monthly'])
        except Exception, e:
            raise ModuleTaskFailed(context,msg='%s' % e)
        
        context = update_current_task_progress(context)
    
    #Write the yearly results
    if yearly_summary_calculation is True:
        try:
            pass
            context['report']['trace']['load_output_to_hbase_job_yearly'] = load_output_to_hbase(context,sep=";", input=context['config']['module']['paths']['result_dir']+'_yearly',
                                                                                                  table=context['config']['module']['hbase_table']['yearly'])
        except Exception, e:
            raise ModuleTaskFailed(context,msg='%s' % e)
        
        context = update_current_task_progress(context)
    
    """
    Clean old results from REST
    """
    try:
        pass
        context['report']['trace']['clean_old_data_from_rest'] = cleanup_old_data_from_rest(context, time_representation = get_day(ts_to),
        type = "day", collection = context['config']['module']['public']['mongodb']['daily']['collection'])
    except Exception, e:
        raise ModuleTaskFailed(context,msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    if monthly_summary_calculation is True:
        try:
            pass
            context['report']['trace']['clean_old_data_from_rest'] = cleanup_old_data_from_rest(context, time_representation = get_month(ts_to),
            type = "month", collection = context['config']['module']['public']['mongodb']['monthly']['collection'])
        except Exception, e:
            raise ModuleTaskFailed(context,msg='%s' % e)
        
        context = update_current_task_progress(context)
    
    """
    Delete HIVE temporary tables
    """
    try:
        pass
        for table in context['temp_hive_tables']['measures']:
            delete_measures_temp_table(context,table)
        for table in context['temp_hive_tables']['input']:
            delete_hive_module_input_table(context,table)
    except Exception, e:
        raise ModuleTaskFailed(context,msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    """
    Delete the HDFS temporary files
    """
    try:
        pass
        context['report']['trace']['cleanup_temp_data'] = cleanup_temp_data(context, context['config']['module']['paths']['all'], recurse=True)
    except Exception, e:
        raise ModuleTaskFailed(context,msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    
    context['current_task'].update_state(state='SUCCESS', meta=True)
        
    context['report'].save()
    
    logger.info('Module OT301 execution finished...')
    
    return True
            
if __name__ == '__main__':
    """
    from ... import module_ot301
    module_ot301.async(params)*
    """
"""
from datetime import datetime
from bson import ObjectId
params = {
        'companyId': 1111111111,
        'ts_to': datetime(2015,2,28,23,59),
        'num_months':36,
        'flatRate_unit': 'euros/year', # 'euros/year','euros/day','euros/(day*power[w])'
        'monthly_summary_calculation': True,
        'yearly_summary_calculation': True,
        'type': 'electricityConsumption',
        'setupId': ObjectId(),
        'timezone':'Europe/Vienna'
        }
"""
