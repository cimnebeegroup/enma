#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import absolute_import
import sys

from datetime import datetime,timedelta
import json

#module imports
from modules import ModuleTaskFailed, ModuleTask
from lib.utils import *
from lib.logs import setup_log
from lib.querybuilder import QueryBuilder

# celery imports
from celery_backend import app
from celery import current_task
from celery.utils.log import get_task_logger
from celery.signals import after_setup_task_logger, after_setup_logger

# import function to execute the corresponding ETL's
from ETL.tasks import *

MODULE="ot703"

after_setup_logger.connect(setup_log)
after_setup_task_logger.connect(setup_log)
logger = get_task_logger('module.%s' % MODULE)


class ModuleOT703Task(ModuleTask):
    abstract=True
    def __init__(self):
        logger.debug('ModuleTask init for module %s' % MODULE)
        ModuleTask.__init__(self)
    
app.Task = ModuleOT703Task


@app.task(name='ot703')
def module_ot703(params):
    
    """ """
    module_ot703.MODULE = MODULE
    
    """Create context dict to pass to functions"""
    logger.info('Starting Module OT703...')
    
    logger.debug('Builing task context with config, clients and report')
    context = {}
    context['current_task'] = current_task
    context['config'] = module_ot703.config
    
    """task_UUID for paths in HDFS and queries"""
    task_UUID = id_generator()
    context['config']['module']['paths'] = random_paths(context['config']['module']['paths'],'UUID', task_UUID)
    
    try:
        context['clients'] = {
                              'hive': module_ot703.hive,
                              'hdfs': module_ot703.hdfs,
                              'mongo': module_ot703.mongo,
                              'hbase': module_ot703.hbase
                              }
    except Exception, e:
        raise ModuleTaskFailed(context, 'Error connecting to needed connection clients: %s' % e)
    
    
    context['report'] = Report(monogodb_fd=context['clients']['mongo'], db=context['config']['app']['report']['db'], collection=context['config']['app']['report']['collection'])
    context['report']['task_id'] = current_task.request.id
    context['report']['task_UUID_paths_queries'] = task_UUID
    context['report']['started_at'] = datetime.now()
    context['report']['params'] = current_task.request.args
    context['report']['module'] = MODULE
    context['report']['progress'] = 1
    context['report'].update()
    
    
    """Get Month as param and check inconsistencies"""
    try:
        ts_to = params['ts_to']
        num_months = params['num_months']
        ts_from = monthback_date(ts_to,num_months)
        ts_from_previous = firstday_previousyear(ts_from)
        companyId = params['companyId']
        type = params['type']
        setupId = params['setupId']
        timezone = params['timezone']
        
    except KeyError, e:
        error_msg = 'Not enough parameters provided to module %s: %s' % (MODULE, e)
        logger.error(error_msg)
        raise ModuleTaskFailed(context, msg=error_msg)
    
    
    try:
        month = get_month(ts_to)
        months = get_various_months(ts_to,num_months)
    except Exception, e:
        raise ModuleTaskFailed(context, msg='%s' % e)

    
    
    """ STARTING THE MODULE TASKS """
        
    logger.debug('Starting module OT703 calculations for company %s' % companyId)
    
    
    context['report']['trace'] = {}

    """
    Create meta to display task progress
    5 steps module:
    * 1 hive query
    * 1 Rhipe job (Insert the correct key)
    * output load into hbase (mrjob)
    * output load into rest (mrjob)
    * cleanup data from rest
    * cleanup the hive temp tables
    * cleanup data temporal data
    """
    
    context['job_meta'] = { 'current': 0, 'total': 7 }
    
    
    """Query for the actual and previous consumption"""
    qb = QueryBuilder(context['clients']['hive'])
    table_from = create_measures_temp_table(context, type, companyId, task_UUID)
    fields = [('contractId','string'),('value','float'),('yearmonth','int')]
    table_input = create_hive_module_input_table(context, 'OT703Input',context['config']['module']['paths']['input'], fields, task_UUID)
    qb = qb.add_from(table_from, 'e').add_insert(table=table_input)#.add_join('customers','c','c.key.contractId=e.key.contractId and c.key.company=%s' % companyId)
    qb = qb.add_select('e.key.contractId, sum(e.value),\
        CONCAT(YEAR(from_utc_timestamp(from_unixtime(e.key.ts),"%s")),IF(LENGTH(MONTH(from_utc_timestamp(from_unixtime(e.key.ts),"%s")))==1,CONCAT(0,\
            MONTH(from_utc_timestamp(from_unixtime(e.key.ts),"%s"))),MONTH(from_utc_timestamp(from_unixtime(e.key.ts),"%s"))))' % (timezone,timezone,timezone,timezone))
    qb = qb.add_where('unix_timestamp(from_utc_timestamp(from_unixtime(e.key.ts),"%s")) >= UNIX_TIMESTAMP("%s","yyyy-MM-dd HH:mm:ss")\
        AND unix_timestamp(from_utc_timestamp(from_unixtime(e.key.ts),"%s")) <= UNIX_TIMESTAMP("%s","yyyy-MM-dd HH:mm:ss")' % (timezone,ts_from_previous,
        timezone,ts_to))
    qb = qb.add_groups(['e.key.contractId','YEAR(from_utc_timestamp(from_unixtime(e.key.ts),"%s"))'% timezone,
                        'MONTH(from_utc_timestamp(from_unixtime(e.key.ts),"%s"))'% timezone])
    
    try:
        pass
        context['report']['trace']['hive_query'] = qb.execute_query()
    except Exception, e:
        raise ModuleTaskFailed(context, msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    
    """
    Make a correction on the key ID, and creation of the results file
    """
    cmd = ['Rscript', context['config']['module']['R_scripts']['join_and_normalize.R']]
    r_params = {
                'companyId': companyId,
                'last_month_for_calculation': month,
                'num_months': num_months,
                'value_column':2,
                'yearmonth_column':3,
                'input': context['config']['module']['paths']['input'],
                'output': context['config']['module']['paths']['result_dir']
               }
    try:
        pass
        context['report']['trace']['join_and_normalize'] = run_rscript(cmd, r_params)
    except Exception, e:
        raise ModuleTaskFailed(context,msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    
    """
    Now we have results on HDFS so we can load into REST service
    """
    
    try:
        pass
        context['report']['trace']['load_output_to_rest_job'] = load_output_to_rest_data(context, input=context['config']['module']['paths']['result_dir'], fields=context['config']['module']['public']['mongodb']['fields'],
                                                                                        ts_to_remove=int(str(get_year(ts_to))+'12'), ts_from_remove=get_month(ts_from), company_to_remove=companyId)
    except Exception, e:
        raise ModuleTaskFailed(context,msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    
    try:
        pass
        context['report']['trace']['load_output_to_hbase_job'] = load_output_to_hbase(context, input=context['config']['module']['paths']['result_dir'], table=context['config']['module']['hbase_table'])
    except Exception, e:
        raise ModuleTaskFailed(context,msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    
    try:
        pass
        context['report']['trace']['clean_old_data_from_rest'] = cleanup_old_data_from_rest(context, month)
    except Exception, e:
        raise ModuleTaskFailed(context,msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    
    """ Delete the hive temp tables """
    try:
        pass
        for table in context['temp_hive_tables']['measures']:
            delete_measures_temp_table(context,table)
        for table in context['temp_hive_tables']['input']:
            delete_hive_module_input_table(context,table)
    except Exception, e:
        raise ModuleTaskFailed(context,msg='%s' % e)

    context = update_current_task_progress(context)
    

    try:
        pass
        context['report']['trace']['cleanup_temp_data'] = cleanup_temp_data(context, context['config']['module']['paths']['all'], recurse=True)
    except Exception, e:
        raise ModuleTaskFailed(context,msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    
    context['current_task'].update_state(state='SUCCESS', meta=True)
        
    context['report'].save()
    
    logger.info('Module OT703 execution finished...')
    
    return True
"""
if __name__ == '__main__':
    
    from ... import module_ot703
    module_ot703.async(params)*
    
"""
"""
params = {
        'companyId': 1111111111,
        'ts_to': datetime(2015,12,31,23,59),
        'num_months': 12,
        'type': 'electricityConsumption',
        'setupId': ObjectId(),
        'timezone': 'Europe/Rome'
        }
"""