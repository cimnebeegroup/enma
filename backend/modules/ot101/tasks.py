#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import absolute_import
import sys

from datetime import datetime
import json
import itertools

#module imports
from modules import ModuleTaskFailed, ModuleTask
from lib.utils import *
from lib.logs import setup_log
from lib.querybuilder import QueryBuilder

# celery imports
from celery_backend import app
from celery import current_task
from celery.utils.log import get_task_logger
from celery.signals import after_setup_task_logger, after_setup_logger

# import function to execute the corresponding ETL's
from ETL.tasks import *

MODULE="ot101"

after_setup_logger.connect(setup_log)
after_setup_task_logger.connect(setup_log)
logger = get_task_logger('module.%s' % MODULE)


class ModuleOT101Task(ModuleTask):
    abstract = True
    def __init__(self):
        logger.debug('ModuleTask init for module %s' % MODULE)
        ModuleTask.__init__(self)
    
app.Task = ModuleOT101Task


@app.task(name='ot101')
def module_ot101(params):
    
    """ """
    module_ot101.MODULE = MODULE
    
    """Create context dict to pass to functions"""
    logger.info('Starting Module ot101...')
    
    logger.debug('Builing task context with config, clients and report')
    context = {}
    context['current_task'] = current_task
    context['config'] = module_ot101.config
    
    """task_UUID for paths in HDFS and queries"""
    task_UUID = id_generator()
    context['config']['module']['paths'] = random_paths(context['config']['module']['paths'],'UUID', task_UUID)
    
    try:
        context['clients'] = {
                              'hive': module_ot101.hive,
                              'hdfs': module_ot101.hdfs,
                              'mongo': module_ot101.mongo,
                              'hbase': module_ot101.hbase
                              }
    except Exception, e:
        raise Exception('Error connecting to needed connection clients: %s' % e)
        #raise ModuleTaskFailed(context, 'Error connecting to needed connection clients: %s' % e)
    
    
    context['report'] = Report(monogodb_fd=context['clients']['mongo'], db=context['config']['app']['report']['db'], collection=context['config']['app']['report']['collection'])
    context['report']['task_id'] = current_task.request.id
    context['report']['task_UUID_paths_queries'] = task_UUID
    context['report']['started_at'] = datetime.now()
    context['report']['params'] = current_task.request.args
    context['report']['module'] = MODULE
    context['report']['progress'] = 1
    context['report'].update()
    
    
    """Get Month as param and check inconsistencies"""
    try:
        ts_to = params['ts_to']
        num_months = params['num_months']
        ts_from = monthback_date(ts_to,num_months)
        eff_users = float(params['eff_users'])/100 if params['eff_users']>=1 else params['eff_users']
        criteria = sorted([sorted(criteria_set.split(" + ")) for criteria_set in params['criteria']])
        triggerValue = params['triggerValue'] if 'triggerValue' in params else 25
        type = params['type']
        companyId = params['companyId']
        quantile_min = params['quantile_min']
        quantile_max = params['quantile_max']
        timezone = params['timezone']
        setupId = params['setupId']
        run_complementary = params['run_complementary'] if 'run_complementary' in params else False 
        dailyResampling_company = context['clients']['mongo'][context['config']['app']['mongodb']['db']]['companies'].find_one(
                                  {'companyId':companyId} ) ['dailyResample']
    except KeyError, e:
        error_msg = 'Not enough parameters provided to module %s: %s' % (MODULE, e)
        logger.error(error_msg)
        raise ModuleTaskFailed(context, msg=error_msg)
    
    
    try:
        month = get_month(ts_to)
    except Exception, e:
        raise ModuleTaskFailed(context, msg='%s' % e)

    
    """ STARTING THE MODULE TASKS """
    
    logger.debug('Starting module ot101 calculations for company %s' % companyId)
    
    context['report']['trace'] = {}
    
    """
    Create meta to display task progress
    9 steps module:
    * 1 hive queries
    * 3 rhipe jobs
    * output load into hbase (mrjob)
    * output load into rest (mrjob)
    * cleanup the data from rest
    * cleanup the data from hdfs
    * cleanup the temporary tables from hive
    """
    
    context['job_meta'] = { 'current': 0, 'total': 11 } 
    
        
    # Joining the different list of criterias to eliminate repeated ones
    merged = list(itertools.chain.from_iterable(criteria))
    all_criteria_fields = list(set(merged))
    
    # Criterias definition for queries
    criteria_query = []
    fields_measures = [('contractId', 'string'),('yearmonth','int'),('value','float'),('number_days','int')]
   
    for i in xrange(0,len(all_criteria_fields)):
        
        # Precalculated criterias on selection part of the query
        if len(all_criteria_fields[i].split("__"))>=2:
            sentence_to_query = criteria_querybuilder(all_criteria_fields[i],'c')
            criteria_query.append(sentence_to_query)
                    
        # Standard criterias on selection part of the query
        else:
            criteria_query.append("c."+all_criteria_fields[i])
        
        fields_measures.append((all_criteria_fields[i],"string"))     
    
    
    
    """
    Hive Query of the consumptions. Aggregated by yearmonthday
    """
    
    qb = QueryBuilder(context['clients']['hive'])
    table_from = "%s_%s_HDFS" % (params['type'], companyId)
    fields_measures_daily = [('contractId', 'string'),('yearmonthday','int'),('value','float')]
    table_input_daily = create_hive_module_input_table(context, 'OT101InputDaily', context['config']['module']['paths']['input']+'_daily', fields_measures_daily, task_UUID)
    qb = qb.add_from(table_from, 'e').add_insert(table=table_input_daily)
    qb = qb.add_select('e.contractId,\
                        CONCAT( cast(e.year as string), IF( LENGTH(cast(e.month as string))==1,CONCAT(0,cast(e.month as string)),cast(e.month as string) ),\
                        IF( LENGTH(cast(day(from_unixtime(e.ts)) as string))==1,CONCAT(0,cast(day(from_unixtime(e.ts)) as string)),cast(day(from_unixtime(e.ts)) as string) ) ),\
                        sum(e.value)' )
    qb = qb.add_where('e.ts >= UNIX_TIMESTAMP("%s","yyyy-MM-dd HH:mm:ss")' % (ts_from))
    qb = qb.add_and_where('e.ts <= UNIX_TIMESTAMP("%s","yyyy-MM-dd HH:mm:ss")' % (ts_to))
    qb = qb.add_and_where('e.value IS NOT NULL')
    qb = qb.add_groups(['e.contractId', 'e.month','e.year','day(from_unixtime(e.ts))'])
    
    try:
        pass
        context['report']['trace']['hive_query_daily'] = qb.execute_query()
    except Exception, e:
        raise ModuleTaskFailed(context, msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    
    
    """
    Hive Query of the consumptions. Aggregated by yearmonths
    """
    
    qb = QueryBuilder(context['clients']['hive'])
    table_input = create_hive_module_input_table(context, 'OT101Input', context['config']['module']['paths']['input'], fields_measures, task_UUID)
    qb = qb.add_from(table_input_daily, 'e').add_insert(table=table_input).add_join('customers', 'c', 'e.contractId = c.key.contractId')
    qb = qb.add_select('e.contractId,substr(e.yearmonthday,1,6),sum(e.value),count(e.value),\
                        %s' % (", ".join(criteria_query)))
    qb = qb.add_where('c.key.company = %s' % companyId)
    qb = qb.add_groups(['e.contractId', 'substr(e.yearmonthday,1,6)', '%s' % (", ".join(criteria_query))])
    
    try:
        pass
        context['report']['trace']['hive_query_input'] = qb.execute_query()
    except Exception, e:
        raise ModuleTaskFailed(context, msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    
    """
    Map Reduce calculation of the average consumptions depending the criteria
    """
    
    for criteria_item in criteria:
        cmd = ['Rscript', context['config']['module']['R_scripts']['MR_mean_meaneff.R']]
        r_params = {
                    'input': context['config']['module']['paths']['input'],
                    'output': context['config']['module']['paths']['means']+'/'+'-'.join(criteria_item),
                    'eff_users': eff_users,
                    'key_column': 5,
                    'number_days_column': 4,
                    'value_column': 3,
                    'yearmonth_column': 2,
                    'all_criteria_fields': ",".join(all_criteria_fields),
                    'criteria': ",".join(criteria_item),
                    'quantile_min': quantile_min,
                    'quantile_max': quantile_max,
                    'dailyResampling_company': dailyResampling_company
                    }
        try:
            pass
            context['report']['trace']['MR_job_'+"-".join(criteria_item)] = run_rscript(cmd, r_params)
        except Exception, e:
            raise ModuleTaskFailed(context, msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    
    """
    Creation of the summary table of results depending the grouping criteria
    """
    if run_complementary:
        for criteria_item in criteria:
            cmd = ['Rscript', context['config']['module']['R_scripts']['sequence_to_text.R']]
            r_params = {
                        'input': context['config']['module']['paths']['means']+'/'+'-'.join(criteria_item),
                        'output': context['config']['module']['paths']['summary_result_dir']+'/'+'-'.join(criteria_item),
                        'criteria': "-".join(criteria_item),
                        'companyId': companyId,
                        }
            try:
                pass
                context['report']['trace']['sequence_to_text_%s' % '-'.join(criteria_item)] = run_rscript(cmd, r_params)
            except Exception, e:
                raise ModuleTaskFailed(context, msg='%s' % e)
        
    context = update_current_task_progress(context)
    
    
    """
    Once we have calculate groups average we join results with users single consumption and calculate the other needed output
    """
    cmd = ['Rscript', context['config']['module']['R_scripts']['join_and_compare_with_others.R']]
    r_params = {
                'all_criteria_fields': ",".join(all_criteria_fields),
                'criterias': ",".join(["-".join(criteria_item) for criteria_item in criteria]),
                'companyId': companyId,
                'triggerValue': triggerValue,
                'key_column': 5,
                'number_days_column': 4,
                'contractId_column': 1,
                'yearmonth_column': 2,
                'value_column': 3,
                'paths': context['config']['module']['paths'],
                'input': context['config']['module']['paths']['input'],
                'output': context['config']['module']['paths']['result_dir'],
                'path_means': context['config']['module']['paths']['means'],
                'dailyResampling_company': dailyResampling_company
                }
    
    try:
        pass
        context['report']['trace']['join_job'] = run_rscript(cmd, r_params)
    except Exception, e:
        raise ModuleTaskFailed(context,msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    
    """
    Now we have results on HDFS so we can load into REST service and HBase the final results and the summary tables.
    """
    #Loading to Rest the final results
    try:
        pass
        context['report']['trace']['load_output_to_rest_job'] = load_output_to_rest_data(context, input=context['config']['module']['paths']['result_dir'], fields=context['config']['module']['public']['mongodb']['fields'],
                                                                                         ts_from_remove=get_month(ts_from), ts_to_remove=get_month(ts_to), company_to_remove=companyId)
    except Exception, e:
        raise ModuleTaskFailed(context,msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    #Loading to Hbase the summary grouping criteria table
    if run_complementary:
        for criteria_item in criteria:
            try:
                pass
                context['report']['trace']['load_summary_table_to_hbase_in_%s' % "-".join(criteria_item)] = load_output_to_hbase(context, input = "%s/%s" % (context['config']['module']['paths']['summary_result_dir'],"-".join(criteria_item)), table=context['config']['module']['hbase_summaryTable'])
            except Exception, e:
                raise ModuleTaskFailed(context,msg='%s' % e)
            
    context = update_current_task_progress(context)
    
    #Loading to Hbase the final results
    try:
        pass
        context['report']['trace']['load_output_to_hbase_job'] = load_output_to_hbase(context, input=context['config']['module']['paths']['result_dir'], table=context['config']['module']['hbase_table'])
    except Exception, e:
        raise ModuleTaskFailed(context,msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    """ Clean old and temporary data """
    # Old data from REST
    try:
        pass
        context['report']['trace']['clean_old_data_from_rest'] = cleanup_old_data_from_rest(context, month)
    except Exception, e:
        raise ModuleTaskFailed(context,msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    # Delete HIVE temporary tables
    try:
        pass
        for table in context['temp_hive_tables']['measures']:
            delete_measures_temp_table(context,table)
        for table in context['temp_hive_tables']['input']:
            delete_hive_module_input_table(context,table)
    except Exception, e:
        raise ModuleTaskFailed(context,msg='%s' % e)
    logger.debug("Deleted hive temporary tables: %s, %s" % (table_from, table_input))
    
    context = update_current_task_progress(context)
    
    # Delete HDFS temporary files
    try:
        pass
        context['report']['trace']['cleanup_temp_data'] = cleanup_temp_data(context, context['config']['module']['paths']['all'], recurse=True)
    except Exception, e:
        raise ModuleTaskFailed(context,msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    
    context['current_task'].update_state(state='SUCCESS', meta=True)
    
    context['report'].save()
    
    logger.info('Module OT101 execution finished...')
    
    return True
            
if __name__ == '__main__':
    """
    from ... import module_ot101
    module_ot101.async(params)*
    """
"""
    from datetime import datetime
    from bson import ObjectId
    import time
    params = {
            'companyId': 1111111111,
            'ts_to': datetime(2015,1,31,23,59,59),
            'num_months': 12,
            'eff_users': 0.2,
            'criteria': ['criteria_1'],
            'triggerValue': 10,
            'type': 'electricityConsumption',
            'quantile_min': 0.15, #Put 0 to consider data from the minimum consumption in the average calculations
            'quantile_max': 0.997, #Put 1 to consider data up to the maximum consumption in the average calculations
            'timezone':"Europe/Madrid",
            'setupId': ObjectId()
            }
    
"""