#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import absolute_import
import sys

from datetime import datetime,timedelta
import json

#module imports
from modules import ModuleTaskFailed, ModuleTask
from lib.utils import *
from lib.logs import setup_log
from lib.querybuilder import QueryBuilder

# celery imports
from celery_backend import app
from celery import current_task
from celery.utils.log import get_task_logger
from celery.signals import after_setup_task_logger, after_setup_logger

# import function to execute the corresponding ETL's
from ETL.tasks import *

MODULE="ot201"

after_setup_logger.connect(setup_log)
after_setup_task_logger.connect(setup_log)
logger = get_task_logger('module.%s' % MODULE)


class ModuleOT201Task(ModuleTask):
    abstract=True
    def __init__(self):
        logger.debug('ModuleTask init for module %s' % MODULE)
        ModuleTask.__init__(self)
    
app.Task = ModuleOT201Task


@app.task(name='ot201')
def module_ot201(params):
    
    """ """
    module_ot201.MODULE = MODULE
    
    """Create context dict to pass to functions"""
    logger.info('Starting Module OT201...')
    
    logger.debug('Builing task context with config, clients and report')
    context = {}
    context['current_task'] = current_task
    context['config'] = module_ot201.config
    
    """task_UUID for paths in HDFS and queries"""
    task_UUID = id_generator()
    context['config']['module']['paths'] = random_paths(context['config']['module']['paths'],'UUID', task_UUID)
    
    try:
        context['clients'] = {
                              'hive': module_ot201.hive,
                              'hdfs': module_ot201.hdfs,
                              'mongo': module_ot201.mongo,
                              'hbase': module_ot201.hbase
                              }
    except Exception, e:
        raise ModuleTaskFailed(context, 'Error connecting to needed connection clients: %s' % e)
    
    
    context['report'] = Report(monogodb_fd=context['clients']['mongo'], db=context['config']['app']['report']['db'], collection=context['config']['app']['report']['collection'])
    context['report']['task_id'] = current_task.request.id
    context['report']['task_UUID_paths_queries'] = task_UUID
    context['report']['started_at'] = datetime.now()
    context['report']['params'] = current_task.request.args
    context['report']['module'] = MODULE
    context['report']['progress'] = 1
    context['report'].update()
         
    
    """Get Month as param and check inconsistencies"""
    try:
        ts_to = params['ts_to']
        num_months = params['num_months']
        ts_from = monthback_date(ts_to,num_months)
        companyId = params['companyId']
        type = params['type']
        timezone = params['timezone']
        setupId = params['setupId']
        
    except KeyError, e:
        error_msg = 'Not enough parameters provided to module %s: %s' % (MODULE, e)
        logger.error(error_msg)
        raise ModuleTaskFailed(context, msg=error_msg)
    
    
    try:
        month = get_month(ts_to)
    except Exception, e:
        raise ModuleTaskFailed(context, msg='%s' % e)
    
    
    """ STARTING THE MODULE TASKS """
        
    logger.debug('Starting module OT201 calculations for company %s' % companyId)
    
    
    context['report']['trace'] = {}

    """
    Create meta to display task progress
    5 steps module:
    * 1 hive query
    * 1 Rhipe job (Insert the correct key)
    * output load into hbase (mrjob)
    * output load into rest (mrjob)
    * cleanup data from rest
    * cleanup the hive temp tables
    * cleanup data temporal data
    """
    
    context['job_meta'] = { 'current': 0, 'total': 8 }
 
    """Calculation of the previous time (12 months before)"""
    ts_from_previous = monthback_date(ts_from,12)-timedelta(minutes=1)
    ts_to_previous = monthback_date(ts_to,12)-timedelta(minutes=1)
    

    """Hive Query of the consumptions. Aggregated by yearmonthday """
    
    qb = QueryBuilder(context['clients']['hive'])
    table_from = "%s_%s_HDFS" % (params['type'], companyId)
    fields_measures_daily = [('contractId', 'string'),('yearmonthday','int'),('value','float')]
    table_input_daily = create_hive_module_input_table(context, 'OT201InputDaily', context['config']['module']['paths']['input']+'_daily', fields_measures_daily, task_UUID)
    qb = qb.add_from(table_from, 'e').add_insert(table=table_input_daily)
    qb = qb.add_select('e.contractId,\
                        CONCAT( cast(e.year as string), IF( LENGTH(cast(e.month as string))==1,CONCAT(0,cast(e.month as string)),cast(e.month as string) ),\
                        IF( LENGTH(cast(day(from_unixtime(e.ts)) as string))==1,CONCAT(0,cast(day(from_unixtime(e.ts)) as string)),cast(day(from_unixtime(e.ts)) as string) ) ),\
                        sum(e.value)' )
    qb = qb.add_where('((e.ts >= UNIX_TIMESTAMP("%s","yyyy-MM-dd HH:mm:ss") \
                        AND e.ts <= UNIX_TIMESTAMP("%s","yyyy-MM-dd HH:mm:ss")) OR\
                        (e.ts >= UNIX_TIMESTAMP("%s","yyyy-MM-dd HH:mm:ss") \
                        AND e.ts <= UNIX_TIMESTAMP("%s","yyyy-MM-dd HH:mm:ss")))' % (ts_from,ts_to,ts_from_previous,ts_to_previous))
    qb = qb.add_and_where('e.value IS NOT NULL')
    qb = qb.add_groups(['e.contractId', 'e.month','e.year','day(from_unixtime(e.ts))'])
    
    try:
        pass
        context['report']['trace']['hive_query_daily'] = qb.execute_query()
    except Exception, e:
        raise ModuleTaskFailed(context, msg='%s' % e)
    
    context = update_current_task_progress(context)

    
    """Query for the actual and previous monthly consumption"""
    qb = QueryBuilder(context['clients']['hive'])
    fields_measures = [('contractId', 'string'),('yearmonth','int'),('value','float'),('number_days','int')]
    table_input = create_hive_module_input_table(context, 'OT201Input', context['config']['module']['paths']['input'], fields_measures, task_UUID)
    qb = qb.add_from(table_input_daily, 'e').add_insert(table=table_input)
    qb = qb.add_select('e.contractId, substr(e.yearmonthday,1,6), sum(e.value), count(e.value)')
    qb = qb.add_groups(['e.contractId', 'substr(e.yearmonthday,1,6)'])
    
    
    #qb = QueryBuilder(context['clients']['hive'])
    #table_from = create_measures_temp_table(context, type, companyId, task_UUID)
    #fields = [('contractId','string'),('value','float'),('month','int'),('year','int'),('postalCode','string')]
    #table_input = create_hive_module_input_table(context, 'OT201Input',context['config']['module']['paths']['input'], fields, task_UUID)
    #qb = qb.add_from(table_from, 'e').add_insert(table=table_input).add_join('customers', 'c', 'e.key.contractId = c.key.contractId')
    #qb = qb.add_select('c.key.contractId, sum(e.value),month(from_utc_timestamp(from_unixtime(e.key.ts),"%s")),year(from_utc_timestamp(from_unixtime(e.key.ts),"%s")),c.postalCode' % (timezone,timezone))
    #qb = qb.add_where('((unix_timestamp(from_utc_timestamp(from_unixtime(e.key.ts),"%s")) >= UNIX_TIMESTAMP("%s","yyyy-MM-dd HH:mm:ss") \
    #AND unix_timestamp(from_utc_timestamp(from_unixtime(e.key.ts),"%s")) <= UNIX_TIMESTAMP("%s","yyyy-MM-dd HH:mm:ss")) OR\
    #(unix_timestamp(from_utc_timestamp(from_unixtime(e.key.ts),"%s")) >= UNIX_TIMESTAMP("%s","yyyy-MM-dd HH:mm:ss") \
    #AND unix_timestamp(from_utc_timestamp(from_unixtime(e.key.ts),"%s")) <= UNIX_TIMESTAMP("%s","yyyy-MM-dd HH:mm:ss")))' % (timezone,ts_from,timezone,ts_to,timezone,ts_from_previous,timezone,ts_to_previous))
    #qb = qb.add_and_where('c.key.company = %s' % companyId)
    #qb = qb.add_groups(['c.key.contractId','month(from_utc_timestamp(from_unixtime(e.key.ts),"%s"))' %timezone,
    #                    'year(from_utc_timestamp(from_unixtime(e.key.ts),"%s"))' %timezone, 'c.postalCode' ])

    try:
        #pass
        context['report']['trace']['hive_query'] = qb.execute_query()
    except Exception, e:
        raise ModuleTaskFailed(context, msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    

    """
    Make a correction on the key ID, and creation of the results file
    """
    cmd = ['Rscript', context['config']['module']['R_scripts']['join_and_normalize.R']]
    r_params = {
                'companyId': companyId,
                'month': month,
                #'value_column':2,
                #'month_column':3,
                #'year_column':4,
                'value_column':3,
                'yearmonth_column':2,
                'number_days_column':4,
                'paths': context['config']['module']['paths']
                }
    try:
        context['report']['trace']['join_and_normalize'] = run_rscript(cmd, r_params)
    except Exception, e:
        raise ModuleTaskFailed(context,msg='%s' % e)
    
    context = update_current_task_progress(context)


    """
    Now we have results on HDFS so we can load into REST service
    """
    
    try:
        #pass
        context['report']['trace']['load_output_to_rest_job'] = load_output_to_rest_data(context, input=context['config']['module']['paths']['result_dir'], fields=context['config']['module']['public']['mongodb']['fields'],
                                                                                         ts_from_remove=get_month(ts_from), ts_to_remove=get_month(ts_to), company_to_remove=companyId)
    except Exception, e:
        raise ModuleTaskFailed(context,msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    
    try:
        #pass
        context['report']['trace']['load_output_to_hbase_job'] = load_output_to_hbase(context, input=context['config']['module']['paths']['result_dir'], table=context['config']['module']['hbase_table'])
    except Exception, e:
        raise ModuleTaskFailed(context,msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    
    try:
        #pass
        context['report']['trace']['clean_old_data_from_rest'] = cleanup_old_data_from_rest(context, month)
    except Exception, e:
        raise ModuleTaskFailed(context,msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    
    """ Delete the hive temp tables """
    try:
        pass
        for table in context['temp_hive_tables']['measures']:
            delete_measures_temp_table(context,table)
        for table in context['temp_hive_tables']['input']:
            delete_hive_module_input_table(context,table)
    except Exception, e:
        raise ModuleTaskFailed(context,msg='Error deleting the Hive Temporary Tables: %s' % e)
    
    context = update_current_task_progress(context)
    
    
    try:
        pass
        context['report']['trace']['cleanup_temp_data'] = cleanup_temp_data(context, context['config']['module']['paths']['all'], recurse=True)
    except Exception, e:
        raise ModuleTaskFailed(context,msg='%s' % e)
    
    context = update_current_task_progress(context)
    
    
    context['current_task'].update_state(state='SUCCESS', meta=True)
    
    context['report'].save()
    
    logger.info('Module OT201 execution finished...')
    
    return True
"""
if __name__ == '__main__':
    
    from ... import module_ot201
    module_ot201.async(params)*
    
"""
"""
params = {
        'companyId': 1111111111,
        'ts_to': datetime(2014,9,30,23,59),
        'num_months':24,
        'type': 'electricityConsumption',
        'timezone':"Europe/Madrid",
        'setupId': ObjectId()
        }
"""
