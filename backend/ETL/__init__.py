from celery import Task
from os import path, getenv
from json import load as JSONload
from json import dump as JSONdump
from copy import deepcopy
from datetime import datetime
import sys

import logging
logger = logging.getLogger('etl')

# MongoDB libs
from pymongo import MongoClient

# HDFS libs
from snakebite.client import Client as snakeBiteClient

# HBase libs
import happybase

# HIVE imports
import pyhs2


class ETLTask(Task):
    abstract = True
    _mongo = None
    _mongoApp = None
    _hdfs = None
    _hbase = None
    _config = None
    _hive = None

 

    def get_config(self, etl=None):
        config = {}
        paths_to_join = {}
    
        # Create a dict with all config paths to load
        paths_to_join['app'] = path.join(path.dirname(__file__), '../ETL/config.json')
    
        if etl:
            paths_to_join['etl'] = path.join(path.dirname(__file__), '../ETL/%s/config.json' % etl)
    
        logger.info('Creating configuration dictionary for ETL %s ' % etl)
        # Build a result config with all config files
        for key,fn in paths_to_join.items():
            config_file = open(fn)
            try:
                # Load json and aggregate to final result
                c = JSONload(config_file)
                config[key] = c
            except ValueError,e:
                logger.error('Can\'t load configuration file "%s": %s ' % (fn,e))
                raise Exception('Can\'t load configuration file "%s": %s ' % (fn,e))

    
        logger.info('Configuration dictionary created successfully for etl %s ' % etl)
        logger.debug('Configuration dictionary for module %s: %s' % (etl, config))
    
        return config

    @property
    def config(self):
        if self._config is None:
            logger.debug('Setup configuration')
            self._config = self.get_config(self.etl_name)
        return self._config

    @property
    def mongo(self):
        if self._mongo is None:
            self._mongo = MongoClient(self.config['app']['mongodb']['host'], self.config['app']['mongodb']['port'])
            
            self._mongo[self.config['app']['mongodb']['db']].authenticate(
                self.config['app']['mongodb']['username'],
                self.config['app']['mongodb']['password']
                )

        return self._mongo

    @property
    def hive(self):
        if self._hive is None:
            self._hive = pyhs2.connect(host=self.config['app']['hive']['host'], port=self.config['app']['hive']['port'], authMechanism='PLAIN', user="hive", password="")

        return self._hive.cursor()

    @property
    def hdfs(self):
        if self._hdfs is None:
            self._hdfs = snakeBiteClient(self.config['app']['hdfs']['host'], int(self.config['app']['hdfs']['port']))
        return self._hdfs

    @property
    def hbase(self):
        if self._hbase is None:
            self._hbase = happybase.Connection(self.config['app']['hbase']['host'], self.config['app']['hbase']['port'])
            self._hbase.open()
        return self._hbase

    def _close_connections(self):
        logger.debug('Closing HIVE, HBASE and MONGO client connections...')
        if self._hbase:
            self._hbase.close()
            self._hbase = None
        if self._hive:
            self._hive.close()
            self._hive = None
        if self._mongo:
            self._mongo.close()
            self._mongo = None
        if self._mongoApp:
            self._mongoApp.close()
            self._mongoApp = None
        logger.debug('Connections closed')

    def after_return(self, status, retval, task_id, args, kwargs, einfo):
        logger.debug('Task exiting. Calling _close_connections function')
        self._close_connections()


class ETLTaskFailed(Exception):
    def __init__(self, context, msg):

        Exception.__init__(self, msg)

        if 'report' in context:
            self.report = context['report']
            self.report['error_raised_at'] = datetime.now()
            self.report['error_msg'] = msg
            self.msg = msg
            # If we try to save traceback information it raises a PicklingError. Just save first field
            context['current_task'].update_state(state='FAILURE', meta={'result': msg, 'traceback': sys.exc_info()[0]})    
    
            self.report.save()
