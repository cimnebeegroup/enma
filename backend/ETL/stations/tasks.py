from lib.logs import setup_log
from celery.utils.log import get_task_logger
from celery.signals import after_setup_task_logger, after_setup_logger

after_setup_logger.connect(setup_log)
after_setup_task_logger.connect(setup_log)
logger = get_task_logger('etl.stations')

from ETL import ETLTask, ETLTaskFailed
from lib.utils import Report

#celery imports
from celery_backend import app
from celery import current_task

from datetime import datetime


class ETLStationsTask(ETLTask):
    abstract = True
    
    def __init__(self):
        logger.debug('ETLStationsTask init')
        self.etl_name = 'stations'
        #self.init_readings_cache()
        ETLTask.__init__(self)
    
    """
    def init_readings_cache(self):
        logger.debug('Initialize Readings cache to join information with measures')
        self.cache = {}
        for reading in self.mongo[self.config['app']['mongodb']['db']]['readings'].find():
            # save entire document (without _id field) to embed when needed
            self.cache[reading.pop('_id')] = reading
    """
    def LatLngToDMS(self, value, type):
                lat_0 = int(value) #Parte entera con o sin signo
                lat_decimal_0 = abs(value) - abs(lat_0) #Parte decimal
                lat_1 = int(60 * lat_decimal_0)
                lat_decimal_1 = (60 * lat_decimal_0) - lat_1 #Parte decimal
                lat_2 = int(round(60 * lat_decimal_1))
                if type == 'Lat':
                    text = 'N' if lat_0 > 0 else 'S'
                else:
                    text = 'E' if lat_0 > 0 else 'W'        
                return "{:0>2d} ".format(lat_0) + "{:0>2d} ".format(lat_1) + "{:0>2d} ".format(lat_2) + text
    
    def save_station_into_hbase(self, station):
        row = {}
        #prepare documents to iterate them
        try:
            altitude = station.pop('altitude')
            address = station.pop('address')
            GPS = address.pop('GPS')
            companyId = station.pop('companyId')
            dateStart = station.pop('dateStart').strftime('%s') if 'dateStart' in station and isinstance(station['dateStart'],datetime) else None
            dateEnd = station.pop('dateEnd').strftime('%s') if 'dateEnd' in station and isinstance(station['dateEnd'],datetime) else None
            
            row_key = str(companyId)+'~'+station.pop('stationId')
            
            #dataStart
            if dateStart:
                row['address:dateStart'] = str(dateStart.encode('utf-8'))
            #dataEnd
            if dateEnd:
                row['address:dateEnd'] = str(dateEnd.encode('utf-8'))
            #altitude
            row['address:altitude'] = str(altitude.encode('utf-8'))
            #other address
            for key, value in address.iteritems():
                row['address:%s'%key] = str(value.encode('utf-8'))
            #GPS
            # Doesn't matter if the latitude is expressed in lat/latitude or the longitude is expressed in long/longitude
            # Also transform the float latitude and longitude values into the correct string format.
            for key, value in GPS.iteritems():
                
                if key=='latitude' or key=='lat': 
                    try:
                        float(value)
                        row['address:lat'] = str(self.LatLngToDMS(value,'Lat').encode('utf-8'))
                    except ValueError:
                        row['address:lat'] = str(value.encode('utf-8'))
                        
                elif key=='longitude' or key=='long':
                    try:
                        float(value)
                        row['address:long'] = str(self.LatLngToDMS(value,'Long').encode('utf-8'))
                    except ValueError:
                        row['address:long'] = str(value.encode('utf-8'))
                    
            self.hbase_table.put(row_key, row)
            
        except KeyError,e:
            logger.debug('Station with missing fields: %s. Not saved into HBase' % e)


@app.task(name='ETL_stations', base=ETLStationsTask)
def ETL_stations(params):
    
    
    logger.info('Starting Stations ETL')
    
    logger.debug('Builing task context with config, clients and report')
    context = {}
    context['current_task'] = current_task
    context['config'] = ETL_stations.config
    
    try:
        context['clients'] = { 'mongo': ETL_stations.mongo }
    except Exception, e:
        raise ModuleTaskFailed(context, 'Error connecting to needed connection clients: %s' % e)
    
    
    context['report'] = Report(monogodb_fd=context['clients']['mongo'], db=context['config']['app']['report']['db'], collection=context['config']['app']['report']['collection'])
    context['report']['task_id'] = current_task.request.id
    context['report']['started_at'] = datetime.now()
    context['report']['params'] = [params]
    context['report']['module'] = ETL_stations.etl_name
    
    context['report'].update()
    
    logger.debug('Parameters provided to ETL_stations function: %s' % params)
    
    # set query dictionary
    if 'last_etl' in params:
        query = {
                 '_updated': {
                               '$gte': params['last_etl']
                               }
                 }
    else:
        query = {}
        
    # setting variables for readability
    db = context['config']['app']['mongodb']['db']
    collection = context['config']['etl']['mongodb']['collection']
    
    
    logger.debug('Querying for measures in MongoDB: %s' % query)
    context['report']['etl'] = {}
    context['report']['etl'] = {
                                'started_at': datetime.now(),
                                'query': str(query),
                                'state': 'started',
                                'extracted': 0,
                                'loaded': 0
                                }
    
    projection = {
                  '_id':0,
                  '_created':0,
                  '_updated':0
                  }
    
    table = context['config']['etl']['hbase_table']['name']
    ETL_stations.hbase_table = ETL_stations.hbase.table(table)
    
    for station in context['clients']['mongo'][db][collection].find(query, projection):
        context['report']['etl']['extracted'] += 1
        ETL_stations.save_station_into_hbase(station)
        context['report']['etl']['loaded'] += 1
    
    context['report']['etl']['state'] = 'finished'
    
    context['report'].save()
    
    if not current_task.request.id:
        logger.debug('Closing the connections opened by ETL_stations function')
        ETL_stations._close_connections()
    
    return True



"""
params = {
          'last_etl': datetime(2014,1,1,0,0,0)
         }
"""