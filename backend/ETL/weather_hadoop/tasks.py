from lib.logs import setup_log
from celery.utils.log import get_task_logger
from celery.signals import after_setup_task_logger, after_setup_logger

from tempfile import NamedTemporaryFile

after_setup_logger.connect(setup_log)
after_setup_task_logger.connect(setup_log)
logger = get_task_logger('etl.weather_hadoop')

from hadoop_job import Hadoop_ETL

from ETL import ETLTask, ETLTaskFailed
from lib.etl import transformations
from lib.utils import Report

import lib

#celery imports
from celery_backend import app
from celery import current_task

from json import dumps as JSONdumps
from datetime import datetime
from os import path
import types

BUFFER_SIZE = 1000000

class ETLMongoHBaseHadoopTask(ETLTask):
    abstract = True
    
    def __init__(self):
        logger.debug('ETLMongoHBaseHadoopTask init')
        self.etl_name = 'weather_hadoop'
        #self.init_readings_cache()
        ETLTask.__init__(self)
    
    """
    def init_readings_cache(self):
        logger.debug('Initialize Readings cache to join information with measures')
        self.cache = {}
        for reading in self.mongo[self.config['app']['mongodb']['db']]['readings'].find():
            # save entire document (without _id field) to embed when needed
            self.cache[reading.pop('_id')] = reading
    """
    
    def hadoop_job(self, input):
        """Runs the Hadoop job uploading task configuration"""
        # create report to save on completion or error
        report = {
                  'started_at': datetime.now(),
                  'state': 'launched',
                  'input': input
                  }
        
        # Create temporary file to upload with json extension to identify it in HDFS
        f = NamedTemporaryFile(delete=False, suffix='.json')
        f.write(JSONdumps(self.config))
        f.close()
        report['config_temp_file'] = f.name
        logger.debug('Created temporary config file to upload into hadoop and read from job: %s' % f.name)
        
        # create hadoop job instance adding file location to be uploaded 
        #mr_job = Hadoop_ETL(args=['-r', 'hadoop', input, '--file', f.name, '--output-dir', '/tmp/proves_xavi', '--python-archive', path.dirname(lib.__file__)])
        mr_job = Hadoop_ETL(args=['-r', 'hadoop', input, '--file', f.name, '--python-archive', path.dirname(lib.__file__)])
        with mr_job.make_runner() as runner:
            try:
                runner.run()
            except Exception,e:
                f.unlink(f.name)
                raise Exception('Error running MRJob ETL process using hadoop: %s' % e)
            
        f.unlink(f.name)
        logger.debug('Temporary config file uploaded has been deleted from FileSystem')
        
        
        report['finished_at'] = datetime.now()
        report['state'] = 'finished'
        
        return report
    


@app.task(name='ETL_weather_hadoop', base=ETLMongoHBaseHadoopTask)
def ETL_weather_hadoop(params):
    
    
    logger.info('Starting MongoDB-HBase ETL using Hadoop ...')
    
    logger.debug('Builing task context with config, clients and report')
    context = {}
    context['current_task'] = current_task
    context['config'] = ETL_weather_hadoop.config
    
    try:
        context['clients'] = { 'mongo': ETL_weather_hadoop.mongo }
    except Exception, e:
        raise ModuleTaskFailed(context, 'Error connecting to needed connection clients: %s' % e)
    
    
    context['report'] = Report(monogodb_fd=context['clients']['mongo'], db=context['config']['app']['report']['db'], collection=context['config']['app']['report']['collection'])
    context['report']['task_id'] = current_task.request.id
    context['report']['started_at'] = datetime.now()
    context['report']['params'] = [params]
    context['report']['module'] = ETL_weather_hadoop.etl_name
    
    context['report'].update()
    
    try:
        ts_to = params['ts_to'] if 'ts_to' in params else 0
        ts_from = params['ts_from'] if 'ts_from' in params else 0
        specific_stations = params['specific_stations'] if 'specific_stations' in params else None
    except KeyError, e:
        error_msg = 'Not enough parameters provided to ETL_weather_hadoop: %s' % (e)
        logger.error(error_msg)
        raise ModuleTaskFailed(context, msg=error_msg)
    
    
    logger.debug('Parameters provided to ETL_Hadoop function: %s' % params)
    
    # set query dictionary
    # defined timestamps, non defined stations
    if ts_from!=0 and ts_to!=0 and specific_stations is None:
        query = {
                 'timestamp': {
                               '$gte': ts_from,
                               '$lt': ts_to
                               }
                 }
    # defined timestamps, defined stations
    elif ts_from!=0 and ts_to!=0 and specific_stations is not None:
        query = {
                 'timestamp': {
                               '$gte': ts_from,
                               '$lt': ts_to
                               },
                 'stationId': {
                               '$in': specific_stations
                               }
                 }
    # non defined timestamps, non defined stations
    elif specific_stations is not None:
        query = {
                 'stationId': {
                               '$in': specific_stations
                               }
                 }
    # non defined timestamps, defined stations
    else:
        query = {}
                 
    # add the company if it's present
    if 'companyId' in params:
        companyId = params['companyId'] if isinstance(params['companyId'],types.ListType) else [ params['companyId'] ]
        query.update({'companyId':{'$in':companyId}})
        
    # set projection dictionary (1 means field returned, 0 field wont be returned)
    projection = {
                  'reading': 1,
                  'stationId': 1,
                  'timestamp': 1,
                  'value': 1,
                  'type': 1,
                  '_id': 0
                  }
    
    # setting variables for readability
    measures_collection = context['config']['etl']['mongodb']['collection']
    db = context['config']['app']['mongodb']['db']
    
    logger.debug('Querying for measures in MongoDB: %s' % query)
    cursor = context['clients']['mongo'][db][measures_collection].find(query, projection, timeout=False)
    
    # get buffer size from parameters. Default if not provided
    n = params.get('buffer_size')
    if not n:
        n = BUFFER_SIZE
    context['report']['buffer_size'] = n
    
    
    context['report']['hadoop_jobs'] = []
    logger.debug('Creating buffer to execute hadoop job. Buffer size: %s' % n)
    buffer = transformations.create_buffer(cursor, n)
    while buffer:
        # copy buffer to hdfs
        #file = transformations.create_csv_from_buffer(buffer, context['config']['etl']['row_definition'])
        #df = DataFrame(buffer, columns=['timestamp', 'deviceId', 'value'])
        #logger.debug('DataFrame: %s' % df)
        file = transformations.create_pickle_file_from_buffer(buffer)
        try:
            # launch mapreduce
            ## N transformations on each record
            ## Loading into HBase
            job_report = ETL_weather_hadoop.hadoop_job(file.name)
            context['report']['hadoop_jobs'].append({'job_report':job_report, 'buffer_size': len(buffer)})
        except Exception, e:
            raise ETLTaskFailed(context, 'MRJob process on MongoDB-HBase ETL job has failed: %s' % e)
        
        logger.info('A Hadoop job performing ETL process has finished. Loaded %s measures' % len(buffer))
        
        # remove temporary file
        logger.debug('Removing temporary local CSV file: %s' % file.name)
        file.unlink(file.name)
        
        # create new buffer for next loop
        buffer = transformations.create_buffer(cursor, n)
    
    """Delete the results of REST that had been loaded in Hbase"""
    try:
        logger.info('Deleting the measures from REST that has been loaded to Hbase')
        transformations.remove_measures(context, db, measures_collection, query)
        logger.info('The measures from REST that has been loaded to Hbase are deleted')
    except Exception, e:
        raise ETLTaskFailed(context, 'Deleting process of the loaded data in REST has failed: %s' % e)
    
    
    
    context['report'].save()
    
    if not current_task.request.id:
        logger.debug('Closing the connections opened by ETL_weather_hadoop function')
        ETL_weather_hadoop._close_connections()
    
    return True

"""
params = {
         'specific_stations': None, #If you don't want any specific stationId, put None. Exemple: ['B236C','B662X']
         'buffer_size':1000000
         }
"""