from lib.logs import setup_log
from celery.utils.log import get_task_logger
from celery.signals import after_setup_task_logger, after_setup_logger

after_setup_logger.connect(setup_log)
after_setup_task_logger.connect(setup_log)
logger = get_task_logger('etl.cumulative_instant')

from hadoop_job import Cumulative_Instant_ETL
from hadoop_job_tertiary import Cumulative_Instant_Tertiary_ETL
from hadoop_job_tou import Cumulative_Instant_Tou_ETL
from lib.etl.mapreduce.MR_delete_keys_from_hbase import MRJob_delete_keys

from ETL import ETLTask, ETLTaskFailed
from lib.utils import *
from lib.querybuilder import QueryBuilder

#celery imports
from celery_backend import app
from celery import current_task

from datetime import datetime
from os import path
import types
import lib


class ETLCumulativeInstantTask(ETLTask):
    abstract = True
    
    def __init__(self):
        logger.debug('ETLCumulativeInstantTask init')
        self.etl_name = 'cumulative_instant'
        ETLTask.__init__(self)
    
    
    def hadoop_job_delete(self, input, columns_to_delete):
        """Runs the Hadoop job uploading task configuration"""
        # create report to save on completion or error
        report = {
                  'started_at': datetime.now(),
                  'state': 'launched',
                  'input': input
                  }
        
        # Create temporary file to upload with json extension to identify it in HDFS
        job_extra_config = self.config.copy()
        job_extra_config.update({
                                'columns_to_delete': columns_to_delete
                                })
        f = NamedTemporaryFile(delete=False, suffix='.json')
        f.write(JSONdumps(job_extra_config))
        f.close()
        report['config_temp_file'] = f.name
        logger.debug('Created temporary config file to upload into hadoop and read from job: %s' % f.name)
        
        # create hadoop job instance adding file location to be uploaded 
        mr_job = MRJob_delete_keys(args=['-r', 'hadoop', 'hdfs://'+input, '--file', f.name, '--python-archive', path.dirname(lib.__file__)])
        with mr_job.make_runner() as runner:
            try:
                runner.run()
            except Exception,e:
                f.unlink(f.name)
                raise Exception('Error running MRJob KeysToDelete process using hadoop: %s' % e)
    
        f.unlink(f.name)
        logger.debug('Temporary config file uploaded has been deleted from FileSystem')
        
        
        report['finished_at'] = datetime.now()
        report['state'] = 'finished'
        
        return report
    
    
    def hadoop_job(self, input, measure_type, companyId):
        """Runs the Hadoop job uploading task configuration"""
        # create report to save on completion or error
        report = {
                  'started_at': datetime.now(),
                  'state': 'launched',
                  'input': input
                  }
        
        # Create temporary file to upload with json extension to identify it in HDFS
        job_extra_config = self.config.copy()
        job_extra_config.update({
                                'measure_type': measure_type,
                                'companyId': companyId
                                })
        f = NamedTemporaryFile(delete=False, suffix='.json')
        f.write(JSONdumps(job_extra_config))
        f.close()
        report['config_temp_file'] = f.name
        logger.debug('Created temporary config file to upload into hadoop and read from job: %s' % f.name)
        
        # create hadoop job instance adding file location to be uploaded 
        #mr_job = Cumulative_Instant_ETL(args=['-r', 'hadoop', 'hdfs://'+input, '--file', f.name, '--output-dir', '/tmp/proves_xavi', '--python-archive', path.dirname(lib.__file__)])
        mr_job = Cumulative_Instant_ETL(args=['-r', 'hadoop', 'hdfs://'+input, '--file', f.name, '--python-archive', path.dirname(lib.__file__)])
        with mr_job.make_runner() as runner:
            try:
                runner.run()
            except Exception,e:
                f.unlink(f.name)
                raise Exception('Error running MRJob ETL process using hadoop: %s' % e)
    
        f.unlink(f.name)
        logger.debug('Temporary config file uploaded has been deleted from FileSystem')
        
        
        report['finished_at'] = datetime.now()
        report['state'] = 'finished'
        
        return report
    
    def hadoop_job_tertiary(self, input, measure_type, companyId, maximum_number_of_periods):
        """Runs the Hadoop job uploading task configuration"""
        # create report to save on completion or error
        report = {
                  'started_at': datetime.now(),
                  'state': 'launched',
                  'input': input
                  }
        
        # Create temporary file to upload with json extension to identify it in HDFS
        job_extra_config = self.config.copy()
        job_extra_config.update({
                                'measure_type': measure_type,
                                'companyId': companyId,
                                'maximum_number_of_periods': maximum_number_of_periods
                                })
        f = NamedTemporaryFile(delete=False, suffix='.json')
        f.write(JSONdumps(job_extra_config))
        f.close()
        report['config_temp_file'] = f.name
        logger.debug('Created temporary config file to upload into hadoop and read from job: %s' % f.name)
        
        # create hadoop job instance adding file location to be uploaded 
        #mr_job = Cumulative_Instant_ETL(args=['-r', 'hadoop', 'hdfs://'+input, '--file', f.name, '--output-dir', '/tmp/proves_etl_cumulative_instant_daily', '--python-archive', path.dirname(lib.__file__)])
        mr_job = Cumulative_Instant_Tertiary_ETL(args=['-r', 'hadoop', 'hdfs://'+input, '--file', f.name, '--python-archive', path.dirname(lib.__file__)])
        with mr_job.make_runner() as runner:
            try:
                runner.run()
            except Exception,e:
                f.unlink(f.name)
                raise Exception('Error running MRJob ETL process using hadoop: %s' % e)
    
        f.unlink(f.name)
        logger.debug('Temporary config file uploaded has been deleted from FileSystem')
        
        
        report['finished_at'] = datetime.now()
        report['state'] = 'finished'
        
        return report
    
    def hadoop_job_tou(self, input, measure_type, companyId, maximum_number_of_periods):
        """Runs the Hadoop job uploading task configuration"""
        # create report to save on completion or error
        report = {
                  'started_at': datetime.now(),
                  'state': 'launched',
                  'input': input
                  }
        
        # Create temporary file to upload with json extension to identify it in HDFS
        job_extra_config = self.config.copy()
        job_extra_config.update({
                                'measure_type': measure_type,
                                'companyId': companyId,
                                'maximum_number_of_periods': maximum_number_of_periods
                                })
        f = NamedTemporaryFile(delete=False, suffix='.json')
        f.write(JSONdumps(job_extra_config))
        f.close()
        report['config_temp_file'] = f.name
        logger.debug('Created temporary config file to upload into hadoop and read from job: %s' % f.name)
        
        # create hadoop job instance adding file location to be uploaded 
        #mr_job = Cumulative_Instant_ETL(args=['-r', 'hadoop', 'hdfs://'+input, '--file', f.name, '--output-dir', '/tmp/proves_etl_cumulative_instant_daily', '--python-archive', path.dirname(lib.__file__)])
        mr_job = Cumulative_Instant_Tou_ETL(args=['-r', 'hadoop', 'hdfs://'+input, '--file', f.name, '--python-archive', path.dirname(lib.__file__)])
        with mr_job.make_runner() as runner:
            try:
                runner.run()
            except Exception,e:
                f.unlink(f.name)
                raise Exception('Error running MRJob ETL process using hadoop: %s' % e)
    
        f.unlink(f.name)
        logger.debug('Temporary config file uploaded has been deleted from FileSystem')
        
        
        report['finished_at'] = datetime.now()
        report['state'] = 'finished'
        
        return report

@app.task(name='ETL_cumulative_instant', base=ETLCumulativeInstantTask)
def ETL_cumulative_instant(params):
    
    
    logger.info('Starting Cumulative Instant ETL')
    
    logger.debug('Builing task context with config, clients and report')
    context = {}
    context['current_task'] = current_task
    context['config'] = ETL_cumulative_instant.config
    
    """task_UUID for paths in HDFS and queries"""
    task_UUID = id_generator()
    context['config']['etl']['paths'] = random_paths(context['config']['etl']['paths'],'UUID', task_UUID)
    
    try:
        context['clients'] = { 
                              'mongo': ETL_cumulative_instant.mongo,
                              'hive': ETL_cumulative_instant.hive,
                              'hdfs': ETL_cumulative_instant.hdfs
                              }
    except Exception, e:
        raise ETLTaskFailed(context, 'Error connecting to needed connection clients: %s' % e)
    
    
    
    context['report'] = Report(monogodb_fd=context['clients']['mongo'], db=context['config']['app']['report']['db'], collection=context['config']['app']['report']['collection'])
    context['report']['task_id'] = current_task.request.id
    context['report']['started_at'] = datetime.now()
    context['report']['params'] = [params]
    context['report']['module'] = ETL_cumulative_instant.etl_name
    context['report']['trace'] = {}
    
    context['report'].update()
    
    
    try:
        ts_from = params['ts_from'] if 'ts_from' in params else None
        ts_to = params['ts_to'] if 'ts_to' in params else None
        companyId = params['companyId']
        timezone = params['timezone'] if 'timezone' in params else 'Europe/Madrid'
        measure_types = params['type'] if isinstance(params['type'],types.ListType) else [params['type']]
        maximum_number_of_periods = params['maximum_number_of_periods_of_the_measures'] if 'maximum_number_of_periods_of_the_measures' in params else context['config']['etl']['maximum_number_of_periods_default']
        
    except Exception, e:
        raise ETLTaskFailed(context, 'Error with provided parameters: %s' % e)
    
    for measure_type in measure_types:
        
        tertiary_type = True if measure_type[0:8] == 'tertiary' else False
        
        residential_tou_type = True if measure_type[0:3] == 'tou' else False
        
        # TERTIARY MEASURES TYPE
        if tertiary_type:
            
            table_from = create_measures_temp_table(context, measure_type, companyId, task_UUID, tertiary=True, number_period_tertiary=maximum_number_of_periods)
            fields_input = [('b','tinyint'),('contractId','string'),('ts', 'bigint')]
            
            # Generate two lists with the instant and the accumulated values for each period
            measures_periods_instant = []
            measures_periods_accumulated = []
            for i in xrange(1,maximum_number_of_periods+1):
                fields_input.extend( [ ( 'p%s' % i, 'float' ) ] )
                measures_periods_instant.extend( [ 'p%s' % i ] )
            for i in xrange(1,maximum_number_of_periods+1):
                fields_input.extend( [ ( 'p%sa' % i, 'float' ) ] )
                measures_periods_accumulated.extend( [ 'p%sa' % i ] )
                
            fields_input.extend( [ ('value','float'), ('calculated','int') ] )
            table_input = create_hive_module_input_table(context, 'ETL_cumulative_instant_Raw_%s' % measure_type, context['config']['etl']['paths']['input'], fields_input, task_UUID)
            
            #Query to obtain all the measures of the contracts
            qb = QueryBuilder(context['clients']['hive'])
            qb = qb.add_from(table_from, 'e').add_insert(table=table_input)
            qb = qb.add_select('e.key.b, e.key.contractId, e.key.ts, %s, %s, e.value, e.calculated' % ( ",".join(measures_periods_instant), ",".join(measures_periods_accumulated) ) )
            
            if ts_from and ts_to:
                qb = qb.add_where('unix_timestamp(from_utc_timestamp(from_unixtime(e.key.ts),"%s")) >= UNIX_TIMESTAMP("%s","yyyy-MM-dd HH:mm:ss")' % (timezone,ts_from))
                qb = qb.add_and_where('unix_timestamp(from_utc_timestamp(from_unixtime(e.key.ts),"%s")) <= UNIX_TIMESTAMP("%s","yyyy-MM-dd HH:mm:ss")' % (timezone,ts_to))
            
            try:
                context['report']['trace']['hive_query_RAW_%s' % measure_type] = qb.execute_query()
            except Exception, e:
                raise ETLTaskFailed(context, msg='%s' % e)
            
            #Query to obtain only the calculated measures of the contracts
            qb = QueryBuilder(context['clients']['hive'])
            table_input2 = create_hive_module_input_table(context, 'ETL_cumulative_instant_KeysToDelete_%s' % measure_type, context['config']['etl']['paths']['input']+'_to_delete', [('key','string'),('hbase_table','string')], task_UUID)
            qb = qb.add_from(table_input, 'e').add_insert(table=table_input2)
            qb = qb.add_select( 'concat_ws( "~",cast(e.b as string), cast(e.ts as string), e.contractId), "%s_%s"' % (measure_type, companyId) )
            qb = qb.add_where('(e.value is not null and e.calculated = 1)')
            
            try:
                context['report']['trace']['hive_query_ToDelete_%s' % measure_type] = qb.execute_query()
            except Exception, e:
                raise ETLTaskFailed(context, msg='%s' % e)
            
            # Execute the MR job to delete the old calculated measures for each contract
            try:
                columns_to_delete = ['m:v','m:calc']
                for period in xrange(1,maximum_number_of_periods+1):
                    columns_to_delete.append('m:p%s' % period)
                context['report']['hadoop_job_delete_%s' % measure_type] = ETL_cumulative_instant.hadoop_job_delete(context['config']['etl']['paths']['input']+'_to_delete', columns_to_delete)
            except Exception, e:
                raise ETLTaskFailed(context, 'MRJob process on deleting the old calculated measures for each contract has failed: %s' % e)            
            
            #Query to obtain only the real measures of the contracts
            qb = QueryBuilder(context['clients']['hive'])
            table_input3 = create_hive_module_input_table(context, 'ETL_cumulative_instant_ToTreat_%s' % measure_type, context['config']['etl']['paths']['input']+'_to_treat', fields_input, task_UUID)
            qb = qb.add_from(table_input, 'e').add_insert(table=table_input3)
            qb = qb.add_select('e.b, e.contractId, e.ts, %s, %s, e.value, e.calculated' % ( ",".join(measures_periods_instant), ",".join(measures_periods_accumulated) ) )
            qb = qb.add_where('(%s) OR (e.value is not null and (e.calculated is null or e.calculated = 0) )' % " OR ".join( ["e.%s is not null" % item for item in measures_periods_accumulated] ) )
            
            try:
                context['report']['trace']['hive_query_ToTreat_%s' % measure_type] = qb.execute_query()
            except Exception, e:
                raise ETLTaskFailed(context, msg='%s' % e)
            
            
            # Execute the MR job to pass the cumulative measures to instant
            try:
                context['report']['hadoop_job_%s' % measure_type] = ETL_cumulative_instant.hadoop_job_tertiary(context['config']['etl']['paths']['input']+'_to_treat', measure_type, companyId, maximum_number_of_periods)
            except Exception, e:
                raise ETLTaskFailed(context, 'MRJob process on Cumulative Instant ETL job has failed: %s' % e)
        
        # TERTIARY MEASURES TYPE
        elif residential_tou_type:
            
            table_from = create_measures_temp_table(context, measure_type, companyId, task_UUID, tertiary=True, number_period_tertiary=maximum_number_of_periods)
            fields_input = [('b','tinyint'),('contractId','string'),('ts', 'bigint')]
            
            # Generate two lists with the instant and the accumulated values for each period
            measures_periods_instant = []
            measures_periods_accumulated = []
            for i in xrange(1,maximum_number_of_periods+1):
                fields_input.extend( [ ( 'p%s' % i, 'float' ) ] )
                measures_periods_instant.extend( [ 'p%s' % i ] )
            for i in xrange(1,maximum_number_of_periods+1):
                fields_input.extend( [ ( 'p%sa' % i, 'float' ) ] )
                measures_periods_accumulated.extend( [ 'p%sa' % i ] )
                
            fields_input.extend( [ ('value','float'), ('calculated','int') ] )
            table_input = create_hive_module_input_table(context, 'ETL_cumulative_instant_Raw_%s' % measure_type, context['config']['etl']['paths']['input'], fields_input, task_UUID)
            
            #Query to obtain all the measures of the contracts
            qb = QueryBuilder(context['clients']['hive'])
            qb = qb.add_from(table_from, 'e').add_insert(table=table_input)
            qb = qb.add_select('e.key.b, e.key.contractId, e.key.ts, %s, %s, e.value, e.calculated' % ( ",".join(measures_periods_instant), ",".join(measures_periods_accumulated) ) )
            
            if ts_from and ts_to:
                qb = qb.add_where('unix_timestamp(from_utc_timestamp(from_unixtime(e.key.ts),"%s")) >= UNIX_TIMESTAMP("%s","yyyy-MM-dd HH:mm:ss")' % (timezone,ts_from))
                qb = qb.add_and_where('unix_timestamp(from_utc_timestamp(from_unixtime(e.key.ts),"%s")) <= UNIX_TIMESTAMP("%s","yyyy-MM-dd HH:mm:ss")' % (timezone,ts_to))
            
            try:
                context['report']['trace']['hive_query_RAW_%s' % measure_type] = qb.execute_query()
            except Exception, e:
                raise ETLTaskFailed(context, msg='%s' % e)
            
            #Query to obtain only the calculated measures of the contracts
            qb = QueryBuilder(context['clients']['hive'])
            table_input2 = create_hive_module_input_table(context, 'ETL_cumulative_instant_KeysToDelete_%s' % measure_type, context['config']['etl']['paths']['input']+'_to_delete', [('key','string'),('hbase_table','string')], task_UUID)
            qb = qb.add_from(table_input, 'e').add_insert(table=table_input2)
            qb = qb.add_select( 'concat_ws( "~",cast(e.b as string), cast(e.ts as string), e.contractId), "%s_%s"' % (measure_type, companyId) )
            qb = qb.add_where('(e.value is not null and e.calculated = 1)')
            
            try:
                context['report']['trace']['hive_query_ToDelete_%s' % measure_type] = qb.execute_query()
            except Exception, e:
                raise ETLTaskFailed(context, msg='%s' % e)
            
            # Execute the MR job to delete the old calculated measures for each contract
            try:
                columns_to_delete = ['m:v','m:calc']
                for period in xrange(1,maximum_number_of_periods+1):
                    columns_to_delete.append('m:p%s' % period)
                context['report']['hadoop_job_delete_%s' % measure_type] = ETL_cumulative_instant.hadoop_job_delete(context['config']['etl']['paths']['input']+'_to_delete', columns_to_delete)
            except Exception, e:
                raise ETLTaskFailed(context, 'MRJob process on deleting the old calculated measures for each contract has failed: %s' % e)            
            
            #Query to obtain only the real measures of the contracts
            qb = QueryBuilder(context['clients']['hive'])
            table_input3 = create_hive_module_input_table(context, 'ETL_cumulative_instant_ToTreat_%s' % measure_type, context['config']['etl']['paths']['input']+'_to_treat', fields_input, task_UUID)
            qb = qb.add_from(table_input, 'e').add_insert(table=table_input3)
            qb = qb.add_select('e.b, e.contractId, e.ts, %s, %s, e.value, e.calculated' % ( ",".join(measures_periods_instant), ",".join(measures_periods_accumulated) ) )
            qb = qb.add_where('(%s) OR (e.value is not null and (e.calculated is null or e.calculated = 0) )' % " OR ".join( ["e.%s is not null" % item for item in measures_periods_accumulated] ) )
            
            try:
                context['report']['trace']['hive_query_ToTreat_%s' % measure_type] = qb.execute_query()
            except Exception, e:
                raise ETLTaskFailed(context, msg='%s' % e)
            
            
            # Execute the MR job to pass the cumulative measures to instant
            try:
                context['report']['hadoop_job_%s' % measure_type] = ETL_cumulative_instant.hadoop_job_tou(context['config']['etl']['paths']['input']+'_to_treat', measure_type, companyId, maximum_number_of_periods)
            except Exception, e:
                raise ETLTaskFailed(context, 'MRJob process on Cumulative Instant ETL job has failed: %s' % e)
        
        # RESIDENTIAL MEASURES TYPE
        else:
            #Query to obtain all the measures of the contracts
            table_from = create_measures_temp_table(context, measure_type, companyId, task_UUID, cumulative = True)
            fields_input = [('b','tinyint'), ('contractid', 'string'), ('ts', 'bigint'), ('value','float'), ('accumulated','float'),('calculated','int')]
            table_input = create_hive_module_input_table(context, 'ETL_cumulative_instant_Raw_%s' % measure_type, context['config']['etl']['paths']['input'], fields_input, task_UUID)
            
            qb = QueryBuilder(context['clients']['hive'])
            qb = qb.add_from(table_from, 'e').add_insert(table=table_input)
            qb = qb.add_select('e.key.b, e.key.contractid, e.key.ts, e.value, e.accumulated, e.calculated')
            if ts_from and ts_to:
                qb = qb.add_where('unix_timestamp(from_utc_timestamp(from_unixtime(e.key.ts),"%s")) >= UNIX_TIMESTAMP("%s","yyyy-MM-dd HH:mm:ss")' % (timezone,ts_from))
                qb = qb.add_and_where('unix_timestamp(from_utc_timestamp(from_unixtime(e.key.ts),"%s")) <= UNIX_TIMESTAMP("%s","yyyy-MM-dd HH:mm:ss")' % (timezone,ts_to))
            
            try:
                context['report']['trace']['hive_query_Raw_%s' % measure_type] = qb.execute_query()
            except Exception, e:
                raise ETLTaskFailed(context, msg='%s' % e)
            
            #Query to obtain only the calculated measures of the contracts
            qb = QueryBuilder(context['clients']['hive'])
            table_input2 = create_hive_module_input_table(context, 'ETL_cumulative_instant_KeysToDelete_%s' % measure_type, context['config']['etl']['paths']['input']+'_to_delete', [('key','string'),('hbase_table','string')], task_UUID)
            qb = qb.add_from(table_input, 'e').add_insert(table=table_input2)
            qb = qb.add_select( 'concat_ws( "~",cast(e.b as string), cast(e.ts as string), e.contractId), "%s_%s"' % (measure_type, companyId) )
            qb = qb.add_where('(e.value is not null and e.calculated = 1)')
            
            try:
                context['report']['trace']['hive_query_ToDelete_%s' % measure_type] = qb.execute_query()
            except Exception, e:
                raise ETLTaskFailed(context, msg='%s' % e)
            
            # Execute the MR job to delete the old calculated measures for each contract
            try:
                columns_to_delete = ['m:v','m:calc']
                context['report']['hadoop_job_delete_%s' % measure_type] = ETL_cumulative_instant.hadoop_job_delete(context['config']['etl']['paths']['input']+'_to_delete', columns_to_delete)
            except Exception, e:
                raise ETLTaskFailed(context, 'MRJob process on deleting the old calculated measures for each contract has failed: %s' % e)            
            
            #Query to obtain only the real measures of the contracts
            qb = QueryBuilder(context['clients']['hive'])
            table_input3 = create_hive_module_input_table(context, 'ETL_cumulative_instant_ToTreat_%s' % measure_type, context['config']['etl']['paths']['input']+'_to_treat', fields_input, task_UUID)
            qb = qb.add_from(table_input, 'e').add_insert(table=table_input3)
            qb = qb.add_select('e.b, e.contractId, e.ts, e.value, e.accumulated, e.calculated')
            qb = qb.add_where('(e.accumulated is not null or (e.value is not null and (e.calculated is null or e.calculated = 0)))')
            try:
                context['report']['trace']['hive_query_ToTreat_%s' % measure_type] = qb.execute_query()
            except Exception, e:
                raise ETLTaskFailed(context, msg='%s' % e)
            
            
            # Execute the MR job to pass the cumulative measures to instant
            try:
                context['report']['hadoop_job_%s' % measure_type] = ETL_cumulative_instant.hadoop_job(context['config']['etl']['paths']['input']+'_to_treat', measure_type, companyId)
            except Exception, e:
                raise ETLTaskFailed(context, 'MRJob process on Cumulative Instant ETL job has failed: %s' % e)
            
        # Delete the temporary HIVE tables
        try:
            pass
            for table in context['temp_hive_tables']['measures']:
                pass
                delete_measures_temp_table(context,table)
            for table in context['temp_hive_tables']['input']:
                pass
                delete_hive_module_input_table(context,table)
        except Exception, e:
            raise ModuleTaskFailed(context,msg='%s' % e)
        
        
        # Delete HDFS temporary files
        try:
            pass
            context['report']['trace']['cleanup_temp_data_%s' % measure_type] = cleanup_temp_data(context, context['config']['etl']['paths']['all'], recurse=True)
        except Exception, e:
            raise ETLTaskFailed(context,msg='%s' % e)
    
    context['report'].save()
    
    if not current_task.request.id:
        logger.debug('Closing the connections opened by ETL_cumulative_instant function')
        ETL_cumulative_instant._close_connections()
    
    return True
"""
params={
        'type':['electricityConsumption','heatConsumption_HW','heatConsumption_SH','waterConsumption'],
        'companyId': 1111111111,
        'timezone':'Europe/Madrid'
        }
"""