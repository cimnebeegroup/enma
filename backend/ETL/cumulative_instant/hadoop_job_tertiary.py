from mrjob.job import MRJob
from mrjob.protocol import PickleValueProtocol

# hbase and mongo clients libs
import happybase
from pymongo import MongoClient
from bson.objectid import ObjectId

# Generic imports
import glob
from json import load

# Imports from ETL and utils functions
from lib.etl import transformations
from lib.utils import value_in_a_dict

class Cumulative_Instant_Tertiary_ETL(MRJob):
    
    #INPUT_PROTOCOL = PickleValueProtocol
    
    def mapper_init(self):
        
        # recover json configuration uploaded with script
        fn = glob.glob('*.json')
        self.config = load(open(fn[0]))
        
        # maximum_number_of_periods definition 
        self.maximum_number_of_periods = self.config['maximum_number_of_periods']
        
    def reducer_init(self):
        
        # recover json configuration uploaded with script
        fn = glob.glob('*.json')
        self.config = load(open(fn[0]))
        
        # hbase connections
        self.hbase = happybase.Connection(self.config['app']['hbase']['host'], self.config['app']['hbase']['port'])
        self.hbase.open()
        self.hbase_table = self.hbase.table(self.config['measure_type']+'_'+str(self.config['companyId']))
        
        # maximum_number_of_periods definition
        self.maximum_number_of_periods = self.config['maximum_number_of_periods']
        
    def mapper(self, _, doc):   #we don't have value -> input protocol pickleValue which means no key is read
        
        doc = doc.split('\t')
        
        d = {
             'b':str(doc[0]),
             'ts': int(doc[2]),
             'v': float(doc[3+(self.maximum_number_of_periods*2)]) if doc[3+(self.maximum_number_of_periods*2)] != '\N' or doc[3+(self.maximum_number_of_periods*2)] != '\\N' else None,
             'c': None if (doc[4+(self.maximum_number_of_periods*2)] == '\N' or doc[4+(self.maximum_number_of_periods*2)] == '\\N' or doc[4+(self.maximum_number_of_periods*2)] == '0') else int(doc[4+(self.maximum_number_of_periods*2)])
            }
        
        d.update( { 'p': [], 'pa': [] } )
        
        for i in xrange(self.maximum_number_of_periods):
            d['p'].append( float(doc[3+i]) if doc[3+i] != '\N' or doc[3+i] != '\\N' else None)
            d['pa'].append( float(doc[3+self.maximum_number_of_periods+i]) if doc[3+self.maximum_number_of_periods+i] != '\N' or doc[3+self.maximum_number_of_periods+i] != '\\N' else None)
        
        yield doc[1], d


    def reducer(self, key, values):
        # sort documents by timestamp (same contractId)        
        docs_sorted = sorted(list(values), key=lambda k: k['ts'])
        
        # Interval of frequency of the resampling (Day == 86400 seconds)
        #interval=86400
        
        # N_periods of the measures
        n_periods = self.maximum_number_of_periods
        
        data=[]
        # Don't do any calculation if there are less or equal than 1 doc in docs_sorted_cumulative.
        if len(docs_sorted)>1:
            
            for n in xrange(len(docs_sorted)-1):
                
                values_between = [ 0.0 for item in xrange(n_periods) ]
                a = list(docs_sorted)[n]
                b = list(docs_sorted)[n+1]
                b['ts_p'] = [b['ts'] for item in xrange(n_periods)]
                
                for period in xrange(n_periods):
                    #ts_to_avoid=[]
                    i = 1
                    while b['pa'][period] is None:
                        if (n+1+i) <= (len(docs_sorted)-1):
                            b['pa'][period] = list(docs_sorted)[n+1+i]['pa'][period]
                            b['ts_p'][period] = list(docs_sorted)[n+1+i]['ts']
                            if docs_sorted[n+i]['p'][period] is not None:
                                values_between[period] += list(docs_sorted)[n+i]['p'][period]
                                #ts_to_avoid.append(docs_sorted[n+i]['ts'])
                        else:
                            break
                        i += 1
                            
                    # only calculate when the dict 'a' have (va - -) or (va v c)
                    if ( ( a['pa'][period] is not None and a['v'] is None and a['c'] is None ) or 
                       ( a['pa'][period] is not None and a['v'] is not None and a['c'] is not None ) ) and b['pa'] is not None:
                        
                        if b['pa'][period] >= a['pa'][period] and a['pa'][period] is not None and b['pa'][period] is not None:
                            diff_consumption = float(b['pa'][period]) - float(a['pa'][period]) - float(values_between[period])
                        elif a['pa'][period] is None or b['pa'][period] is None:
                            diff_consumption = None
                        else:
                            diff_consumption = 0
                                        
                        aux_ts = []
                        aux_ts.append(a['ts'])
                        #while ( aux_ts[len(aux_ts)-1] - ( aux_ts[len(aux_ts)-1] % interval ) + interval ) < b['ts_p'][period]:
                        #    aux_ts.append( aux_ts[len(aux_ts)-1] - ( aux_ts[len(aux_ts)-1] % interval ) + interval )
                        #
                        #if len(ts_to_avoid)>0:
                        #    for item in ts_to_avoid:
                        #        try:
                        #            aux_ts.pop(aux_ts.index(item))
                        #        except:
                        #            pass
                        
                        data_dict = value_in_a_dict(data,"ts")
                        try:
                            ind = data_dict[aux_ts[0]]['index']
                        except:
                            ind = len(data)
                            data.append({
                                 "b": (aux_ts[0]/100) % 100,
                                 "ts": aux_ts[0],
                                 "calc":1,
                                 "v": 0.0
                                })
                        partial_p = diff_consumption/float(len(aux_ts)) if diff_consumption is not None else None
                        if partial_p:
                            data[ind]['v'] += partial_p
                            if "p%s" % (period+1) in data[ind]:
                                data[ind]["p%s" % (period+1)] += partial_p
                            else:
                                data[ind].update( {"p%s" % (period+1): partial_p} )

                        
                        
        # WRITE THE THEORETICAL MEASURES TO HBASE
        
        for item in data:
            row_key= "%s~%s~%s" % (str(item['b']),str(item['ts']),key) # str(item['b'])=0 for test purposes
            row = { 'm:v': str(item['v']), 'm:calc': str(item['calc']) }
            for period in xrange(n_periods):
				if 'p%s' % (period+1) in item:
					row.update({ 'm:p%s' % (period+1): str( item['p%s' % (period+1)] ) })
            self.hbase_table.put(row_key, row) #print row_key,row


if __name__ == '__main__':
    Cumulative_Instant_Tertiary_ETL.run()