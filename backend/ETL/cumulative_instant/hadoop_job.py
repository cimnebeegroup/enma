from mrjob.job import MRJob
from mrjob.protocol import PickleValueProtocol

# hbase and mongo clients libs
import happybase
from pymongo import MongoClient
from bson.objectid import ObjectId

# Generic imports
import glob
from json import load
from datetime import datetime
from calendar import timegm
from copy import deepcopy

from lib.etl import transformations


class Cumulative_Instant_ETL(MRJob):
    
    #INPUT_PROTOCOL = PickleValueProtocol
    
    def reducer_init(self):
        
        # recover json configuration uploaded with script
        fn = glob.glob('*.json')
        self.config = load(open(fn[0]))
        
        # open connections
        self.hbase = happybase.Connection(self.config['app']['hbase']['host'], self.config['app']['hbase']['port'])
        self.hbase.open()
        
        self.hbase_table = self.hbase.table(self.config['measure_type']+'_'+str(self.config['companyId']))
        
        
    def mapper(self, _, doc):   #we don't have value -> input protocol pickleValue which means no key is read
        
        doc = doc.split('\t')
        d = {
             'b':str(doc[0]),
             'ts': int(doc[2]),
             'v': float(doc[3]) if doc[3] != '\N' or doc[3] != '\\N' else None,
             'va': float(doc[4]) if doc[4] != '\N' or doc[4] != '\\N' else None,
             'c': None if (doc[5] == '\N' or doc[5] == '\\N' or doc[5] == '0') else int(doc[5])
            }
        yield doc[1], d
    
    
    def reducer(self, key, values):
        
        
        docs_sorted = sorted(values, key=lambda k: k['ts'])
        
        # Interval of frequency of the resampling (Day == 86400 seconds)
        interval=86400
        data=[]
        
        # Don't do any calculation if there are less or equal than 1 doc in docs_sorted_cumulative.
        if len(docs_sorted)>1:
            
            for n in xrange(len(docs_sorted)-1):
              
                values_between = 0.0
                a = docs_sorted[n]
                b = docs_sorted[n+1]
                i = 1
                
                while b['va'] is None:
                  if (n+1+i) <= (len(docs_sorted)-1):
                    b['va'] = docs_sorted[n+1+i]['va']
                    if docs_sorted[n+i]['v'] is not None:
                        values_between += docs_sorted[n+i]['v']
                  else:
                    break
                  i += 1
                
                # only calculate when the dict 'a' have (va - -) or (va v c)
                if ( ( a['va'] is not None and a['v'] is None and a['c'] is None ) or 
                   ( a['va'] is not None and a['v'] is not None and a['c'] is not None ) ) and b['va'] is not None:
                  if b['va'] >= a['va']:
                    diff_consumption = b['va'] - a['va'] - values_between
                  else:
                    diff_consumption = 0
                  
                  data.append({
                                "b": a['b'],
                                "ts": a['ts'],
                                "v":diff_consumption,
                                "calc":1
                              })
                  
            
        """ WRITE THE THEORETICAL MEASURES TO HBASE """
        
        for item in data:
            row_key= "%s~%s~%s" % (str(item['b']),str(item['ts']),key) # str(item['b'])=0 for test purposes
            row = { 'm:v': str(item['v']), 'm:calc': str(item['calc']) }
            self.hbase_table.put(row_key, row) #print row_key,row
            
    
if __name__ == '__main__':
    Cumulative_Instant_ETL.run()    