from mrjob.job import MRJob
from mrjob.protocol import PickleValueProtocol

# hbase and mongo clients libs
import happybase
from pymongo import MongoClient, ASCENDING, DESCENDING
from bson.objectid import ObjectId

# Generic imports
import glob
from json import load
from datetime import datetime
from calendar import timegm
from copy import deepcopy

from lib.etl import transformations


class Hadoop_ETL(MRJob):
    
    INPUT_PROTOCOL = PickleValueProtocol
    
    def mapper_init(self):
        
        # recover json configuration uploaded with script
        fn = glob.glob('*.json')
        self.config = load(open(fn[0]))
        
        self.readings_cache = {}
        self.devices_cache = {} #careful: consider 1 Million contracts with 5 devices each it will take 1GB on memory [[ (48*2+100)*5 * 1000000 / 1024 / 1024 = 934 MB  ]]
        
        # open connections
        self.hbase = happybase.Connection(self.config['app']['hbase']['host'], self.config['app']['hbase']['port'])
        self.hbase.open()
        
        self.tables_list = self.hbase.tables()
        
        self.mongo = MongoClient(self.config['app']['mongodb']['host'], self.config['app']['mongodb']['port']) 
        self.mongo[self.config['app']['mongodb']['db']].authenticate(
                self.config['app']['mongodb']['username'],
                self.config['app']['mongodb']['password']
                )
        
        #if not 'amon_measures_measurements_with_errors' in self.mongo[self.config['app']['mongodb']['db']].collection_names():
        #        self.mongo[self.config['app']['mongodb']['db']].create_collection('amon_measures_measurements_with_errors')
        #        self.mongo[self.config['app']['mongodb']['db']]['amon_measures_measurements_with_errors'].create_index([("companyId", ASCENDING),("timestamp", DESCENDING)])
        
        
    def add_reading_information(self, doc):
        r = self.readings_cache.get(doc['reading'])
        if not r:
            #r = self.mongo[self.config['app']['mongodb']['db']]['readings'].find_one({'_id': doc['reading']})
            r = self.mongo[self.config['app']['mongodb']['db']]['readings'].find_one({'_id': ObjectId(doc['reading'])})
            self.readings_cache[doc['reading']] = r
        
        if r is None:
            doc['error'] = "No reading information related"
            return doc
        
        doc['reading'] = r
        
        return doc
    
    def build_row_key(self, doc):
        row_key = []
        for element in self.config['etl']['hbase_table']['key']:
            row_key.append(str(doc[element]))
            #row_key.append(element)
            
        return "~".join(row_key)        
                
    
    def translate_deviceId_contractId(self, doc):
        # pop from document to replace it for contractId
        device = doc.pop('deviceId')

        contracts = self.devices_cache.get(device)
        """
        {
         devices: { 
          $elemMatch: { 
           deviceId: "A96B6E8373536E4A64D1BD2D8297A4C0",
           dateStart: {
             $lte: ISODate("2013-12-12T00:00:00Z")
            },
           $or: [ 
                  { dateEnd: null }, 
                  { dateEnd: { $gte: ISODate("2013-12-12T00:00:00Z") } }
            ]
          } 
         }
        }
        """
        contract = False
        if contracts:
            #return contracts
            for con in contracts:
                if con['devices'][0]['dateStart'] <= doc['timestamp'] and ( not 'dateEnd' in con['devices'][0] or not con['devices'][0]['dateEnd'] or con['devices'][0]['dateEnd'] >= doc['timestamp'] ):
                    contract = con
                    break 
            
        if not contract:
            query = {
                     'devices' : {
                                  '$elemMatch': {
                                                 'deviceId': device,
                                                 'dateStart': {
                                                               '$lte': doc['timestamp']
                                                               },                                                 
                                                '$or': [
                                                     { 'dateEnd': None } ,
                                                     { 'dateEnd': {'$exists': False } },
                                                     { 'dateEnd': { '$gte': doc['timestamp'] } }   
                                                     ]
                                                 }
                                  }
                     }
            
            projection = deepcopy(query)
            projection.update({'contractId': 1}) 
            
            contract = self.mongo[self.config['app']['mongodb']['db']]['contracts'].find_one(query, projection)
            
            if not contract:
                doc['deviceId'] = device
                error = doc.get('error')
                doc['error'] = "No contract information related with this deviceId" if not error else error + 'and no contract information related with this deviceId'
                return doc
            
            if not contracts:
                self.devices_cache[device] = [contract]
            else:
                self.devices_cache[device].append(contract)
                
        doc['contractId'] = contract['contractId']
             
        
        return doc
        
    def mapper(self, _, doc):   #we don't have value -> input protocol pickleValue which means no key is read
        
        """
        doc = {
            "timestamp": "2013-11-30 18:00:00",
            "reading": "52a9845fdfeb570207c02319",
            "deviceId": "912062bb-21ec-5787-805d-cf3858c67405",
            "value": "1148.0",
            "companyId": "8449512768"
            }
        """
        
        # create a dictionary from python string
        # use config file uploaded with script
        #doc = self.list_to_doc(line)
        
        # Transform functions
        doc = self.add_reading_information(doc)
        doc = transformations.convert_units_to_kilo(doc)
        doc = self.translate_deviceId_contractId(doc)
        
        if 'error' in doc:
            #If there are errors with the doc, save the measure in the amon_measure_measurements_with_errors collection in REST
            doc['reading'] = doc['reading']['_id']
            doc['error_detected_at'] = datetime.now()
            #self.mongo[self.config['app']['mongodb']['db']]['amon_measures_measurements_with_errors'].insert(doc)
            yield 1, str(doc) # yielding records will tell us if something went wrong (output map records should be 0)
            #yield 2, str(doc['query'])
            # customer not found
            #raise Exception("The deviceId %s dont correspond to any contractId" % doc['deviceId'])
            return
        
        doc = transformations.translate_datetime(doc)
        doc = transformations.add_ts_bucket(doc)
        
        row_key = self.build_row_key(doc)
        doc_key = 'm:v' if doc['reading']['period'] in ['INSTANT','PULSE'] else 'm:va'
        row = { 
                doc_key : str(doc['value']),
                'm:calc': '0'
              }
        
        table_name = doc['reading']['type']+'_'+str(doc['companyId'])
        
        try:
            if not table_name in self.tables_list:
                self.hbase.create_table(table_name, { 'm': dict() })
                self.tables_list.append(table_name)
        except:
            pass
            
        hbase_table = self.hbase.table(table_name)
            
        hbase_table.put(row_key, row)
        
        #yield row_key, str(row, table_name)  
    
    
    
if __name__ == '__main__':
    Hadoop_ETL.run()    