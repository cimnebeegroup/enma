from celery.utils.log import get_task_logger
from celery.signals import after_setup_task_logger, after_setup_logger
from pymongo import ASCENDING, DESCENDING
from tempfile import NamedTemporaryFile
from json import dumps as JSONdumps
from datetime import datetime

from ETL import ETLTask, ETLTaskFailed
from lib.etl import transformations, delete_measures_hbase, delete_results_hbase
from lib.utils import *
from lib.logs import setup_log

after_setup_logger.connect(setup_log)
after_setup_task_logger.connect(setup_log)
logger = get_task_logger('etl.mh_hadoop')

#mapreduce imports
import lib
from os import path
from hadoop_job import Hadoop_ETL

#celery imports
from celery_backend import app
from celery import current_task


class ETLMongoHBaseHadoopTask(ETLTask):
    abstract = True
    
    def __init__(self):
        logger.debug('ETLMongoHBaseHadoopTask init')
        self.etl_name = 'mh_hadoop_v2'
        #self.init_readings_cache()
        ETLTask.__init__(self)
    
    def hadoop_job(self, input,params):
        """Runs the Hadoop job uploading task configuration"""
        # create report to save on completion or error
        report = {
                  'started_at': datetime.now(),
                  'state': 'launched',
                  'input': input
                  }
        
        # Create temporary file to upload with json extension to identify it in HDFS
        f = NamedTemporaryFile(delete=False, suffix='.json')
        f.write(JSONdumps(self.config))
        f.close()
        report['config_temp_file'] = f.name
        logger.debug('Created temporary config file to upload into hadoop and read from job: %s' % f.name)
        
        # create hadoop job instance adding file location to be uploaded 
        dtnow = datetime.now()
        str_dtnow = str(dtnow.year)+str("%02i" % dtnow.month)+str("%02i" % dtnow.day)+str("%02i" % dtnow.hour)+str("%02i" % dtnow.minute)
        mr_job = Hadoop_ETL(args=['-r', 'hadoop', input, '--file', f.name, '--output-dir', '/tmp/ETL_measures_not_considered/%s/%s' % (str(params['companyId']),str_dtnow), '--python-archive', path.dirname(lib.__file__)])
        #mr_job = Hadoop_ETL(args=['-r', 'hadoop', input, '--file', f.name, '--python-archive', path.dirname(lib.__file__)])
        with mr_job.make_runner() as runner:
            try:
                runner.run()
            except Exception,e:
                f.unlink(f.name)
                raise Exception('Error running MRJob ETL process using hadoop: %s' % e)
    
        f.unlink(f.name)
        logger.debug('Temporary config file uploaded has been deleted from FileSystem')
        
        
        report['finished_at'] = datetime.now()
        report['state'] = 'finished'
        
        return report
    


@app.task(name='ETL_hadoop_v2', base=ETLMongoHBaseHadoopTask)
def ETL_hadoop_v2(params):
    
    
    logger.info('Starting MongoDB-HBase ETL using Hadoop ...')
    
    logger.debug('Builing task context with config, clients and report')
    context = {}
    context['current_task'] = current_task
    context['config'] = ETL_hadoop_v2.config
    
    try:
        context['clients'] = {
                              'mongo': ETL_hadoop_v2.mongo,
                              'hive': ETL_hadoop_v2.hive,
                              'hdfs': ETL_hadoop_v2.hdfs,
                              'hbase': ETL_hadoop_v2.hbase,
                              'hive': ETL_hadoop_v2.hive
                              }
    except Exception, e:
        raise ETLTaskFailed(context, 'Error connecting to needed connection clients: %s' % e)
    
    task_UUID=id_generator()
    context['config']['etl']['delete_measures']['paths'] = random_paths(context['config']['etl']['delete_measures']['paths'],'UUID', task_UUID)
    context['config']['etl']['delete_results']['paths'] = random_paths(context['config']['etl']['delete_results']['paths'],'UUID', task_UUID)
    
    context['report'] = Report(monogodb_fd=context['clients']['mongo'], db=context['config']['app']['report']['db'], collection=context['config']['app']['report']['collection'])
    context['report']['task_id'] = current_task.request.id
    context['report']['started_at'] = datetime.now()
    context['report']['params'] = [params]
    context['report']['module'] = ETL_hadoop_v2.etl_name
    
    context['report'].update()
    
    logger.debug('Parameters provided to ETL_Hadoop function: %s' % params)
    
    
    
    """CHECK INCONSISTENCIES IN params"""
    try:
        ts_to = params['ts_to'] if 'ts_to' in params else None
        ts_from = params['ts_from'] if 'ts_from' in params else None
        companyId = params['companyId'] if 'companyId' in params else None
        buffer_size = params['buffer_size'] if 'buffer_size' in params else 1000000
        delete_process = params['delete_measures_in_mongo'] if 'delete_measures_in_mongo' in params else True
    except KeyError, e:
        raise ETLTaskFailed(context, 'Not enough parameters provided to module: %s' % (e))
    
    
    
    
    """
    DELETE THE DESIRED MEASURES AND RESULTS FROM HBASE (THE DELETE ORDERS ARE STORED IN delete_measures AND delete_results COLLECTION IN REST)
    """
    logger.info('Execute the delete orders of the utility (Undesired data gonna be deleted from HBase)')
    if companyId is not None:
        try:
            logger.debug('Delete the undesired HBase measures')
            context['report']['execution_of_the_measures_delete_orders'] =  delete_measures_hbase.delete_measures_hbase(context,companyId,task_UUID)
        except Exception,e:
            raise ETLTaskFailed(context,'Error deleting the measures of companyId %s. Error: %s' % (companyId,e))
        
        try:
            logger.debug('Delete the undesired HBase results')
            context['report']['execution_of_the_results_delete_orders'] =  delete_results_hbase.delete_results_hbase(context,companyId,task_UUID)
        except Exception,e:
            raise ETLTaskFailed(context,'Error deleting the results of companyId %s. Error: %s' % (companyId,e))
    
    
    
    
    
    """
    QUERY THE MEASURES FROM REST
    """
    
    # set query dictionary
    if ts_from is not None and ts_to is not None and companyId is not None:
        query = {
                 'companyId': companyId,
                 'timestamp': {
                               '$gte': ts_from,
                               '$lt': ts_to
                               }
                 }
    
    elif companyId is not None:
        query = {
                 'companyId': companyId,
                }
    
    else:
        query = {}
    
    # set projection dictionary (1 means field returned, 0 field wont be returned)
    projection = {
                  'reading': 1,
                  'deviceId': 1,
                  'timestamp': 1,
                  'companyId': 1,
                  'value': 1,
                  '_id': 0
                  }

    # setting variables for readability
    measures_collection = context['config']['etl']['mongodb']['collection']
    db = context['config']['app']['mongodb']['db']
    
    logger.debug('Querying for measures in MongoDB: %s' % query)
    cursor = context['clients']['mongo'][db][measures_collection].find(query, projection, timeout=False)
    
    
    
    
    """
    LOAD THE SELECTED MEASURES TO HBASE
    """
    
    # get buffer size from parameters. Default if not provided
    context['report']['buffer_size'] = buffer_size
    
    context['report']['hadoop_jobs'] = []
    logger.debug('Creating buffer to execute hadoop job. Buffer size: %s' % buffer_size)
    buffer = transformations.create_buffer(cursor, buffer_size)
    while buffer:
        # Copy buffer to hdfs
        file = transformations.create_pickle_file_from_buffer(buffer)
        try:
            # Launch MapReduce job
            ## Buffered measures to HBase
            logger.debug('Loading the buffer to Hbase')
            job_report = ETL_hadoop_v2.hadoop_job(file.name,params)
            context['report']['hadoop_jobs'].append({'job_report':job_report, 'buffer_size': len(buffer)})
        except Exception, e:
            raise ETLTaskFailed(context, 'MRJob process on MongoDB-HBase ETL job has failed: %s' % e)
        
        logger.info('A Hadoop job performing ETL process has finished. Loaded %s measures' % len(buffer))
        
        # remove temporary file
        logger.debug('Removing temporary local CSV file: %s' % file.name)
        file.unlink(file.name)
        
        # create new buffer for next loop
        del buffer
        logger.debug('Creating buffer to execute hadoop job. Buffer size: %s' % buffer_size)
        buffer = transformations.create_buffer(cursor, buffer_size)
    
    
    
    
    """
    DELETE THE REST MEASURES THAT HAS BEEN UPLOADED TO HBASE
    """
    
    if delete_process:
        try:
            logger.info('Deleting the REST measures that has been loaded to Hbase')
            transformations.remove_measures(context,db, measures_collection, query,
                                            index=[("companyId", ASCENDING),("timestamp", DESCENDING),('deviceId',ASCENDING)],
                                            index2=[("companyId", DESCENDING)])
            logger.info('The measures from REST were deleted')
        except Exception, e:
            raise ETLTaskFailed(context, 'There was an error with the deleting process of the REST measures: %s' % e)
    
    
    
    
    
    """
    SAVE THE FINAL REPORT
    """
    context['report'].save()
    
    if not current_task.request.id:
        logger.debug('Closing the connections opened by ETL_hadoop_v2 function')
        ETL_hadoop_v2._close_connections()
    
    return True





"""
params = {
         'companyId': 1111111111
         'ts_from': datetime(2013,10,30,0,0,0),
         'ts_to': datetime(2014,1,1,23,59,59),
         'buffer_size':1000000,
         'delete_measures_in_mongo': False
         }
"""