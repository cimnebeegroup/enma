from mh_hadoop_v2.tasks import ETL_hadoop_v2
from weather_hadoop.tasks import ETL_weather_hadoop
from contracts.tasks import ETL_contracts
from stations.tasks import ETL_stations
from cumulative_instant.tasks import ETL_cumulative_instant
from measures_hive.tasks import ETL_measures_hive


