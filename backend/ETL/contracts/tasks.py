from lib.logs import setup_log
from celery.utils.log import get_task_logger
from celery.signals import after_setup_task_logger, after_setup_logger

after_setup_logger.connect(setup_log)
after_setup_task_logger.connect(setup_log)
logger = get_task_logger('etl.contracts')


from ETL import ETLTask, ETLTaskFailed
from lib.utils import Report

#celery imports
from celery_backend import app
from celery import current_task

from datetime import datetime, timedelta

class ETLContractsTask(ETLTask):
    abstract = True
    
    def __init__(self):
        logger.debug('ETLContractsTask init')
        self.etl_name = 'contracts'
        #self.init_readings_cache()
        ETLTask.__init__(self)
    
    """
    def init_readings_cache(self):
        logger.debug('Initialize Readings cache to join information with measures')
        self.cache = {}
        for reading in self.mongo[self.config['app']['mongodb']['db']]['readings'].find():
            # save entire document (without _id field) to embed when needed
            self.cache[reading.pop('_id')] = reading
    """
    
    def save_contract_into_hbase(self, contract):
        row = {}
        #prepare documents to iterate them
        try:
            customer = contract.pop('customer')
            companyId = contract.pop('companyId')
            contract['customerId'] = customer.pop('customerId')
            
            row_key = str(companyId)+'~'+contract.pop('contractId')
            #info cf
            for key, value in contract.iteritems():
                if isinstance(value,datetime):
                    row['info:%s'%key] = str(value.strftime('%s'))
                else:
                    row['info:%s'%key] = str(value)
            #address cf
            if customer.get('address'):
                for key, value in customer['address'].iteritems():
                    row['address:%s'%key] = str(value.encode('utf-8'))
            #customisedGroupingCriteria cf
            if customer.get('customisedGroupingCriteria'):
                for key, value in customer['customisedGroupingCriteria'].iteritems():
                    row['info:%s'%key] = str(value)
            #customisedServiceParameters
            if customer.get('customisedServiceParameters'):
                for key, value in customer['customisedServiceParameters'].iteritems():
                    row['info:%s'%key] = str(value)
            #buildingData
            if customer.get('buildingData'):
                for key, value in customer['buildingData'].iteritems():
                    row['profile:%s'%key] = str(value)
            #profile
            if customer.get('profile'):
                for key, value in customer['profile'].iteritems():
                    row['profile:%s'%key] = str(value)

            
            self.hbase_table.put(row_key, row)
        except KeyError,e:
            logger.debug('Contract with missing fields: %s. Not saved into HBase' % e)
    
    def _get_last_ts(self, context, companyId):
        folder = "%s/%s" % (context['config']['app']['config_execution']['path'], context['config']['etl']['paths']['last_ts'])
        
        try:
            with open( '%s/%s.ts' % (folder,str(companyId)) ) as f:
                ts = f.readline().strip('\n')
                dt = datetime.strptime(ts, '%Y-%m-%dT%H:%M:%SZ')
        except Exception, e:
            # consider any error as there is no previous backup yet
            dt = None
        
        return dt

    def _save_ts(self, context, companyId, dt):
        folder = "%s/%s" % (context['config']['app']['config_execution']['path'], context['config']['etl']['paths']['last_ts'])
            
        with open('%s/%s.ts' % (folder, str(companyId)), 'w') as f:
            ts = dt.strftime('%Y-%m-%dT%H:%M:%SZ')
            f.write(ts)
                    
        return


@app.task(name='ETL_contracts', base=ETLContractsTask)
def ETL_contracts(params):
    
    
    logger.info('Starting Contracts ETL')
    
    logger.debug('Builing task context with config, clients and report')
    
    context = {}
    context['current_task'] = current_task
    context['config'] = ETL_contracts.config
    
    
    try:
        context['clients'] = { 'mongo': ETL_contracts.mongo }
    except Exception, e:
        raise ModuleTaskFailed(context, 'Error connecting to needed connection clients: %s' % e)
    
    
    context['report'] = Report(monogodb_fd=context['clients']['mongo'], db=context['config']['app']['report']['db'], collection=context['config']['app']['report']['collection'])
    context['report']['task_id'] = current_task.request.id
    context['report']['started_at'] = datetime.now()
    context['report']['params'] = [params]
    context['report']['module'] = ETL_contracts.etl_name
    
    context['report'].update()
    
    logger.debug('Parameters provided to ETL_contracts function: %s' % params)
    
    # set query dictionary to get the contracts in REST
    if 'last_etl' not in params or params['last_etl']==0:
        
        # if there is not specified last_etl datetime in params, consider the last execution
        
        last_etl = ETL_contracts._get_last_ts(context,params['companyId'])
        
        if last_etl:
            query = {
                     'companyId': params['companyId'],
                     '_updated': {
                               '$gte': last_etl
                               }
                    }
        else:
            query = {
                     'companyId': params['companyId']
                    }
    else:
        query = {
             'companyId': params['companyId'],
             '_updated': {
                           '$gte': params['last_etl']
                           }
             }
    # save the actual datetime to 
    ETL_contracts._save_ts( context, params['companyId'], datetime.now()-timedelta(seconds=1) )
    
    # setting variables for readability in MONGO
    db = context['config']['app']['mongodb']['db']
    collection = context['config']['etl']['mongodb']['name']
    
    logger.debug('Querying for measures in MongoDB: %s' % query)
    context['report']['etl'] = {}
    context['report']['etl'] = {
                                'started_at': datetime.now(),
                                'query': str(query),
                                'state': 'started',
                                'extracted': 0,
                                'loaded': 0
                                }
    
    projection = {
                  'devices':0,
                  '_id':0,
                  '_created':0,
                  '_updated':0
                  }
    
    ETL_contracts.hbase_table = ETL_contracts.hbase.table(context['config']['etl']['hbase_table']['name']) 
    
    for contract in context['clients']['mongo'][db]['contracts'].find(query, projection, timeout=False):
        context['report']['etl']['extracted'] += 1
        ETL_contracts.save_contract_into_hbase(contract)
        context['report']['etl']['loaded'] += 1
        
    context['report']['etl']['state'] = 'finished'
    
    context['report'].save()
    
    if not current_task.request.id:
        logger.debug('Closing the connections opened by ETL_contracts function')
        ETL_contracts._close_connections()
    
    return True



"""
params = {
          'companyId': 1111111111
         }
"""