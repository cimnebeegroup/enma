from celery.utils.log import get_task_logger
from celery.signals import after_setup_task_logger, after_setup_logger
from pymongo import ASCENDING, DESCENDING
from tempfile import NamedTemporaryFile
from json import dumps as JSONdumps
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
import types

from ETL import ETLTask, ETLTaskFailed
from snakebite.errors import FileNotFoundException
from lib.etl import transformations, delete_measures_hbase, delete_results_hbase
from lib.querybuilder import QueryBuilder
from lib.utils import *
from lib.logs import setup_log

after_setup_logger.connect(setup_log)
after_setup_task_logger.connect(setup_log)
logger = get_task_logger('etl.measures_hive')

#mapreduce imports
import lib
from os import path

#celery imports
from celery_backend import app
from celery import current_task


class ETLMeasuresHiveTask(ETLTask):
    abstract = True
    
    def __init__(self):
        logger.debug('ETLMeasuresHiveTask init')
        self.etl_name = 'measures_hive'
        #self.init_readings_cache()
        ETLTask.__init__(self)



@app.task(name='ETL_measures_hive', base=ETLMeasuresHiveTask)
def ETL_measures_hive(params):
    
    
    logger.info('Starting Measures-hive ETL ...')
    
    logger.debug('Builing task context with config, clients and report')
    context = {}
    context['current_task'] = current_task
    context['config'] = ETL_measures_hive.config
    
    try:
        context['clients'] = {
                              'mongo': ETL_measures_hive.mongo,
                              'hive': ETL_measures_hive.hive,
                              'hdfs': ETL_measures_hive.hdfs,
                              'hbase': ETL_measures_hive.hbase
                              }
    except Exception, e:
        raise ETLTaskFailed(context, 'Error connecting to needed connection clients: %s' % e)
    
    task_UUID = id_generator()
    
    context['report'] = Report(monogodb_fd=context['clients']['mongo'], db=context['config']['app']['report']['db'], collection=context['config']['app']['report']['collection'])
    context['report']['task_id'] = current_task.request.id
    context['report']['started_at'] = datetime.now()
    context['report']['params'] = [params]
    context['report']['module'] = ETL_measures_hive.etl_name
    
    context['report'].update()
    
    logger.debug('Parameters provided to Measures-hive function: %s' % params)
    
    
    try:
        table_needs_to_be_recreated = params['table_needs_to_be_recreated'] if 'table_needs_to_be_recreated' in params else False
        dtnow = datetime.now()
        if table_needs_to_be_recreated is True:
            num_months = context['config']['etl']['num_months_default']
            ts_to = dtnow.replace(day=1, hour=0, minute=0, second=0, microsecond=0) + relativedelta(months=1) - relativedelta(seconds=1)
            ts_from = monthback_date(ts_to,num_months)
        else:
            num_months = params['num_months'] if 'num_months' in params else context['config']['etl']['num_months_default']
            ts_to = params['ts_to'] if 'ts_to' in params else dtnow.replace(day=1, hour=0, minute=0, second=0, microsecond=0) + relativedelta(months=1) - relativedelta(seconds=1)
            ts_from = monthback_date(ts_to,num_months)
        companyId = params['companyId']
        measures_types = params['type'] if isinstance(params['type'],types.ListType) else [params['type']]
        timezone = params['timezone']
        maximum_number_of_periods = params['maximum_number_of_periods_of_the_measures'] if 'maximum_number_of_periods_of_the_measures' in params else context['config']['etl']['maximum_number_of_periods_default']
        
    except KeyError, e:
        raise ETLTaskFailed(context, 'Not enough parameters provided to ETL: %s' % (e))
        
    # ITERATE THE ETL FOR ALL THE MEASURES_TYPES    
    for type in measures_types:
        
        tertiary_type = True if type[0:8]=='tertiary' else False
        tou_type = True if type[0:3]=='tou' else False
        
        # GET THE DIRECTORY PATHS WHERE THE MEASURES WILL BE SAVED IN HDFS
        temporary_path = "%s/%s/%s" % (context['config']['etl']['hive']['temporary_path'],task_UUID,type)
        root_path = "%s/%s/%s" % (context['config']['etl']['hive']['root_path'],str(companyId),type)
        
        
        # GET THE MEASURES FROM HBASE AND LOAD IN A TEMPORARY HDFS DIRECTORY
        context['report']['trace'] = {}
        qb = QueryBuilder(context['clients']['hive'])
        
        if tertiary_type or tou_type:
            table_from = create_measures_temp_table(context, type, companyId, task_UUID, tertiary=True, number_period_tertiary=maximum_number_of_periods)
            fields = [('ts','bigint'),('contractId','string')]
            measures_periods = []
            for i in xrange(1,maximum_number_of_periods+1):
                fields.extend([('p'+str(i),'float'),('p'+str(i)+'a','float')])
                measures_periods.extend(['p'+str(i),'p'+str(i)+'a'])
            fields.extend([('value','float'),('calculated','int'),('month','int'),('year','int')])
        else:
            table_from = create_measures_temp_table(context, type, companyId, task_UUID, cumulative=True)
            fields = [('ts', 'bigint'),('contractId','string'),('value','float'),('accumulated','float'),('calculated','int'),('month','int'),('year','int')]
        
        table_tmp = create_hive_module_input_table(context, 'HBaseTempTable_%s' % type, temporary_path, fields, task_UUID)
        logger.debug('Selecting from %s and inserting into %s' % (table_from, table_tmp))
        qb = qb.add_from(table_from, 'e').add_insert(table=table_tmp)
        
        if tertiary_type or tou_type:
            qb = qb.add_select('unix_timestamp(from_utc_timestamp(from_unixtime(e.key.ts),"%s")),\
                            e.key.contractId,\
                            %s,\
                            e.value,\
                            e.calculated,\
                            month(from_utc_timestamp(from_unixtime(e.key.ts),"%s")),\
                            year(from_utc_timestamp(from_unixtime(e.key.ts),"%s"))' % (timezone,",".join(measures_periods),timezone,timezone))
        else:
            qb = qb.add_select('unix_timestamp(from_utc_timestamp(from_unixtime(e.key.ts),"%s")),\
                            e.key.contractId,\
                            e.value,\
                            e.accumulated,\
                            e.calculated,\
                            month(from_utc_timestamp(from_unixtime(e.key.ts),"%s")),\
                            year(from_utc_timestamp(from_unixtime(e.key.ts),"%s"))' % (timezone,timezone,timezone))
        
        qb = qb.add_where('unix_timestamp(from_utc_timestamp(from_unixtime(e.key.ts),"%s")) >= UNIX_TIMESTAMP("%s","yyyy-MM-dd HH:mm:ss")' % (timezone,ts_from))
        qb = qb.add_and_where('unix_timestamp(from_utc_timestamp(from_unixtime(e.key.ts),"%s")) <= UNIX_TIMESTAMP("%s","yyyy-MM-dd HH:mm:ss")' % (timezone,ts_to))
        
        try:
            pass
            context['report']['trace']['hive_temporal_query_%s' % type] = qb.execute_query()
        except Exception, e:
            raise ETLTaskFailed(context, msg='%s' % e)
        
        
        
        # GET THE MEASURES FROM THE TEMPORARY HDFS DIRECTORY AND GENERATE THE PARTITIONED HDFS TABLE
        qb = QueryBuilder(context['clients']['hive'])
        if tertiary_type or tou_type:
            table_fields = [('ts','bigint'),('contractId','string')]
            measures_periods = []
            for i in xrange(1,maximum_number_of_periods+1):
                table_fields.extend([('p'+str(i),'float'),('p'+str(i)+'a','float')])
                measures_periods.extend(['p'+str(i),'p'+str(i)+'a'])
            table_fields.extend([('value','float'),('calculated','int')])
        else:
            table_fields = [('ts', 'bigint'),('contractId','string'),('value','float'),('accumulated','float'),('calculated','int')]
        partitioner_fields = [('year','int'),('month','int')]
        table_input = create_hive_partitioned_table(context, type, companyId, table_fields, partitioner_fields, root_path, table_needs_to_be_recreated)
        logger.debug('Selecting from %s and inserting into %s' % (table_tmp, table_input))
        qb = qb.add_from(table_tmp, 'e').add_insert( table=table_input, partition='year, month' )
        qb = qb.add_dynamic()
        if tertiary_type or tou_type:
            qb = qb.add_select('e.ts,\
                            e.contractId,\
                            %s,\
                            e.value,\
                            e.calculated,\
                            e.year,\
                            e.month' % ",".join(measures_periods) )
        else:
            qb = qb.add_select('e.ts,\
                            e.contractId,\
                            e.value,\
                            e.accumulated,\
                            e.calculated,\
                            e.year,\
                            e.month')
        
        try:
            context['report']['trace']['hive_partitioned_table_generation_%s' % type] = qb.execute_query()
        except Exception, e:
            raise ETLTaskFailed(context, msg='%s' % e)
    
    
    
    # Delete the hive temp tables
    try:
        pass
        for table in context['temp_hive_tables']['measures']:
            delete_measures_temp_table(context,table)
        for table in context['temp_hive_tables']['input']:
            delete_hive_module_input_table(context,table)
    except Exception, e:
        raise ETLTaskFailed(context,msg='%s' % e)


    # Delete HDFS temporary files
    try:
        pass
        context['report']['trace']['cleanup_temp_data'] = cleanup_temp_data(context, "%s/%s" % (context['config']['etl']['hive']['temporary_path'], task_UUID), recurse=True)
    except Exception, e:
        raise ETLTaskFailed(context,msg='%s' % e)
    
            
    context['report'].save()
    
    if not current_task.request.id:
        logger.debug('Closing the connections opened by ETL_measures_hive function')
        ETL_measures_hive._close_connections()

    return True






"""
params = {
         'companyId': 1111111111, 
         'type': ['electricityConsumption','waterConsumption','heatConsumption','heatConsumption_SH','heatConsumption_HW'],
         'timezone': 'Europe/Madrid',
         'num_months': 36,
         #'maximum_number_of_periods_of_the_measures':3,
         #'ts_to': datetime(2015,6,30,23,59,59)
         }
"""
