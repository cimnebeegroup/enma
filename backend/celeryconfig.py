from kombu import Exchange, Queue
from json import load


CELERY_TASK_RESULT_EXPIRES = 3600
CELERY_RESULT_PERSISTENT = False
CELERY_TASK_SERIALIZER = 'pickle'
CELERY_RESULT_SERIALIZER = 'pickle'
CELERY_TIMEZONE = 'Europe/Madrid'
CELERY_ENABLE_UTC = True
CELERY_QUEUES = (
    Queue('modules',  Exchange('modules', type='direct')),
    Queue('etl',  Exchange('etl', type='direct')),
    # we create a direct exchange so we can send all tasks to a single queue and routing them by name
)
CELERY_ROUTES = ({
    'ETL_hadoop_v2': {'queue': 'etl'},
    'ETL_weather_hadoop': {'queue': 'etl'},
    'ETL_contracts': {'queue': 'etl'},
    'ETL_stations': {'queue': 'etl'},     
    'ETL_cumulative_instant': {'queue': 'etl'},
    'ETL_measures_hive': {'queue': 'etl'},
    'ot101': {'queue': 'modules'},
    'ot201': {'queue': 'modules'},
    'ot301': {'queue': 'modules'},
    'ot501': {'queue': 'modules'},
    'ot703': {'queue': 'modules'},
})
#CELERY_IGNORE_RESULT = True,
CELERY_CHORD_PROPAGATES = True
CELERY_DISABLE_RATE_LIMITS = True
CELERYD_LOG_COLOR = False






# environment dependent configuration
f = open('envconfig.json')
config = load(f)
f.close()
BROKER_URL = 'amqp://%s@%s:%s//' % (config['broker']['user'], config['broker']['host'], config['broker']['port'])
CELERY_RESULT_BACKEND = 'amqp'